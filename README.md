Asset Parser
===

Parse assets from html documents. 

## Installation

Clone or download the library. `cd` into the directory. Install dependencies and create autoloader with `composer install`.

## Basic Usage

```php
<?php

use ClearC2\AssetParser\Entity\HtmlDocument;
use ClearC2\AssetParser\Entity\Map;
use ClearC2\AssetParser\Parser;

$maps = /* instanceof ClearC2\AssetParser\Repository\MapRepository */; 
$parser = new Parser($maps);

$document = new HtmlDocument(file_get_contents('receipt.html'));
$result = $parser->parse($document);
$assets = $result->getAssets();

// or

$map = new Map(file_get_contents('receipt_map.json'));
$assets = $parser->parseUsingMap($document, $map)->getAssets();
```

## Testing

`./vendor/bin/phpunit -c phpunit.xml`