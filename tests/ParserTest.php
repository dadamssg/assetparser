<?php

namespace ClearC2\AssetParser;

use ClearC2\AssetParser\Entity\Asset;
use ClearC2\AssetParser\Repository\FileHtmlDocumentRepository;
use ClearC2\AssetParser\Repository\FileMapRepository;

/**
 * Class EmailParserTest
 * You can run this (on James' machine) at the Terminal, with the command
 * C:\phonegap\websites\theLogue\app\php\tests>phpunit EmailParserTest.php
 * or
 * C:\>phpunitEmailParser.bat
 *
 * @author James Jurczewsky james@clearc2.com
 * @copyright Clear C2, Inc.  2015
 * @filesource
 * @version 0.0.1
 * @since 0.0.1
 */
class ParserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * The results of the last compare.  Will be zero count if no compare was executed, or if the compare was true.
     * Typically, used in PHPUnit testing.
     * @var string[]
     */
    private $resultsOfLastCompare = array();

    /**
     * If true, any files that are created during the test are automatically deleted; otherwise, they are left in place for further review.
     * @var bool
     */
    private $deleteFilesAfterCreating = true;

    /**
     * @test
     */
    public function test_amazon_1_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000001.html');
        $map = 'amazon_1_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('JLJ.GETTHIS Canon Speedlite 430EX II Flash for Canon Digital SLR Cameras');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-1144443-9473047'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41P8DvSdttL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 249.00));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('March 8, 2014')));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));

    }

    /**
     * @param string $filename The name of the file.
     * @param string $mapname The name of the map.
     * @return Result
     */
    private function parseSpecificFileAndMap($filename, $mapname)
    {

        // Provided good description for error.
        $workingfilename = Parser::changeWindowsSeparatorToSystem($this->getDirectoryPrefix() . "receipts\\$filename");
        $workingmapname = Parser::changeWindowsSeparatorToSystem($this->getDirectoryPrefix() . "maps\\$mapname");
        if ((!is_file($workingfilename)) || (!is_file($workingmapname))) {
            $errormsg = "\n\n******************************\n.=" . realpath('.') . "\n..=" . realpath('..') . "\nfilename=$workingfilename\nmapname=$workingmapname\n******************************\n\n";
            echo $errormsg;
        }


        $html = file_get_contents($workingfilename);
        $mapname = Parser::changeWindowsSeparatorToSystem($workingmapname);
        $maps = new FileMapRepository(array($mapname));
        $email_parser = new Parser($maps);
        $map = file_get_contents($mapname);
        $rc = $email_parser->parseHTMLAgainstSingleMap($html, $map);
        $rc->setFilename($filename);
        $rc->setMapused($mapname);
        return $rc;

    }

    /**
     * Get the directory prefix for the files (receipts) and maps.
     * @return string
     */
    private function getDirectoryPrefix()
    {
        return ".\\tests\\files\\";
    }

    /**
     * @param string $filename The name of the file.
     * @return Result
     */
    private function parseSpecificFileAgainstAllMaps($filename)
    {

        $html = file_get_contents(Parser::changeWindowsSeparatorToSystem($this->getDirectoryPrefix() . "receipts\\$filename"));
        $maps = new FileMapRepository(glob(__DIR__ . '/files/maps/*.json'));
        $email_parser = new Parser($maps);
        $rc = $email_parser->parseHTMLAgainstAllMaps($html);
        $rc->setFilename($filename);
        return $rc;

    }

    /**
     * Compare the returned assets with the expected assets.
     * @param Result $foundHeader
     * @param Result $expectedHeader
     * @returns boolean True if everything matches; false otherwise.
     */
    private function compareAssets($foundHeader, $expectedHeader)
    {

        $returnValue = $foundHeader->compare($expectedHeader);
        $this->resultsOfLastCompare = $foundHeader->getResultsOfLastCompare();
        return $returnValue;

    }

    /**
     * @test
     */
    public function test_amazon_1_00000002()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000002.html');
        $map = 'amazon_1_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('IBM Cognos 10 Framework Manager');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-7503440-5477022'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51LwfKNmAJL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 31.49));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('January 7, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));

    }

    /**
     * @test
     */
    public function test_amazon_1_00000011()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000011.html');
        $map = 'amazon_1_00000011.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Mr Selfridge: Season 1 TV Series [Blu-ray]');
        $wa->addAttribute(new Entity\Attribute('order_number', '111-1334005-3241066'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 24.95));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('April 8, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }

    /**
     * @test
     */
    public function test_amazon_1_00000012()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000012.html');
        $map = 'amazon_1_00000012.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Fontana(TM) Caramel Sauce, 63 fl oz');
        $wa->addAttribute(new Entity\Attribute('order_number', '111-4244119-2249863'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 21.95));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('April 8, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/31jgsnNugpL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }

    /**
     * @test
     */
    public function test_amazon_1_00000013()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000013.html');
        $map = 'amazon_1_00000013.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('3 x SmartBones Peanut Butter Dog Chew, Mini, 24-count');
        $wa->addAttribute(new Entity\Attribute('order_number', '111-4155247-5560228'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 13.69));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('April 11, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'AmazonSmile'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51cbDAdO1PL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_amazon_1_00000014()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_1_00000014.html');
        $map = 'amazon_1_00000014.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Bloomsz Southern High Bush Blueberries In Decorative Planter');
        $wa->addAttribute(new Entity\Attribute('order_number', '109-6672029-7706663'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 26.99));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/512wavisP8L._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_amazon_2_00000006()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_2_00000006.html');
        $map = 'amazon_4_00000005.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$map found $rc_count assets; should have found 2 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$file parsed, but found $rc_count assets; should have found 2 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Kurgo Door Guard, Khaki');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-9657058-4490625'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 22, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51Ui58AbHwL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 22.49));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Dog Car Seat Cover SUV Size Lrg (60"W) Color Sand');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-9657058-4490625'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 22, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/319magiRFIL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 39.95));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_amazon_2_00000007()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_2_00000007.html');
        $map = 'amazon_1_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$map found $rc_count assets; should have found 2 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$file parsed, but found $rc_count assets; should have found 2 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Training Kit (Exam 70-462): Administering Microsoft SQL Server 2012 Databases');
        $wa->addAttribute(new Entity\Attribute('order_number', '111-4662335-7334655'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('September 1, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51rTDV+JSZL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 41.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Training Kit (Exam 70-461): Querying Microsoft SQL Server 2012');
        $wa->addAttribute(new Entity\Attribute('order_number', '111-4662335-7334655'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('September 1, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51FNoP9xOeL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 43.79));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_amazon_2_00000008()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_2_00000008.html');
        $map = 'amazon_1_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$map found $rc_count assets; should have found 2 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$file parsed, but found $rc_count assets; should have found 2 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Mr Beams MB360 Wireless LED Spotlight with Motion Sensor and Photocell - Weatherproof - Battery Operated - 140 Lumens');
        $wa->addAttribute(new Entity\Attribute('order_number', '112-9711635-1539461'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('December 9, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/31m9UHF4mrL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 19.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Kingston Digital 240GB SSDNow V300 SATA 3 2.5 (7mm height) with Adapter Solid State Drive SV300S37A/240G');
        $wa->addAttribute(new Entity\Attribute('order_number', '112-9711635-1539461'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('December 9, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51yiwis4T+L._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 159.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * SPECIAL NOTE : This map requires ignoreChildCount and ignoreSiblingCount to be set to true.
     * @test
     */
    public function test_amazon_3_00000009()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_3_00000009.html');
        $map = 'amazon_3_00000009.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$map found $rc_count assets; should have found 3 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$file parsed, but found $rc_count assets; should have found 3 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Case Logic QHDC-101 Portable EVA Hard Drive Case - Black');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-1907318-1055405'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('December 30, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41DN8bzY7EL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 10.22));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('WD My Passport Ultra 1TB Portable External Hard Drive USB 3.0 with Auto and Cloud Backup - Blue (WDBZFP0010BBL-NESN)');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-1907318-1055405'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('December 30, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41V3a6BeTAL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 79.00));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Bluecell Micro USB OTG (On-the-go) USB 2.0 Cable for Cellphone/Tablet (Black color) Pack of 3');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-1907318-1055405'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('December 30, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/31Zp+TImauL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 6.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * NOTE: This test will fail if Parser->requireAllAttributesAtAllRepeats is set to true (i.e., strict mode).
     * @test
     */
    public function test_amazon_3_00000010()
    {
        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_3_00000010.html');
        $map = 'amazon_3_00000010.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$map found $rc_count assets; should have found 3 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$file parsed, but found $rc_count assets; should have found 3 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Speedliter\'s Handbook: Learning to Craft Light with Canon Speedlites');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-2854095-1773066'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('March 14, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51dqzTu5avL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 31.89));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('eneloop XX 2500mAh Typical / 2400 mAh Minimum, High Capacity, 8 Pack AA Ni-MH Pre-Charged Rechargeable Batteries');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-2854095-1773066'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('March 14, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/51G69o508CL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 30.24));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('La Crosse Technology BC-700 Alpha Power Battery Charger');
        $wa->addAttribute(new Entity\Attribute('order_number', '113-2854095-1773066'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('March 14, 2014')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41gJ5-yLA2L._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 29.53));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }

    /**
     * @test
     */
    public function test_amazon_4_00000005()
    {

        $file = Parser::changeWindowsSeparatorToSystem('amazon\\amazon_4_00000005.html');
        $map = 'amazon_4_00000005.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 4, "$map found $rc_count assets; should have found 4 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 4, "$file parsed, but found $rc_count assets; should have found 4 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Tiffen 67mm Circular Polarizer');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-8755401-7044222'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 2, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/31Mr4e0oNUL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 22.95));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Tiffen 67mm UV Protection Filter');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-8755401-7044222'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 2, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41kiHzOffNL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 6));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Canon Remote Switch RS60 E3');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-8755401-7044222'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 2, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/41QVWJJBECL._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 20.38));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Canon EW-73B Lens Hood For 17-85mm f/4-5.6 IS EF-S and 18-135mm f/3.5-5.6 IS Lenses');
        $wa->addAttribute(new Entity\Attribute('order_number', '110-8755401-7044222'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('June 2, 2013')));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Amazon.com'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://ecx.images-amazon.com/images/I/31o6aULeS4L._SCLZZZZZZZ__SY115_SX115_.jpg'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 19.89));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_bestbuy_1_00000002()
    {

        $file = Parser::changeWindowsSeparatorToSystem('best buy\\bestbuy_1_00000002.html');
        $map = 'bestbuy_3_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('PNY - 32GB Secure Digital High Capacity (SDHC) Class 10 Memory Card');
        $wa->addAttribute(new Entity\Attribute('order_number', 'Order number: BBY01-533690005830'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'BESTBUY.COM'));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_bestbuy_3_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('best buy\\bestbuy_3_00000001.html');
        $map = 'bestbuy_3_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$map found $rc_count assets; should have found 3 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$file parsed, but found $rc_count assets; should have found 3 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Canon - EOS Rebel T3i 18.0-Megapixel DSLR Camera with 18-135mm Lens - Black');
        $wa->addAttribute(new Entity\Attribute('order_number', 'Order number: BBY01-583705043826'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'BESTBUY.COM'));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Trax - 170 Camera Bag');
        $wa->addAttribute(new Entity\Attribute('order_number', 'Order number: BBY01-583705043826'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'BESTBUY.COM'));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('PNY - 8GB Secure Digital High Capacity (SDHC) Class 10 Memory Card');
        $wa->addAttribute(new Entity\Attribute('order_number', 'Order number: BBY01-583705043826'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'BESTBUY.COM'));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_costco_1_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\costco_1_00000001.html');
        $map = 'costco_1_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('$549.99 after $100 OFF Canon Vixia HF100 High Definition Camcorder 12x Optical 200x Digital Item# 283176');
        $wa->addAttribute(new Entity\Attribute('order_number', '41620794'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '2/22/2009'));
        $wa->addAttribute(new Entity\Attribute('quantity', 1));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 549.99));
        $wa->addAttribute(new Entity\Attribute('purchase_price_extended', 549.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_dicks_8_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\dicks_8_00000001.html');
        $map = 'dicks_8_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 8, "$map found $rc_count assets; should have found 8 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 8, "$file parsed, but found $rc_count assets; should have found 8 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('NEWDFSTRIP/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 55));
        $wa->addAttribute(new Entity\Attribute('sku_number', 886061855120));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('DFSTRETCHS/U');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 50));
        $wa->addAttribute(new Entity\Attribute('sku_number', 886915972928));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('NFLCOWBOYS/C');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 5.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', '095855511450'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('234100PACK/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 4.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', 822688496407));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('234100PACK/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 4.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', 822688496407));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('2012DTSOLO/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 19.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', '084984439520'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('2012DTSOLO/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 19.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', '084984439520'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('STJUDEDONA/N');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Dick\'s Sporting Goods'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '12/16/2013 03:20 PM'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 1.00));
        $wa->addAttribute(new Entity\Attribute('sku_number', 400000759944));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_itunes_2_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('itunes\\itunes_2_00000001.html');
        $map = 'itunes_2_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$map found $rc_count assets; should have found 2 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 2, "$file parsed, but found $rc_count assets; should have found 2 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->addAttribute(new Entity\Attribute('order_number', 'M1W3L01V5Y'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Apple Inc.'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('04/10/14')));
        $wa->addAttribute(new Entity\Attribute('payment_method', 'American Express .... 5000'));
        $wa->setName('Candy Crush Saga, Extra Moves      Report a Problem');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 0.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->addAttribute(new Entity\Attribute('order_number', 'M1W3L01V5Y'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Apple Inc.'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', strtotime('04/10/14')));
        $wa->addAttribute(new Entity\Attribute('payment_method', 'American Express .... 5000'));
        $wa->setName('Candy Crush Saga, Lollipop Hammer      Report a Problem');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 1.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_nike_3_00000002()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\nike_3_00000002.html');
        $map = 'nike_3_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$map found $rc_count assets; should have found 3 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$file parsed, but found $rc_count assets; should have found 3 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('NIKE TWIST STYLED WRAP');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 69.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('NEW ELEMENT 1/2 ZIP');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 44.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('SLIM POLY PRINCIPLE PANT');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 39.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_outlook_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('golf_galaxy\\outlook_00000001.txt');
        $map = 'outlook_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('50OFFLOFTA/N');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 15.99));
        $wa->addAttribute(new Entity\Attribute('sku_number', '400000245454'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '2/18/2014 4:12 PM'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_golfgalaxy_7_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('golf_galaxy\\golfgalaxy_7_00000001.html');
        $map = 'golfgalaxy_7_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 7, "$map found $rc_count assets; should have found 7 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 7, "$file parsed, but found $rc_count assets; should have found 7 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('THERMAFLEE/C');
        $wa->addAttribute(new Entity\Attribute('sku_number', '883153468916'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 65.00));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('ESSENTIALS/C');
        $wa->addAttribute(new Entity\Attribute('sku_number', '886081795673'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 60.00));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('ESSENTIALS/D');
        $wa->addAttribute(new Entity\Attribute('sku_number', '886081795604'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 60.00));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('THERMAFLEE/W');
        $wa->addAttribute(new Entity\Attribute('sku_number', '883153469159'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 65.00));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('13WINTERSO/N');
        $wa->addAttribute(new Entity\Attribute('sku_number', '667974597402'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 21.99));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('UAMATCHPLA/B');
        $wa->addAttribute(new Entity\Attribute('sku_number', '887907961692'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 79.99));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('STJUDEDONA/N');
        $wa->addAttribute(new Entity\Attribute('sku_number', '400000759944'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 5.00));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'Golf Galaxy'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', '11/17/2014 5:39 PM'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_visio_3_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\visio_3_00000001.html');
        $map = 'visio_3_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$map found $rc_count assets; should have found 3 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 3, "$file parsed, but found $rc_count assets; should have found 3 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('The Shining');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 19.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Carrie');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 7.99));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Pet Sematary');
        $wa->addAttribute(new Entity\Attribute('purchase_price', 10.99));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_barnesandnoble_1_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\barnesandnoble_1_00000001.html');
        $map = 'barnesandnoble_1_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Deadly Blessings (Alex St James Mystery Series #1)');
        $wa->addAttribute(new Entity\Attribute('image', 'http://img1.imagesbn.com/p/2940000891100_p0_v2_s114x166.JPG'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 4.99));
        $wa->addAttribute(new Entity\Attribute('order_number', '755744584'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', 'Order Date: April 17, 2014'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_paypal_1_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\paypal_1_00000001.html');
        $map = 'paypal_1_00000002.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$map found $rc_count assets; should have found 1 asset.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 1, "$file parsed, but found $rc_count assets; should have found 1 asset.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Foldover Clutch - Leopard-HairOn-FOC');
        $wa->addAttribute(new Entity\Attribute('purchase_date', ' Nov 20, 2013 07:25:58 PST'));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 210.00));
        $wa->addAttribute(new Entity\Attribute('order_number', '1WM05452GJ143042G'));
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'CLARE VIVIER LLCinfo@clarevivier.com'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * @test
     */
    public function test_staples_5_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\staples_5_00000001.html');
        $map = 'staples_5_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 5, "$map found $rc_count assets; should have found 5 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 5, "$file parsed, but found $rc_count assets; should have found 5 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Duracell Battery Quantum Alkaline AA 24Pack');
        $wa->addAttribute(new Entity\Attribute('order_number', '9707590399'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 22.99));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Master Casterreg Door Stop Giant Brown');
        $wa->addAttribute(new Entity\Attribute('order_number', '9707590399'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 8.24));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Master Casterreg Door Stop Brown');
        $wa->addAttribute(new Entity\Attribute('order_number', '9707590399'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 6.80));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Staples Bright White Multipurpose Paper 8 12 x 11 Case');
        $wa->addAttribute(new Entity\Attribute('order_number', '9707590399'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 39.99));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('CyberPower Intelligent LCD 1500VA 8Outlet UPS');
        $wa->addAttribute(new Entity\Attribute('order_number', '9707590399'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 149.99));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $expectedAssets[] = $wa;
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * In this instance, while there are 5 items on the receipt, 1 is a future order that cannot be processed.
     * @test
     */
    public function test_staples_4_00000001()
    {

        $file = Parser::changeWindowsSeparatorToSystem('unknown\\staples_4_00000001.html');
        $map = 'staples_4_00000001.json';
        $rc = $this->parseSpecificFileAndMap($file, $map);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 4, "$map found $rc_count assets; should have found 4 assets.");
        // ----------------------------------------------------------------
        $rc = $this->parseSpecificFileAgainstAllMaps($file);
        $rc_count = count($rc->getAssets());
        $this->assertTrue($rc_count === 4, "$file parsed, but found $rc_count assets; should have found 4 assets.");
        // ==================================================================
        $expectedHeader = new Result();
        $expectedAssets = array();
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Brighton Professional Wastebasket Black 7 gal');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'staples.com'));
        $wa->addAttribute(new Entity\Attribute('order_number', '9724276545'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', 'June 2, 2015'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 15.58));
        $wa->addAttribute(new Entity\Attribute('quantity', '2'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://www.staples-3p.com/s7/is/image/Staples/s0275838_sc7?$thb$'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Pilot G2 Retractable GelInk Pens Fine Point Blue Dozen');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'staples.com'));
        $wa->addAttribute(new Entity\Attribute('order_number', '9724276545'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', 'June 2, 2015'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 12.00));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://www.staples-3p.com/s7/is/image/Staples/s0025506_sc7?$thb$'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('Pilot G2 Retractable GelInk Pens Fine Point Black Dozen');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'staples.com'));
        $wa->addAttribute(new Entity\Attribute('order_number', '9724276545'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', 'June 2, 2015'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 12.00));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://www.staples-3p.com/s7/is/image/Staples/s0025985_sc7?$thb$'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wa = new Asset();
        $wa->setName('The Legal Pad Legal Rule White Perforated 50 SheetsPad 12 PadsPack 812 x 1134');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'staples.com'));
        $wa->addAttribute(new Entity\Attribute('order_number', '9724276545'));
        $wa->addAttribute(new Entity\Attribute('purchase_date', 'June 2, 2015'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 13.99));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://www.staples-3p.com/s7/is/image/Staples/s0748075_sc7?$thb$'));
        $expectedAssets[] = $wa;
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // This one does NOT exist (not parsed) because it is on back-order.
        /*
        $wa = new Asset();
        $wa->setName('Monoprice 109426 Mini DisplayPort Thunderbolt to HDMI Adapter Cable White');
        $wa->addAttribute(new Entity\Attribute('purchase_location', 'staples.com'));
        $wa->addAttribute(new Entity\Attribute('order_number', '9724276545'));
        $wa->addAttribute(new Entity\Attribute('purchase_price', 18.99));
        $wa->addAttribute(new Entity\Attribute('quantity', '1'));
        $wa->addAttribute(new Entity\Attribute('image', 'http://www.staples-3p.com/s7/is/image/Staples/m001420780_sc7?$thb$'));
        $expectedAssets[] = $wa;
        */
        // ==================================================================
        $expectedHeader->setAssets($expectedAssets);
        $comparison = $this->compareAssets($rc, $expectedHeader);
        $this->assertTrue($comparison, print_r($this->resultsOfLastCompare, true));
    }


    /**
     * Test all files against any and all maps.
     * @test
     */
    public function test_all_files_against_all_maps()
    {
        $maps = new FileMapRepository(glob(__DIR__ . '/files/maps/*.json'));
        $parser = new Parser($maps);
        $documents = new FileHtmlDocumentRepository(glob(__DIR__ . '/files/receipts/*/*.html'));

        foreach ($documents->all() as $document) {
            $result = $parser->parse($document);
            $count = count($result->getAssets());

            $this->assertGreaterThan(0, $count, "A receipt could not be processed.");
        }
    }


    /**
     * @test
     */
    public function test_writeSystemMap_00000001()
    {

        $json_contents =
            <<<HEREDOC
			{
  "header":{
    "vendor":"Amazon.com",
    "description":"amazon_1_00000001.json"
  },
  "map":[]
}
HEREDOC;

        $this->writeMap($json_contents);

    }

    /**
     * @param string $json_contents The map to write to a file.
     */
    private function writeMap($json_contents)
    {
        $maps = new FileMapRepository(array());
        $parser = new Parser($maps);


        // Test with a string.
        $writeok = $parser->storeMapToFile($json_contents);
        $this->assertFalse($writeok === false, "The string was not written to a new file (return=false).");
        if ($writeok !== false) {
            $this->assertTrue(file_exists($writeok), "The string was not written to a new file (! file_exists('$writeok')");
            $this->assertTrue(is_file($writeok), "The string was written, but '$writeok' is not a file.");
            $this->assertTrue(is_readable($writeok), "The string was written to a new file '$writeok', but the file is not readable.");
            if (is_file($writeok)) {
                if ($this->deleteFilesAfterCreating) {
                    unlink($writeok);
                    $this->assertFalse(file_exists($writeok), "The file '$writeok' was not successfully deleted.");
                }
            }
        }


    }

    /**
     * @test
     */
    public function test_writeSystemMap_00000002()
    {

        $json_contents =
            <<<HEREDOC
            			{
  "header":{
    "vendor":"",
    "description":""
  },
  "map":[]
}
HEREDOC;

        $this->writeMap($json_contents);

    }
}
