<?php

namespace ClearC2\AssetParser\Utils;

use DateTime;

/**
 * Created by PhpStorm.
 * User: JJurczewsky
 * Date: 7/15/2015
 * Time: 7:28 AM
 * @see http://www-12.lotus.com/ldd/doc/domino_notes/Rnext/help6_designer.nsf/f4b82fbb75e942a6852566ac0037f284/292f5b677fa6b3ce85256c54004e82d5?OpenDocument
 */
class AtFunctionsTest extends \PHPUnit_Framework_TestCase
{

    public function test_instantiation()
    {
        $at = new AtFunctions();

        $this->assertSame('ClearC2\AssetParser\Utils\AtFunctions', get_class($at));
    }

    public function test_abs()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(2.16, $at->abs(2.16), "abs(2.16) is not valid.");
        $this->assertSame(2.16, $at->abs(-2.16), "abs(-2.16) is not valid.");

        // Our tests
        $this->assertSame(array(2.16, 'a'), $at->abs(array(-2.16, 'a')), "abs(array(-2.16, 'a')) is not valid.");

    }

    public function test_adjust()
    {

        $at = new AtFunctions();

        // Lotus tests.
        // @Adjust(array(06/30/95);2;2;2;0;0;0)
        $this->assertSame($at->text($at->adjust($at->date(1995, 6, 30), 2, 2, 2)), $at->text($at->date(1997, 9, 2)), "test_adjust.001 is not valid.");
        // @Adjust(array(03/30/96);­2;0;­10;0;0;0)
        $this->assertSame($at->text($at->adjust($at->date(1996, 3, 30), -2, 0, -10)), $at->text($at->date(1994, 3, 20)), "test_adjust.002 is not valid.");

        // Our tests


    }

    /**
     * @test
     */
    public function test_char()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame("A", $at->char(65), "The charCode 65 did not convert to 'A'.");
        $this->assertSame("a", $at->char(97), "The charCode 97 did not convert to 'a'.");
        $this->assertSame("8", $at->char(56), "The charCode 56 did not convert to '8'.");

        // Our tests.
        $this->assertSame("\t", $at->char(9), 'The charCode 9 did not convert to a tab.');
        $this->assertSame("\t", $at->char(9.1), 'The charCode 9.1 did not convert to a tab.');
        $this->assertSame("\r", $at->char(13), 'The charCode 13 did not convert to a carriage return (\r).');
        $this->assertSame("\r", $at->char(13.9), 'The charCode 13.9 did not convert to a carriage return (\r).');
        $this->assertSame("\n", $at->char(10), 'The charCode 10 did not convert to a linefeed (\n).');
    }


    /**
     * @test
     */
    public function test_contains()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->contains('Hi There', 'Th'), "The substring 'Th' was not found in the string 'Hi There'");
        $this->assertTrue($at->contains(array('Tom', 'Dick', 'Harry'), array('Harry', 'Tom')), "The substring array('Harry', 'Tom') was not found in the string array('Tom', 'Dick', 'Harry')");
        $this->assertTrue($at->contains(array('Tom'), array('Tom', 'Dick', 'Harry')), "The substring array('Tom', 'Dick', 'Harry') was not found in the string array('Tom')");

        // Our tests.
        $this->assertTrue($at->contains('abc', 'a'), "The substring 'a' was not found in the string 'abc'");
        $this->assertTrue($at->contains('abc', 'b'), "The substring 'b' was not found in the string 'abc'");
        $this->assertTrue($at->contains('abc', 'c'), "The substring 'c' was not found in the string 'abc'");
        $this->assertFalse($at->contains('abc', 'C'), "The substring 'C' was found in the string 'abc'");

        $this->assertTrue($at->contains('abc', array('a', 'b')), "The substring array('a', 'b') was not found in the string 'abc'");
        $this->assertTrue($at->contains('abc', array('A', 'b')), "The substring array('A', 'b') was not found in the string 'abc'");
        $this->assertTrue($at->contains('abc', array('a', 'B')), "The substring array('a', 'B') was not found in the string 'abc'");
        $this->assertFalse($at->contains('abc', array('A', 'B')), "The substring array('A', 'B') was found in the string 'abc'");

        $this->assertTrue($at->contains(array('abcdef', 'dEf'), 'dEf'), "The substring 'dEf' was not found in the string array('abcdef', 'dEf')");
        $this->assertTrue($at->contains(array('abcdef', 'dEf'), array('abc', 'dEf')), "The substring array('abc', 'dEf') was not found in the string array('abcdef', 'dEf')");
        $this->assertTrue($at->contains(array('abcdef', 'dEf'), array('abc', 'def')), "The substring array('abc', 'def') was not found in the string array('abcdef', 'dEf')");
        $this->assertTrue($at->contains(array('abcdef', 'dEf'), array('xyz', 'def')), "The substring array('xyz', 'def') was not found in the string array('abcdef', 'dEf')");
        $this->assertFalse($at->contains(array('abcdef', 'dEf'), array('xyz', 'DeF')), "The substring array('xyz', 'DeF') was found in the string array('abcdef', 'dEf')");

        $this->assertTrue($at->contains('abc', ''), "The substring '' was not found in the string 'abc'");
        $this->assertFalse($at->contains('abc', ' '), "The substring ' ' was found in the string 'abc'");
        $this->assertTrue($at->contains('abc', array('def', '')), "The substring array('def','') was not found in the string 'abc'");
    }

    /**
     * @test
     */
    public function test_begins()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->begins('Hi There', 'Hi'), "The substring 'Hi' did not begin the string 'Hi There'");
        $this->assertFalse($at->begins('Hi There', 'hi'), "The substring 'hi' did begin the string 'Hi There'");

        // Our tests.
        $this->assertTrue($at->begins('abc', 'a'), "The substring 'a' did not begin the string 'abc'");
        $this->assertFalse($at->begins('abc', 'b'), "The substring 'b' did begin the string 'abc'");
        $this->assertFalse($at->begins('abc', 'c'), "The substring 'c' did begin the string 'abc'");

        $this->assertTrue($at->begins('abc', array('a', 'b')), "The substring array('a', 'b') did not begin the string 'abc'");
        $this->assertFalse($at->begins('abc', array('A', 'b')), "The substring array('A', 'b') did begin the string 'abc'");
        $this->assertTrue($at->begins('abc', array('a', 'B')), "The substring array('a', 'B') did not begin the string 'abc'");
        $this->assertFalse($at->begins('abc', array('A', 'B')), "The substring array('A', 'B') did begin the string 'abc'");

        $this->assertTrue($at->begins(array('abcdef', 'dEf'), 'dEf'), "The substring 'dEf' did not begin the string array('abcdef', 'dEf')");
        $this->assertTrue($at->begins(array('abcdef', 'dEf'), array('abc', 'dEf')), "The substring array('abc', 'dEf') did not begin the string array('abcdef', 'dEf')");
        $this->assertTrue($at->begins(array('abcdef', 'dEf'), array('abc', 'def')), "The substring array('abc', 'def') did not begin the string array('abcdef', 'dEf')");
        $this->assertFalse($at->begins(array('abcdef', 'dEf'), array('xyz', 'def')), "The substring array('xyz', 'def') did begin the string array('abcdef', 'dEf')");
        $this->assertFalse($at->begins(array('abcdef', 'dEf'), array('xyz', 'DeF')), "The substring array('xyz', 'DeF') did begin the string array('abcdef', 'dEf')");

        $this->assertTrue($at->begins('abc', ''), "The substring '' did not begin the string 'abc'");
        $this->assertFalse($at->begins('abc', ' '), "The substring ' ' did begin the string 'abc'");
        $this->assertTrue($at->begins('abc', array('def', '')), "The substring array('def','') did not being the string 'abc'");
    }

    /**
     * @test
     */
    public function test_ends()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->ends('Hi There', 're'), "The substring 're' did not end the string 'Hi There'");
        $this->assertFalse($at->ends('Hi There', 'The'), "The substring 'The' did end the string 'Hi There'");

        // Our tests.
        $this->assertTrue($at->ends('abc', 'c'), "The substring 'c' did not end the string 'abc'");
        $this->assertFalse($at->ends('abc', 'b'), "The substring 'b' did end the string 'abc'");
        $this->assertFalse($at->ends('abc', 'a'), "The substring 'a' did end the string 'abc'");

        $this->assertTrue($at->ends('abc', array('a', 'c')), "The substring array('a', 'c') did not end the string 'abc'");
        $this->assertFalse($at->ends('abc', array('A', 'b')), "The substring array('A', 'b') did end the string 'abc'");
        $this->assertTrue($at->ends('abc', array('c', 'A')), "The substring array('c', 'A') did not end the string 'abc'");
        $this->assertFalse($at->ends('abc', array('A', 'C')), "The substring array('A', 'C') did end the string 'abc'");

        $this->assertTrue($at->ends(array('abcdef', 'dEf'), 'dEf'), "The substring 'dEf' did not end the string array('abcdef', 'dEf')");
        $this->assertTrue($at->ends(array('abcdef', 'dEf'), array('abc', 'dEf')), "The substring array('abc', 'dEf') did not end the string array('abcdef', 'dEf')");
        $this->assertTrue($at->ends(array('abcdef', 'dEf'), array('abc', 'def')), "The substring array('abc', 'def') did not end the string array('abcdef', 'dEf')");
        $this->assertFalse($at->ends(array('abcdef', 'dEf'), array('xyz', 'abc')), "The substring array('xyz', 'abc') did end the string array('abcdef', 'dEf')");
        $this->assertFalse($at->ends(array('abcdef', 'dEf'), array('xyz', 'DeF')), "The substring array('xyz', 'DeF') did end the string array('abcdef', 'dEf')");

        $this->assertTrue($at->ends('abc', ''), "The substring '' did not end the string 'abc'");
        $this->assertFalse($at->ends('abc', ' '), "The substring ' ' did end the string 'abc'");
        $this->assertTrue($at->ends('abc', array('def', '')), "The substring array('def','') did not being the string 'abc'");
    }


    public function test_uppercase()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('ROBERT T. SMITH', $at->uppercase('Robert T. Smith'), "Uppercase 'Robert T. Smith' did not convert correctly.");


        // Our tests.
        $this->assertSame('A', $at->uppercase('a'), 'Uppercase a did not convert correctly.');
        $this->assertSame(array('A', 'B', 'C'), $at->uppercase(array('a', 'b', 'c')), "Uppercase array('a', 'b', 'c') did not convert correctly.");
        $this->assertSame(array('A', 'B', 'C', 7), $at->uppercase(array('a', 'b', 'c', 7)), "Uppercase array('a', 'b', 'c', 7) did not convert correctly.");
    }


    public function test_lowercase()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('juan mendoza', $at->lowercase('Juan Mendoza'), "Lowercase 'Juan Mendoza' did not convert correctly.");

        // Our tests.
        $this->assertSame('a', $at->lowercase('a'), 'Lowercase a did not convert correctly.');
        $this->assertSame(array('a', 'b', 'c'), $at->lowercase(array('a', 'b', 'c')), "Lowercase array('a', 'b', 'c') did not convert correctly.");
        $this->assertSame(array('a', 'b', 'c', 7), $at->lowercase(array('a', 'b', 'c', 7)), "Lowercase array('a', 'b', 'c', 7) did not convert correctly.");
    }


    public function test_propercase()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Every Child Loves Toys', $at->propercase('every CHILD LOves toys'), "propercase('every CHILD LOves toys') did not convert correctly.");
//		$this->assertSame('3Digit Code', $at->propercase('3digit code'), "propercase('3digit code') did not convert correctly.");
        $this->assertSame('Los Angeles', $at->propercase('lOS anGeles'), "propercase('lOS anGeles') did not convert correctly.");

        // Our tests.
        $this->assertSame('A', $at->propercase('a'), 'propercase a did not convert correctly.');
        $this->assertSame(array('The Quick Brown', 'Fox Runs Quite', 'Quickly For A Fox.'), $at->propercase(array('The quick BROWN', 'fOx runs quitE', 'QUickly for a foX.')), "propercase(array('The quick BROWN', 'fOx runs quitE', 'QUickly for a foX.')) did not convert correctly.");
        $this->assertSame(array('The Quick Brown', 'Fox Runs Quite', 'Quickly For A Fox.', 7), $at->propercase(array('The quick BROWN', 'fOx runs quitE', 'QUickly for a foX.', 7)), "propercase(array('The quick BROWN', 'fOx runs quitE', 'QUickly for a foX.')) did not convert correctly.");
    }


    public function test_count()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(3, $at->count(array('Moe', 'Larry', 'Curly')));

        // Our tests.
        $this->assertSame(1, $at->count(''));
        $this->assertSame(2, $at->count(array('', 7)));
        $this->assertSame(3, $at->count(array('', 7, null)));
    }

    public function test_compare()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array(-1, 1, -1, 0, 0), $at->compare(array('Boston', 'Tokyo', 'Moscow', 'N', 'n'), 'N', false), 'test_compare.0001 did not compare correctly.');

        // Our tests.
        $this->assertSame(array(-1, 1, -1, 0, 1), $at->compare(array('Boston', 'Tokyo', 'Moscow', 'N', 'n'), 'N', true), 'test_compare.1001 did not compare correctly.');
        $this->assertSame(-1, $at->compare('A', 'B'), 'test_compare.1002 did not compare correctly.');
        $this->assertSame(0, $at->compare('B', 'B'), 'test_compare.1003 did not compare correctly.');
        $this->assertSame(1, $at->compare('C', 'B'), 'test_compare.1004 did not compare correctly.');
    }


    public function test_matches()
    {
        $at = new AtFunctions();

        // Lotus tests.
        // Not used, as this function is NOT the exact same.


        // Our tests.
        // This example returns 0. The underscore matches only a single character.
        $this->assertSame(false, $at->matches('A big test', 'A.test'), 'test_matches.0001 did not compare correctly.');

        // This example returns 1. The five underscores match "<space>big<space>."
        $this->assertSame(true, $at->matches('A big test', 'A.....test'), 'test_matches.0002 did not compare correctly.');
        $this->assertSame(false, $at->matches('A big test', 'A....test'), 'test_matches.0002.a did not compare correctly.');
        $this->assertSame(true, $at->matches('A big test', '^A.*test$'), 'test_matches.0002.b did not compare correctly.');

        // This example returns 1. The % matches "A big ."
        $this->assertSame(true, $at->matches('A big test', '.*test'), 'test_matches.0003 did not compare correctly.');

        // This example returns 0. @matches is case-sensitive.
        $this->assertSame(false, $at->matches('A big test', 'A BIG test'), 'test_matches.0004 did not compare correctly.');

    }


    public function test_sum()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(3, $at->sum(array(1, 2)), 'test_sum.0001 did not compare correctly.');
        $this->assertSame(11, $at->sum(array(-1, 2), array(-10, 20)), 'test_sum.0002 did not compare correctly.');
        $this->assertSame(50, $at->sum(array(5, 10, 15, 20)), 'test_sum.0003 did not compare correctly.');


        // Our tests.
        $this->assertSame(3.14, $at->sum(array(1, 2.14)), 'test_sum.0004 did not compare correctly.');
        $this->assertSame(3.14, $at->sum(array(1.14, 2.0)), 'test_sum.0005 did not compare correctly.');
        $this->assertSame(5.5, $at->sum(1.1, 1.1, 1.1, 1.1, 1.1), 'test_sum.0006 did not compare correctly.');

    }


    public function test_text()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('123.45', $at->text(123.45, 'F2'), 'test_text.001 failed.');
        $this->assertSame('$800.00', $at->text(800, 'C,2'), 'test_text.002 failed.');
        $this->assertSame('$8,123.46', $at->text(8123.456, 'C,'), 'test_text.003 failed.');
        $this->assertSame('3.142e+0', $at->text(3.14156, 'S3'), 'test_text.004 failed.');


        // Our tests.

    }


    public function test_texttonumber()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(123, $at->texttonumber('123'), 'test_texttonumber.0001 did not compare correctly.');


        // Our tests.
        $this->assertSame(array(123, 456), $at->texttonumber(array('123', 456)), 'test_texttonumber.0002 did not compare correctly.');
        $this->assertSame(12, $at->texttonumber('12ABC'), 'test_texttonumber.0003 did not compare correctly.');
        $this->assertSame(0, $at->texttonumber('ABC12'), 'test_texttonumber.0004 did not compare correctly.');
        $this->assertSame(-3.14, $at->texttonumber('-3.1400'), 'test_texttonumber.0005 did not compare correctly.');
    }


    public function test_texttotime()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('1990-08-10 02:40:00', $at->canonicaldate($at->texttotime('8/10/90 2:40')), 'test_texttotime.0001 did not compare correctly.');
        $this->assertSame(array('1990-08-10 02:40:00', '2015-07-29 16:31:00'), $at->canonicaldate($at->texttotime(array('8/10/90 2:40', '7/29/2015 4:31 PM'))), 'test_texttotime.0002 did not compare correctly.');


        // Our tests.
        $this->assertSame(false, $at->texttotime('This is a bad string.'), 'test_texttotime.0002 did not compare correctly.');
    }


    public function test_elements()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(4, $at->elements(array('Rogers', 'Binney', 'Harris', 'Larson')));
        $this->assertSame(2, $at->elements(array('Jones', 'Portsmore')));
        $this->assertSame(5, 3 + $at->elements(array('Liston', 'Reed')));

        // Our tests.
        $this->assertSame(0, $at->elements(''));
        $this->assertSame(2, $at->count(array('', 7)));
        $this->assertSame(3, $at->count(array('', 7, null)));
    }


    public function test_date()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(strtotime('1995-06-23'), $at->date(1995, 6, 23)->getTimestamp(), 'The date 6/23/1995 did not convert correctly.');
//		$this->assertSame(strtotime('95-06-23'), $at->date(95, 6, 23)->getTimestamp(), 'The date 6/23/0095 did not convert correctly.');
        $this->assertSame(strtotime('2095-06-23'), $at->date(2095, 6, 23)->getTimestamp(), 'The date 6/23/2095 did not convert correctly.');
        $this->assertSame(strtotime('1993-01-20 8:58:12'), $at->date(1993, 01, 20, 8, 58, 12)->getTimestamp(), 'The date 1/20/1993 8:58:12 did not convert correctly.');


        // Our tests.
        $this->assertSame('DateTime', get_class($at->date(2015, 7, 15)), '$at->date(2015, 7, 15) did not return the correct DateTime class.');
        $this->assertSame('DateTime', get_class($at->date(2015, 7, 15, 3, 30, 59)), '$at->date(2015, 7, 15, 3, 30, 59) did not return the correct DateTime class.');

        $this->assertSame(strtotime('2015-7-15'), $at->date(2015, 7, 15)->getTimestamp(), 'The date 7/15/2015 did not convert correctly.');
        $this->assertSame(strtotime('2015-7-15 3:30:59'), $at->date(2015, 7, 15, 3, 30, 59)->getTimestamp(), 'The date 7/15/2015 3:30:59 did not convert correctly.');
        $this->assertSame(strtotime('2015-7-15 13:30:59'), $at->date(2015, 7, 15, 13, 30, 59)->getTimestamp(), 'The date 7/15/2015 13:30:59 did not convert correctly.');
    }

    public function test_year()
    {
        $at = new AtFunctions();

        // Lotus tests.


        // Our tests.
        $this->assertSame(2015, $at->year($at->date(2015, 7, 15)), 'Year for 7/15/2015 is incorrect; should be 2015.');
        $this->assertSame(2014, $at->year($at->date(2014, 2, 28)), 'Year for 2/28/2014 is incorrect; should be 2014.');
        $this->assertSame(1990, $at->year($at->date(1990, 2, 29)), 'Year for 2/29/1990 is incorrect; should be 1990.');
    }

    public function test_month()
    {
        $at = new AtFunctions();

        // Lotus tests.


        // Our tests.
        $this->assertSame(7, $at->month($at->date(2015, 7, 15)), 'Month for 7/15/2015 is incorrect; should be 7.');
        $this->assertSame(2, $at->month($at->date(2015, 2, 28)), 'Month for 2/28/2015 is incorrect; should be 2.');
        $this->assertSame(3, $at->month($at->date(2015, 2, 29)), 'Month for 2/29/2015 is incorrect; should be 3.');
    }

    public function test_day()
    {
        $at = new AtFunctions();

        // Lotus tests.


        // Our tests.
        $this->assertSame(15, $at->day($at->date(2015, 7, 15)), 'Day of month for 7/15/2015 is incorrect; should be 15.');
        $this->assertSame(28, $at->day($at->date(2015, 2, 28)), 'Day of month for 2/28/2015 is incorrect; should be 28.');
        $this->assertSame(1, $at->day($at->date(2015, 2, 29)), 'Day of month for 2/29/2015 is incorrect; should be 1.');
    }

    public function test_member()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(0, $at->member('Sales', array('Finance', 'Service', 'Legal')), "member('Sales', array('Finance', 'Service', 'Legal')) is incorrect; should be 0.");
        $this->assertSame(5, $at->member('Sales', array('Finance', 'Service', 'Legal', 'sales', 'Sales')), "member('Sales', array('Finance', 'Service', 'Legal', 'sales', 'Sales')) is incorrect; should be 5.");

        // Our tests.

    }

    public function test_evalex()
    {
        $at = new AtFunctions();

        // Lotus tests.


        // Our tests.
        $this->assertSame("abcd", $at->evalex("return 'ab' . 'cd';"), 'test_evalex.0001 did not compare correctly.');

        // Because nothing was returned, the result will be a null.
        $this->assertNull($at->evalex("'ab' . 'cd';"), 'test_evalex.0002 did not compare correctly.');

        // Because I forgot the semi-colon to end the statement, I get a false (parsing error).
        $this->assertFalse($at->evalex("'ab' . 'cd'"), 'test_evalex.0003 did not compare correctly.');

    }

    public function test_explode()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode('Weekly Status Report'), "explode('Weekly Status Report') did not explode correctly.");
        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode('Weekly,Status,Report'), "explode('Weekly,Status,Report') did not explode correctly.");
        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode('Weekly;Status;Report'), "explode('Weekly;Status;Report') did not explode correctly.");
        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode("Weekly\nStatus\nReport"), "explode('Weekly\\nStatus\\nReport') did not explode correctly.");


        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode('Weekly+Status+Report', '+&'), "explode('Weekly+Status+Report', '+&') did not explode correctly.");
        $this->assertSame(array('Weekly', 'Status', 'Report'), $at->explode("Weekly\nStatus\nReport", '+&'), "explode('Weekly\\nStatus\\nReport', '+&') did not explode correctly.");

        $this->assertSame('Please send resume + references', $at->implode($at->explode('Please send resume & references', '&'), '+'), "explode('Please send resume & references', '&') did not explode correctly.");
        $this->assertSame(array('Attendance grows at UCLA', ' Pomona Colleges', ' and USC'), $at->explode('Attendance grows at UCLA, Pomona Colleges, and USC', ','), "explode('Attendance grows at UCLA, Pomona Colleges, and USC', ',') did not explode correctly.");
        $this->assertSame(4, $at->elements($at->explode('Mexico, Guatemala, Costa Rica, El Salvador', ',')), "elements(explode('Mexico, Guatemala, Costa Rica, El Salvador', ',')) did not explode correctly.");

        // Our tests.


    }

    public function test_floateq()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(true, $at->floateq(100.00001, 100.00002), 'test_floateq.0001 did not compare correctly.');

    }

    public function test_getcurrenttimezone()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $realtimezone = date_default_timezone_get();


        date_default_timezone_set('America/Chicago');
        $this->assertSame('America/Chicago', $at->getcurrenttimezone(), 'test_getcurrenttimezone.0001 did not compare correctly.');

        // This could be CDT OR CST, depending on the time of the year, so we skip the test.
        // $this->assertSame('CDT', $at->getcurrenttimezone(true), 'test_getcurrenttimezone.0002 did not compare correctly.');


        date_default_timezone_set($realtimezone);
    }


    /**
     * This can NOT be tested from PHPUnit, as it requires a HTTP connection.
     */
    public function ignore_test_gethttpheader()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        // Will work if User-Agent = Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36
        $this->assertTrue($at->contains($at->gethttpheader('User-Agent'), 'Mozilla/5.0'), 'test_gethttpheader.0001 did not compare correctly.');

    }

    /**
     * This can NOT be tested from PHPUnit, as it will differ for every machine.
     */
    public function ignore_test_platform()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('WINNT', $at->platform(), 'test_platform.0001 did not compare correctly.');
        $this->assertSame('Windows NT', $at->platform('s'), 'test_platform.0002 did not compare correctly.');
        $this->assertSame('JJURCZEWSKY', $at->platform('n'), 'test_platform.0003 did not compare correctly.');
        $this->assertSame('6.1', $at->platform('r'), 'test_platform.0004 did not compare correctly.');
        $this->assertSame('build 7601 (Windows 7 Professional Edition Service Pack 1)', $at->platform('v'), 'test_platform.0005 did not compare correctly.');
        $this->assertSame('i586', $at->platform('m'), 'test_platform.0006 did not compare correctly.');
        $this->assertSame('Windows NT JJURCZEWSKY 6.1 build 7601 (Windows 7 Professional Edition Service Pack 1) i586', $at->platform('a'), 'test_platform.0007 did not compare correctly.');

        $this->assertSame('Windows NT 6.1 i586', $at->platform('srm'), 'test_platform.0008 did not compare correctly.');

    }

    /**
     * This can NOT be tested from PHPUnit, as it will differ for every machine/host.
     */
    public function ignore_test_servername()
    {

        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('JJURCZEWSKY', $at->servername(), 'test_servername.0001 did not compare correctly.');
    }


    public function test_getenvironment()
    {

        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $at->setenvironment('foo', 'bar');
        $this->assertSame('bar', $at->getenvironment('foo'), 'test_getenvironment.0001 did not compare correctly.');
    }


    public function test_setenvironment()
    {

        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $at->setenvironment('foo', 'bar');
        $this->assertSame('bar', $at->getenvironment('foo'), 'test_setenvironment.0001 did not compare correctly.');
    }


    public function test_soundex()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('F430', $at->soundex('field'), 'test_soundex.0001 did not compare correctly.');
        $this->assertSame('P430', $at->soundex('phield'), 'test_soundex.0002 did not compare correctly.');

        // Our tests.
        $this->assertSame('P430', $at->soundex('pheeld'), 'test_soundex.0003 did not compare correctly.');
        $this->assertSame('F430', $at->soundex('feeled'), 'test_soundex.0003 did not compare correctly.');

    }


    public function test_urldecode()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Employee/My Database.nsf', $at->urldecode('Employee%2FMy%20Database.nsf'), 'test_urldecode.0001 did not compare correctly.');

        // Our tests.
    }


    public function test_urlencode()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('By+Date', $at->urlencode('By Date'), 'test_urlencode.0001 did not compare correctly.');

        // Our tests.
        $this->assertSame('By Date', $at->urldecode($at->urlencode('By Date')), 'test_urlencode.0002 did not compare correctly.');
    }


    public function test_urlquerystring()
    {

        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(array(), $at->urlquerystring(), 'test_urlquerystring.0001 did not compare correctly.');
    }


    public function test_clienttype()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('CLI', $at->clienttype(), 'test_clienttype.0001 did not compare correctly.');

    }


    /**
     * This can NOT be tested, as it yields different results for different installs.
     * The test shown does work on James' machine.
     */
    public function ignore_test_configfile()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('c:\\php\\php.ini', $at->lowercase($at->configfile()), 'test_configfile.0001 did not compare correctly.');
    }


    /**
     * This can NOT be tested, as it yields different results for different installs.
     * The test shown does work on James' machine.
     */
    public function ignore_test_dbname()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('jjurczewsky::c:\\oraclelinuxvm\\thelogue\\assetparser\\src\\utils\\atfunctions.php', $at->lowercase($at->dbname()), 'test_dbname.0001 did not compare correctly.');
    }


    /**
     * This can NOT be tested, as it yields different results for different installs.
     * The test shown does work on James' machine.
     */
    public function ignore_test_fildir()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('c:\\oraclelinuxvm\\thelogue\\assetparser\\src\\utils', $at->lowercase($at->filedir()), 'test_fildir.0001 did not compare correctly.');
    }


    public function test_hashpassword()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('cdab5139720ecf113de47ab4cb3becd7', $at->hashpassword('The quick brown fox jump over the lazy dog.'), 'test_hashpassword.0001 did not compare correctly.');
        $this->assertSame('5100eb0cd4953de04770bbe239dfc0d1e22aa9ecd33a42cd837a5a8fd37ad782390d2dfb6222daa77c558429cb08b2839ffc514121fcf9ff3334abcb29807628', $at->hashpassword('The quick brown fox jump over the lazy dog.', 'sha512'), 'test_hashpassword.0002 did not compare correctly.');
    }


    public function test_implode()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Minneapolis Detroit Chicago', $at->implode(array('Minneapolis', 'Detroit', 'Chicago')), "implode(array('Minneapolis', 'Detroit', 'Chicago')) did not implode correctly.");
        $this->assertSame('Minneapolis,Detroit,Chicago', $at->implode(array('Minneapolis', 'Detroit', 'Chicago'), ','), "implode(array('Minneapolis', 'Detroit', 'Chicago'), ',') did not implode correctly.");
        $this->assertSame('Bonn : Lisbon : Madrid', $at->implode(array('Bonn', 'Lisbon', 'Madrid'), ' : '), "implode(array('Bonn', 'Lisbon', 'Madrid'), ' : ') did not implode correctly.");

        // Our tests.

    }


    public function test_hour()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(9, $at->hour($at->date(2015, 7, 15, 9, 30)), "The correct hour 9 was not returned from the date 2015-7-15 9:30");
        $this->assertSame(8, $at->hour($at->date(1990, 7, 30, 20, 56, 34)), "The correct hour 8 was not returned from the date 1990-7-30 20:56:34");
        $this->assertSame(3, $at->hour($at->date(1992, 2, 15, 3, 00, 12)), "The correct hour 3 was not returned from the date 1992-2-15 3:00:12");

        // Our tests.

    }

    public function test_isappinstalled()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(true, $at->isappinstalled('xml'), 'test_isappinstalled.0001 did not compare correctly.');
        $this->assertSame(true, $at->isappinstalled('zip'), 'test_isappinstalled.0002 did not compare correctly.');

    }


    public function test_minute()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(30, $at->minute($at->date(2015, 7, 15, 9, 30)), "The correct minute 30 was not returned from the date 2015-7-15 9:30");
        $this->assertSame(59, $at->minute($at->date(1995, 7, 30, 9, 59, 59)), "The correct minute 58 was not returned from the date 1995-7-30 9:59:59");
        $this->assertSame(0, $at->minute($at->date(1995, 9, 29, 3, 0, 12)), "The correct minute 0 was not returned from the date 1995-9-29 3:00:12");

        // Our tests.

    }

    public function test_passwordquality()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(1, $at->passwordquality(''), 'test_passwordquality.0001 did not compare correctly.');
        $this->assertSame(1, $at->passwordquality('good'), 'test_passwordquality.0002 did not compare correctly.');
        $this->assertSame(7, $at->passwordquality('The quick brown fox jump over the lazy dog.'), 'test_passwordquality.0003 did not compare correctly.');

    }


    public function test_reqqueryvalue()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        // This test only works with James' machine.
        // $this->assertSame('c:\\android\\sdk', $at->lowercase($at->regqueryvalue('HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet002\\Control\\Session Manager\\Environment\\ANDROID_HOME')), 'test_reqqueryvalue.0001 did not compare correctly.');
        $this->assertSame(7.3, $at->lowercase($at->regqueryvalue('HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet002\\Control\\Session Manager\\Environment\\NOWAYNOHOW', 7.3)), 'test_reqqueryvalue.0002 did not compare correctly.');
        $this->assertInstanceOf('\\Exception', $at->regqueryvalue('HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet002\\Control\\Session Manager\\Environment\\NOWAYNOHOW', '', true), 'test_reqqueryvalue.0003 did not compare correctly.');

    }


    public function test_like()
    {
        $at = new AtFunctions();


        // Lotus tests.
        // @Like( "A big test" ; "A_test" )
        $this->assertSame(false, $at->like('A big test', 'A_test'), 'test_like.0001 did not compare correctly.');
        // @Like( "A big test" ; "A_____test" )
        $this->assertSame(true, $at->like('A big test', 'A_____test'), 'test_like.0002 did not compare correctly.');
        // @Like( "A big test" ; "%test" )
        $this->assertSame(true, $at->like('A big test', '%test'), 'test_like.0003 did not compare correctly.');
        // @Like( "A big test" ; "%test" )
        $this->assertSame(false, $at->like('A big test', 'A BIG test'), 'test_like.0004 did not compare correctly.');
        // @Like( "A 100% improvement" ; "A %/% improv%" ; "/" )
        $this->assertSame(true, $at->like('A 100% improvement', 'A %/% improv%', '/'), 'test_like.0005 did not compare correctly.');

        // Our tests.
        $this->assertSame(true, $at->like('A 100% improvement', 'A %/% improv_____', '/'), 'test_like.0006 did not compare correctly.');
        $this->assertSame(true, $at->like('A 100% improvement.', 'A %/% improv_____.', '/'), 'test_like.0007 did not compare correctly.');
        $this->assertSame(true, $at->like('This isn\'t w^orking!', 'This isn_t w_orking%'), 'test_like.0008 did not compare correctly.');
        $this->assertSame(true, $at->like('This isn\'t w^orking!', 'This isn_t w^orking%'), 'test_like.0009 did not compare correctly.');
        $this->assertSame(true, $at->like('This ({array($isn\'t w^orking)})!', 'This ({array($isn_t w^orking)})_'), 'test_like.0010 did not compare correctly.');
        $this->assertSame(true, $at->like('This? ({array($isn\'t w^orking)})!', 'This? ({array($isn_t w^orking)})_'), 'test_like.0011 did not compare correctly.');
        $this->assertSame(true, $at->like('This?+^A ({array($isn\'t w^orking)})!', 'This?+^A ({array($isn_t w^orking)})_'), 'test_like.0012 did not compare correctly.');
        $this->assertSame(true, $at->like('This *.sentence.* ?+^A ({array($isn\'t w^orking)})!', 'This_*.sentence.*_?+^A ({array($isn_t w^orking)})_'), 'test_like.0013 did not compare correctly.');

        // Because we want to include the backslash as a normal character, we must use a different escape character.
        $this->assertSame(true, $at->like('\\word\\', '\\\\%\\\\'), 'test_like.0014 did not compare correctly.');
        $this->assertSame(true, $at->like('This \\*.sentence.*\\ ?+^A ({array($isn\'t w^orking)})!', 'This_\\\\*.sentence.*\\\\_?+^A ({array($isn_t w^orking)})_'), 'test_like.0015 did not compare correctly.');
        $this->assertSame(true, $at->like('\\word\\', '\\%\\', '`'), 'test_like.0016 did not compare correctly.');
        $this->assertSame(true, $at->like('This \\*.sentence.*\\ ?+^A ({array($isn\'t w^orking)})!', 'This_\\*.sentence.*\\_?+^A ({array($isn_t w^orking)})_', '`'), 'test_like.0017 did not compare correctly.');

    }


    public function test_second()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(45, $at->second($at->date(2015, 7, 15, 9, 30, 45)), "The correct second 45 was not returned from the date 2015-7-15 9:30:45");
        $this->assertSame(0, $at->second($at->date(1995, 7, 30, 9, 59, 0)), "The correct second 0 was not returned from the date 1995-7-30 9:59:0");
        $this->assertSame(12, $at->second($at->date(1995, 9, 29, 3, 0, 12)), "The correct second 12 was not returned from the date 1995-9-29 3:00:12");

        // Our tests.

    }

    public function test_integer()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array(123, 789), $at->integer(array(123.001, 789.999)), "integer(array(123.001, 789.999)) was not converted correctly");
        $this->assertSame(array(127580, 5, 7341), $at->integer(array(127580.35, 5.75, 7341.62015)), "integer(array(127580.35, 5.75, 7341.62015)) was not converted correctly");
        $this->assertSame(3, $at->integer(3.12), "integer(3.12) was not converted correctly");
        $this->assertSame(6, $at->integer(6.735), "integer(6.735) was not converted correctly");

        // Our tests.
    }


    public function test_ismember()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->ismember('computer', array('printer', 'computer', 'monitor')), "ismember('computer', array('printer', 'computer', 'monitor')) did not return true.");
        $this->assertFalse($at->ismember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor')), "ismember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor')) did not return false.");
        $this->assertTrue($at->ismember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor', 'computer')), "ismember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor', 'computer')) did not return true.");
        $this->assertTrue($at->ismember('Fred', array('Barney', 'Wilam', 'Fred')), "ismember('Fred', array('Barney', 'Wilam', 'Fred')) did not return true.");

        // Our tests.

    }

    public function test_isnotmember()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertFalse($at->isnotmember('computer', array('printer', 'computer', 'monitor')), "isnotmember('computer', array('printer', 'computer', 'monitor')) did not return false.");

        // Our tests.
        $this->assertTrue($at->isnotmember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor')), "isnotmember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor')) did not return true.");
        $this->assertFalse($at->isnotmember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor', 'computer')), "isnotmember(array('computer', 'Notes'), array('Notes', 'printer', 'monitor', 'computer')) did not return false.");
        $this->assertFalse($at->isnotmember('Fred', array('Barney', 'Wilam', 'Fred')), "isnotmember('Fred', array('Barney', 'Wilam', 'Fred')) did not return false.");

    }


    public function test_exp()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(1.0, $at->exp(0), "exp(0) failed.");
        $this->assertSame(3.49034295746184, $at->exp(1.25), "exp(1.25) failed.");
        $this->assertSame(0.28650479686019, $at->exp(-1.25), "exp(-1.25) failed.");
    }

    public function test_ln()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(0.693147180559945, $at->ln(2), "ln(2) failed.");

        // Our tests.
    }


    public function test_log()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(0.602059991327962, $at->log(4), "log(4) failed.");

        // Our tests.
    }

    public function test_power()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(8, $at->power(2, 3), "power(2, 3) failed.");
        $this->assertSame(-8, $at->power(-2, 3), "power(-2, 3) failed.");
        $this->assertSame(0.125, $at->power(2, -3), "power(2, -3) failed.");

        // Our tests.
    }

    public function test_sqrt()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(4.0, $at->sqrt(16), "sqrt(16) failed.");

        // Our tests.
        $this->assertSame(array(4.0, 4.0), $at->sqrt(array(16, 16)), "sqrt(16) failed.");
    }


    public function test_isnumber()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->isnumber(123), "isnumber(123) did not return true.");
        $this->assertFalse($at->isnumber(new DateTime()), "isnumber(new DateTime()) did not return false.");
        $this->assertTrue($at->isnumber(array(345, 2.78, 997, 0.7)), "isnumber(array(345, 2.78, 997, 0.7)) did not return true.");
        $this->assertFalse($at->isnumber(array(345, 2.78, 997, 0.7, 'hello there')), "isnumber(array(345, 2.78, 997, 0.7, 'hello there')) did not return false.");

        // Our tests.

    }

    public function test_max()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(3, $at->max(array(1, 3)), 'test_max.0001 did not compare correctly.');
        $this->assertSame(array(99, 6, 7, 8), $at->max(array(99, 2, 3), array(5, 6, 7, 8)), 'test_max.0002 did not compare correctly.');
        $this->assertSame(array(-2, 45, 54), $at->max(array(-2.6, 45, -25), array(-2, -50, 54)), 'test_max.0003 did not compare correctly.');
        $this->assertSame(99, $at->max(array(99, 2, 3)), 'test_max.0004 did not compare correctly.');

        // Our tests.
        $this->assertSame(array(99, 6, 7, 3), $at->max(array(99, 2, 3), array(5, 6, 7, -8)), 'test_max.0005 did not compare correctly.');

    }


    public function test_min()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(35, $at->min(array(35, 100)), 'test_min.0001 did not compare correctly.');
        // @Min(99:2:3;5:6:7:8)
        $this->assertSame(array(5, 2, 3, 3), $at->min(array(99, 2, 3), array(5, 6, 7, 8)), 'test_min.0002 did not compare correctly.');
        // @Min((-3.5):(-35):100;(-2):45:54)
        $this->assertSame(array(-3.5, -35, 54), $at->min(array(-3.5, -35, 100), array(-2, 45, 54)), 'test_min.0003 did not compare correctly.');
        $this->assertSame(2, $at->min(array(99, 2, 3)), 'test_min.0004 did not compare correctly.');

        // Our tests.
        $this->assertSame(array(5, 2, 3, -8), $at->min(array(99, 2, 3), array(5, 6, 7, -8)), 'test_min.0005 did not compare correctly.');

    }


    public function test_getfield()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(null, $at->getfield('doesnotexist'), 'test_getfield.0001 did not compare correctly.');

        $at->setfield('myfieldname', 3);
        $this->assertSame(3, $at->getfield('myfieldname'), 'test_getfield.0002 did not compare correctly.');

        $at->setfield('myfieldname', array(3, 4, 5));
        $this->assertSame(array(3, 4, 5), $at->getfield('myfieldname'), 'test_getfield.0003 did not compare correctly.');

        $at->setfield('myfieldname', array('A string', 3, 4, 5));
        $this->assertSame(array('A string', 3, 4, 5), $at->getfield('myfieldname'), 'test_getfield.0004 did not compare correctly.');

        $at->setfield(7, array('A string', 3, 4, 5, 6));
        $this->assertSame(array('A string', 3, 4, 5, 6), $at->getfield(7), 'test_getfield.0005 did not compare correctly.');
        $at->setfield('myfieldname', array('A string', 3, 4, 5));
        $at->clearfield();
        $this->assertSame(null, $at->getfield(7), 'test_getfield.0006a did not compare correctly.');
        $this->assertSame(null, $at->getfield('myfieldname'), 'test_getfield.0006b did not compare correctly.');

        $at->setfield('myfieldname', 3);
        $this->assertSame(3, $at->getfield('myfieldname'), 'test_getfield.0007 did not compare correctly.');
        $at->clearfield('myfieldname');
        $this->assertSame(null, $at->getfield('myfieldname'), 'test_getfield.0008 did not compare correctly.');

        $at->clearfield();
        $at->setfield('k1', 'v1');
        $at->setfield('k2', 'v2');
        $at->setfield('k3', 'v3');
        $this->assertSame(array('k1', 'k2', 'k3'), $at->getfieldnames(), 'test_getfield.0009 did not compare correctly.');

    }


    public function test_hasfield()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(false, $at->hasfield('doesnotexist'), 'test_hasfield.0001 did not compare correctly.');

        $at->setfield('doesnotexist', '');
        $this->assertSame(true, $at->hasfield('doesnotexist'), 'test_hasfield.0002 did not compare correctly.');

        $at->clearfield('doesnotexist!');
        $this->assertSame(true, $at->hasfield('doesnotexist'), 'test_hasfield.0003 did not compare correctly.');
        $at->clearfield('doesnotexist');
        $this->assertSame(false, $at->hasfield('doesnotexist'), 'test_hasfield.0004 did not compare correctly.');

        $at->setfield('doesnotexist', '');
        $at->clearfield();
        $this->assertSame(false, $at->hasfield('doesnotexist'), 'test_hasfield.0005 did not compare correctly.');

    }

    public function test_select()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(3, $at->select(3, array(1, 2, 3)), 'test_select.0001 did not compare correctly.');
        $this->assertSame(3, $at->select(5, array(1, 2, 3)), 'test_select.0002 did not compare correctly.');
        $this->assertSame(array('Apr', 'May', 'Jun'), $at->select(2, array(array('Jan', 'Feb', 'Mar'), array('Apr', 'May', 'Jun'))), 'test_select.0003 did not compare correctly.');
        $this->assertSame(array('San Diego', 'Sydney', 'New York', 'Amsterdam'), $at->select(3, array(array(), array(), array('San Diego', 'Sydney', 'New York', 'Amsterdam'))), 'test_select.0004 did not compare correctly.');

        // Our tests.

    }

    public function test_validateinternetaddress()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(true, $at->validateinternetaddress('james@clearc2.com'), 'test_validateinternetaddress.0002 did not compare correctly.');
        $this->assertSame(false, $at->validateinternetaddress('james@@clearc2.com'), 'test_validateinternetaddress.0003 did not compare correctly.');

    }


    public function test_istext()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->istext('Blanchard & Daughters'), "istext('Blanchard & Daughters') did not return true.");
        $this->assertFalse($at->istext(new DateTime()), "istext(new DateTime()) did not return false.");
        $this->assertTrue($at->istext(array('a', '', 'b', 'c')), "istext(array('a', '', 'b', 'c')) did not return true.");
        $this->assertFalse($at->istext(array('a', 2.78, 'b', 'c', 'hello there')), "istext(array('a', 2.78, 'b', 'c', 'hello there')) did not return false.");

        // Our tests.

    }


    public function test_istime()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertTrue($at->istime(new DateTime()), "istime(new DateTime()) did not return true.");
        $this->assertTrue($at->istime(array($at->date(2015, 7, 15), $at->date(2015, 7, 15, 11, 59, 59))), "istime(array(date(2015, 7, 15), date(2015, 7, 15, 11, 59, 59))) did not return true.");
        $this->assertFalse($at->istime(array($at->date(2015, 7, 15), $at->date(2015, 7, 15, 11, 59, 59), 3)), "istime(array(date(2015, 7, 15), date(2015, 7, 15, 11, 59, 59), 3)) did not return false.");

        // Our tests.

    }


    /**
     * You can enable this at will ... it is just a bit slow.
     */
    public function test_urlopen()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $return = $at->urlopen('http://www.clearc2.com');
        $this->assertTrue($return['errno'] === null, "test_urlopen.0001 did not compare correctly.\n\n" . print_r($return, true));
        $return = $at->urlopen('https://www.google.com');
        $this->assertTrue($return['errno'] === null, "test_urlopen.0002 did not compare correctly.\n\n" . print_r($return, true));

    }


    public function test_sort()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array('Albany', 'New Boston', 'new york', 'San Francisco'), $at->sort(array('New Boston', 'Albany', 'new york', 'San Francisco')), 'test_sort.0001 did not compare correctly.');
        $this->assertSame(array('San Francisco', 'new york', 'New Boston', 'Albany'), $at->sort(array('New Boston', 'Albany', 'new york', 'San Francisco'), 'desc'), 'test_sort.0002 did not compare correctly.');
        $this->assertSame(array('Albany', 'New Boston', 'New York', 'San Francisco'), $at->sort($at->propercase(array('New Boston', 'Albany', 'new york', 'San Francisco')), 'ascending'), 'test_sort.0003 did not compare correctly.');
        $this->assertSame(array(1009, 85, 79), $at->sort(array(85, 79, 1009), 'desc'), 'test_sort.0004 did not compare correctly.');
        $this->assertSame(array(79.0, 85.0, 1009), $at->sort(array(85.0, 79.0, 1009)), 'test_sort.0005 did not compare correctly.');

        // Our tests.
        $this->assertSame(array('a' => 'new york', 'b' => 'San Francisco', 'f' => 'New Boston', 'g' => 'Albany'), $at->sort(array('f' => 'New Boston', 'g' => 'Albany', 'a' => 'new york', 'b' => 'San Francisco'), 'bykey'), 'test_sort.0006 did not compare correctly.');
        $this->assertSame(array('g' => 'Albany', 'f' => 'New Boston', 'b' => 'San Francisco', 'a' => 'new york'), $at->sort(array('f' => 'New Boston', 'g' => 'Albany', 'a' => 'new york', 'b' => 'San Francisco'), $at->explode('bykey;desc', ';')), 'test_sort.0007 did not compare correctly.');

        $this->assertSame(array('Albany', 'New Boston', 'San Francisco', 'new york'), $at->sort(array('New Boston', 'Albany', 'new york', 'San Francisco'), 'casesensitive'), 'test_sort.0008 did not compare correctly.');

    }


    public function test_keywords()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(
            array('Harvard', 'Yale'),
            $at->keywords(
                $at->propercase(array("EPA Head speaks at Harvard and yale", "The UCLA Chancellor Retires", "Ohio State wins big game", "Reed and University of Oregon share research facilities")),
                array("Harvard", "Brown", "Stanford", "Yale", "Vassar", "UCLA")
            ),
            "Keywords test 001 failed."
        );

        $this->assertSame(
            array(),
            $at->keywords(
                array("EPA Head speaks at Harvard,Yale", "UCLA Chancellor Retires", "Ohio State wins big game", "Reed and University of Oregon share research facilities"),
                array("harvard", "brown", "stanford", "vassar", "ucla")
            ),
            "Keywords test 002 failed."
        );

        $this->assertSame(
            array("Harvard", "Yale University", "UCLA"),
            $at->keywords(
                array("EPA Head speaks at Harvard, Yale University hosts her next month", "UCLA Chancellor Retires", "Ohio State wins big game", "Reed and University of Oregon share research facilities"),
                array("Harvard", "Brown", "Stanford", "Yale University", "UCLA"),
                ""
            ),
            "Keywords test 003 failed."
        );


        $this->assertSame(
            array("Mary Jones."),
            $at->keywords(
                array(",Mary Jones.", ",John Chen.", ",Miguel Sanchez."),
                "Mary Jones.",
                ","
            ),
            "Keywords test 004 failed."
        );

        $this->assertSame(
            array("Mary Jones."),
            $at->keywords(
                array(",Mary Jones., who works downtown, is being interviewed on Friday.", ",John Chen.", ",Miguel Sanchez."),
                "Mary Jones.",
                ","
            ),
            "Keywords test 005 failed."
        );

        $this->assertSame(
            array("div"),
            $at->keywords(
                array("<html> XML tag that represents a list of our books.", "<div> XML tag that represents a book.", "<span> XML tag that represents the sale price of a book."),
                "div",
                "<>"
            ),
            "Keywords test 006 failed."
        );


        // Our tests.
    }


    public function test_left()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Len', $at->left('Lennard Wallace', 3), "left('Lennard Wallace', 3) failed.");
        $this->assertSame('Lennard Wal', $at->left('Lennard Wallace', 'la'), "left('Lennard Wallace', 'la') failed.");
        $this->assertSame('Timothy', $at->left('Timothy Altman', ' '), "left('Timothy Altman', ' ') failed.");
        $this->assertSame('Tim', $at->left('Timmans Altman', 'man'), "left('Timmans Altman', 'man') failed.");
        $this->assertSame('one', $at->left('one two three', ' '), "left('one two three', ' ') failed.");

        // Our tests.
        $this->assertSame(array('Jam', 'Joh', 'Mar', 'Jos'), $at->left(array('James', 'John', 'Mary', 'Joseph'), 3), "left(array('James', 'John', 'Mary', 'Joseph'), 3) failed.");
        $this->assertSame(array('', 'J', '', 'J'), $at->left(array('James', 'John', 'Mary', 'Joseph'), 'o'), "left(array('James', 'John', 'Mary', 'Joseph'), 'o') failed.");
        $this->assertSame('Le', $at->left('Lennard Wallace', 'nna'), "left('Lennard Wallace', 'nna') failed.");

        $this->assertSame(
            '04/10/14',
            $at->trim(
                $at->right(
                    $at->left('Order ID: M1W3L01V5Y  Receipt Date: 04/10/14 Order Total: $3.23 Billed To: American Express .... 5000'
                        , 'Order Total:'),
                    'Receipt Date:')
            ),
            'test_left 001 failed.');
    }

    public function test_replacesubstring()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('I hate apples', $at->replacesubstring('I like apples', 'like', 'hate'), "replacesubstring('I like apples', 'like', 'hate') failed.");
        $this->assertSame('I hate peaches', $at->replacesubstring('I like apples', array('like', 'apples'), array('hate', 'peaches')), "replacesubstring('I like apples', array('like', 'apples'), array('hate', 'peaches')) failed.");
        $this->assertSame('Line 1 Line 2', $at->replacesubstring("Line 1\nLine 2", "\n", " "), "replacesubstring('Line 1\nLine 2', '\n', ' ') failed.");

        // Our tests.
        $this->assertSame('I hate hate apples', $at->replacesubstring('I like like apples', 'like', 'hate'), "replacesubstring('I like like apples', 'like', 'hate') failed.");

    }


    public function test_right()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('ace', $at->right('Lennard Wallace', 3), "right('Lennard Wallace', 3) failed.");
        $this->assertSame('Wallace', $at->right('Lennard Wallace', ' '), "right('Lennard Wallace', ' ') failed.");
        $this->assertSame('man', $at->right('Timothy Altman', 3), "right('Timothy Altman', 3) failed.");
        $this->assertSame('Altman', $at->right('Timmans Altman', ' '), "right('Timmans Altman', ' ') failed.");
        $this->assertSame('two three', $at->right('one two three', ' '), "right('one two three', ' ') failed.");

        // Our tests.
        $this->assertSame(array('mes', 'ohn', 'ary', 'eph'), $at->right(array('James', 'John', 'Mary', 'Joseph'), 3), "right(array('James', 'John', 'Mary', 'Joseph'), 3) failed.");
        $this->assertSame(array('', 'hn', '', 'seph'), $at->right(array('James', 'John', 'Mary', 'Joseph'), 'o'), "right(array('James', 'John', 'Mary', 'Joseph'), 'o') failed.");
        $this->assertSame('rd Wallace', $at->right('Lennard Wallace', 'nna'), "right('Lennard Wallace', 'nna') failed.");
        $this->assertSame(
            '04/10/14',
            $at->trim(
                $at->right(
                    $at->left('Order ID: M1W3L01V5Y  Receipt Date: 04/10/14 Order Total: $3.23 Billed To: American Express .... 5000'
                        , 'Order Total:'),
                    'Receipt Date:')
            ),
            'test_right 001 failed.');

    }


    public function test_rightback()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('nard Wallace', $at->rightback('Lennard Wallace', 3), "rightback('Lennard Wallace', 3) failed.");
        $this->assertSame('', $at->rightback('Lennard Wallace', ''), "rightback('Lennard Wallace', '') failed.");
        $this->assertSame('Wallace', $at->rightback('Lennard Wallace', ' '), "rightback('Lennard Wallace', ' ') failed.");
        $this->assertSame('othy Altman', $at->rightback('Timothy Altman', 3), "rightback('Timothy Altman', 3) failed.");
        $this->assertSame('palooza', $at->rightback('Lalapalooza', 'la'), "rightback('Lalapalooza', 'la') failed.");
        $this->assertSame('palooza', $at->rightback('lalapalooza', 'la'), "rightback('lalapalooza', 'la') failed.");

        // Our tests.
        $this->assertSame('palooza', $at->rightback('lalapalooza', 'lala'), "rightback('lalapalooza', 'lala') failed.");

    }


    public function test_repeat()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('HelloHelloHello', $at->repeat('Hello', 3), "repeat('Hello', 3) failed.");
        $this->assertSame('ByeBy', $at->repeat('Bye', 2, 5), "repeat('Bye', 2, 5) failed.");
    }


    public function test_round()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(2.0, $at->round(2.499), "round(2.499) failed.");
        $this->assertSame(3.0, $at->round(2.5), "round(2.5) failed.");
        $this->assertSame(2.0, $at->round(1.5), "round(1.5) failed.");
        $this->assertSame(12340.0, $at->round(12338, 10), "round(12338, 10) failed.");
        $this->assertSame(array(1.0, 3.0, 3.0, 4.0), $at->round(array(1.333, 2.897654, 3.1, 4)), "round(array(1.333, 2.897654, 3.1, 4)) failed.");
        $this->assertSame(array(4510.0, 45010.0, 450010.0), $at->round(array(4505, 45005, 450005), 10), "round(array(4505, 45005, 450005), 10) failed.");

    }


    public function test_leftback()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Lennard Wall', $at->leftback('Lennard Wallace', 3), "leftback('Lennard Wallace', 3) failed.");
        $this->assertSame('Lennard', $at->leftback('Lennard Wallace', ' '), "leftback('Lennard Wallace', ' ') failed.");
        $this->assertSame('Timmans Alt', $at->leftback('Timmans Altman', 'man'), "leftback('Timothy Altman', ' ') failed.");

        // Our tests.
        $this->assertSame(array('Ja', 'J', 'M', 'Jos'), $at->leftback(array('James', 'John', 'Mary', 'Joseph'), 3), "leftback(array('James', 'John', 'Mary', 'Joseph'), 3) failed.");
        $this->assertSame(array('J', '', '', 'Jo'), $at->leftback(array('James', 'John', 'Mary', 'Joseph'), 4), "leftback(array('James', 'John', 'Mary', 'Joseph'), 4) failed.");
        $this->assertSame(array('', '', '', 'J'), $at->leftback(array('James', 'John', 'Mary', 'Joseph'), 5), "leftback(array('James', 'John', 'Mary', 'Joseph'), 5) failed.");
        $this->assertSame('one two', $at->leftback('one two three', ' '), "leftback('one two three', ' ') failed.");

    }


    public function test_subset()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array('New Orleans', 'London'), $at->subset(array('New Orleans', 'London', 'Frankfurt', 'Tokyo'), 2), "subset(array('New Orleans', 'London', 'Frankfurt', 'Tokyo'), 2) failed.");
        $this->assertSame(array('London', 'Frankfurt', 'Tokyo'), $at->subset(array('New Orleans', 'London', 'Frankfurt', 'Tokyo'), -3), "subset(array('New Orleans', 'London', 'Frankfurt', 'Tokyo'), -3) failed.");

    }

    public function test_tonumber()
    {
        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame(10.0, $at->tonumber('10'), "tonumber('10') failed.");
        $this->assertSame(array(10.0, 20.0, 30.0), $at->tonumber(array('10', '20', '+30.000')), "tonumber(array('10', '20', '+30.000')) failed.");
        $this->assertSame(array(10, 'a', 'b'), $at->tonumber(array(10, 'a', 'b')), "tonumber(array(10, 'a', 'b')) failed.");
        $this->assertSame('double', gettype($at->tonumber('10')), "gettype(tonumber('10')) failed.");
        $this->assertSame('integer', gettype($at->integer($at->tonumber('10'))), "gettype(integer(tonumber('10'))) failed.");

    }


    public function test_trim()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('ROBERT SMITH', $at->uppercase($at->trim('Robert Smith    ')), "trim('Robert Smith    ') failed.");
        $this->assertSame('ROBERT SMITH', $at->uppercase($at->trim('        Robert Smith')), "trim('        Robert Smith') failed.");
        $this->assertSame('Just a quick reminder', $at->trim('Just a quick reminder'), "trim('Just a quick reminder') failed.");
        $this->assertSame(array('Seattle', 'Toronto', 'Santiago', 'USA', 'Canada', 'Chile'), $at->trim(array('Seattle', 'Toronto', 'Santiago', '', 'USA', 'Canada', 'Chile')), "trim(array('Seattle', 'Toronto', 'Santiago', '', 'USA', 'Canada', 'Chile')) failed.");

    }


    public function test_length()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(45, $at->length('The boy crossed the wide, but gentle, stream.'), "length('The boy crossed the wide, but gentle, stream.') did not equal 45.");
        $this->assertSame(array(0, 5, 3), $at->length(array('', 'abcde', 'xyz')), "length(array('', 'abcde', 'xyz')) did not equal array(0, 5, 3).");

        // Our tests.

    }

    public function test_middle()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('h C', $at->middle('North Carolina', 4, 3), "middle('North Carolina', 4, 3) failed.  Should be 'h C'.");
        $this->assertSame('ort', $at->middle('North Carolina', 4, -3), "middle('North Carolina', 4, -3) failed.  Should be 'ort'.");
        $this->assertSame('Car', $at->middle('North Carolina', ' ', 3), "middle('North Carolina', ' ', 3) failed.  Should be 'Car'.");
        $this->assertSame('or', $at->middle('North Carolina', 'th', -2), "middle('North Carolina', 'th', -2) failed.  Should be 'or'.");
        $this->assertSame(' is the ', $at->middle('This is the text', 4, 'text'), "middle('This is the text', 4, 'text') failed.  Should be ' is the '.");
        $this->assertSame(' is the ', $at->middle('This is the text', 'is', 'text'), "middle('This is the text', 'is', 'text') failed.  Should be ' is the '.");

        // Our tests.

    }

    public function test_unique()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(array('red', 'green', 'blue'), $at->unique(array('red', 'green', 'blue', 'green', 'red')), "unique(array('red', 'green', 'blue', 'green', 'red')) is not valid.");
        $this->assertSame(array('red', 'green', 'blue', 'Green'), $at->unique(array('red', 'green', 'blue', 'Green', 'red')), "unique(array('red', 'green', 'blue', 'Green', 'red')) is not valid.");
//		$this->assertSame('abc', $at->unique(), "unique() is not valid.");        // Cannot be tested ... unique 32-byte string generated everytime.

    }

    public function test_word()
    {

        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Collins,', $at->word('Larson, Collins, and Jensen', ' ', 2), "word('Larson, Collins, and Jensen', ' ', 2) is not valid.");
        $this->assertSame('Jensen', $at->word('Larson, Collins, and Jensen', ' ', 4), "word('Larson, Collins, and Jensen', ' ', 4) is not valid.");
        $this->assertSame('', $at->word('Larson, Collins, and Jensen', ' ', 5), "word('Larson, Collins, and Jensen', ' ', 5) is not valid.");
        $this->assertSame(array('Collins,', 'Marketing,'), $at->word(array('Larson, Collins, and Jensen', 'Sales, Marketing, and Administration'), ' ', 2), "word('Larson, Collins, and Jensen', 'Sales, Marketing, and Administration', ' ', 2) is not valid.");
        $this->assertSame('M.', $at->word('Larson,James,M.', ',', 3), "word('Larson,James,M.', ',', 3) is not valid.");
    }

    public function test_weekday()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame(5, $at->weekday($at->date(1988, 9, 29)), '$at->weekday($at->date(1988, 9, 29)) is not valid.');
        $this->assertSame(1, $at->weekday($at->date(2015, 7, 12)), '$at->weekday($at->date(2015, 7, 12)) is not valid.');
        $this->assertSame(7, $at->weekday($at->date(2015, 7, 18)), '$at->weekday($at->date(2015, 7, 18)) is not valid.');

    }

    public function test_middleback()
    {
        $at = new AtFunctions();

        // Lotus tests.
        $this->assertSame('Alt', $at->middleback('Timothy Altman', ' ', 3), "middleback('Timothy Altman', ' ', 3) failed.  Should be 'Alt'.");
        $this->assertSame('', $at->middleback('NoSpaceHere', ' ', 3), "middleback('NoSpaceHere', ' ', 3) failed.  Should be ''.");
        $this->assertSame(' from right to left', $at->middleback('Middleback searches the string from right to left', 'ing', 25), "middleback('Middleback searches the string from right to left', 'ing', 25) failed.  Should be ' from right to left'.");
        $this->assertSame('searches the string ', $at->middleback('@MiddleBack searches the string from right to left', 'from', -20), "middleback('@MiddleBack searches the string from right to left', 'from', -20) failed.  Should be 'searches the string '.");
        $this->assertSame(' is the t', $at->middleback('This is the text', 4, 'This'), "middleback('This is the text', 4, 'This') failed.  Should be ' is the t'.");
        $this->assertSame(' the ', $at->middleback('This is the text', 'text', 'is'), "middleback('This is the text', 'text', 'is') failed.  Should be ' the '.");

        // Our tests.
        $this->assertSame('', $at->middleback('This is a test', 'x', 'test'), "middleback('This is a test', 'x', 'test') failed.  Should be ''.");

        $this->assertSame('est', $at->middleback('This is a test', 4, 3), "middleback('This is a test', 4, 3) failed.  Should be 'est'.");
        $this->assertSame('a t', $at->middleback('This is a test', 4, -3), "middleback('This is a test', 4, -3) failed.  Should be 'a t'.");
        $this->assertSame(' a t', $at->middleback('This is a test', 4, 'is'), "middleback('This is a test', 4, 'is') failed.  Should be ' a t'.");
        $this->assertSame(' a t', $at->middleback('This is a test', 'is', 4), "middleback('This is a test', 'is', 4) failed.  Should be ' a t'.");
        $this->assertSame('his ', $at->middleback('This is a test', 'is', -4), "middleback('This is a test', 'is', -4) failed.  Should be 'his '.");
        $this->assertSame(' a ', $at->middleback('This is a test', 'test', 'is'), "middleback('This is a test', 'test', 'is') failed.  Should be ' a '.");
        $this->assertSame('This ', $at->middleback('This is a test', 'is', 'test'), "middleback('This is a test', 'is', 'test') failed.  Should be 'This '.");

        $this->assertSame('', $at->middleback('This is a test', 'x', 4), "middleback('This is a test', 'x', 4) failed.  Should be ''.");
        $this->assertSame('This is a t', $at->middleback('This is a test', 4, 'x'), "middleback('This is a test', 4, 'x') failed.  Should be 'This is a t'.");
        $this->assertSame('', $at->middleback('This is a test', 'x', 'x'), "middleback('This is a test', 'x', 'x') failed.  Should be ''.");
        $this->assertSame('This is a ', $at->middleback('This is a test', 'test', 'test'), "middleback('This is a test', 'test', 'test') failed.  Should be 'This is a '.");
        $this->assertSame('This is a ', $at->middleback('This is a test', 'test', 'x'), "middleback('This is a test', 'test', 'x') failed.  Should be 'This is a '.");
    }


    /**
     * This can NOT be tested from PHPUnit, as it will differ for every machine.
     */
    public function test_zone()
    {

        $at = new AtFunctions();

        // Lotus tests.

        // Our tests.
        $this->assertSame('CDT', $at->zone(), 'test_zone.0001 did not compare correctly.');

    }


}