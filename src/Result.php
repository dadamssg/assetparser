<?php

namespace ClearC2\AssetParser;

use ClearC2\AssetParser\Entity\Asset;
use stdClass;

/**
 * Class Result
 * This class should match what the backend expects on an asset save.
 * @author James Jurczewsky james@clearc2.com
 * @copyright Clear C2, Inc.  2015
 * @filesource
 * @version 0.0.1
 * @since 0.0.1
 */
class Result
{

    /**
     * The name of the file being parsed. Blank if the HTML did not come directly from a file.
     * @var string
     */
    protected $filename = '';

    /**
     * The name of the map used to parse the file.  Blank if the map did not come directly from a file.
     * @var string
     */
    protected $mapused = '';

    /**
     * The assets found for the file being parsed.
     * @var Asset[]
     */
    protected $assets = array();


    /**
     * Set to true if the file was successfully parsed.
     * @var bool
     */
    protected $parsed = false;


    /**
     * The results of the last compare.  Will be zero count if no compare was executed, or if the compare was true.
     * Typically, used in PHPUnit testing.
     * @var string[]
     */
    private $resultsOfLastCompare = array();

    /**
     * Destructor.
     */
    public function __destruct()
    {
        foreach ($this->assets as $sa) {
            $sa->__destruct();
        }
    }

    /**
     * Get the results of the last compare() operation.
     * @return string[]
     */
    public function getResultsOfLastCompare()
    {
        return $this->resultsOfLastCompare;
    }

    /**
     * Get the JSON value of the object.
     * @param boolean $strict If true, then return ONLY those properties expected by the backend.
     * @return stdClass
     */
    public function getJSONObject($strict = false)
    {
        $returnValue = new stdClass();

        $returnValue->filename = Parser::enforceUTF8forJSON($this->getFilename());
        $returnValue->mapused = Parser::enforceUTF8forJSON($this->getMapused());
        $returnValue->parsed = $this->getParsed();
        $returnValue->assets = array();

        foreach ($this->getAssets() as $inx => $sa) {
            $returnValue->assets[] = $sa->getJSONObject($strict);
        }

        return $returnValue;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Internal spaces are honored here.
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = trim($filename);
    }

    /**
     * @return string
     */
    public function getMapused()
    {
        return $this->mapused;
    }

    /**
     * Internal spaces are honored here.
     * @param string $mapused
     */
    public function setMapused($mapused)
    {
        $this->mapused = trim($mapused);
    }

    /**
     * @return bool
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    /**
     * @param boolean $parsed
     */
    public function setParsed($parsed)
    {
        $this->parsed = ($parsed === true);
    }

    /**
     * @return Asset[]
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * Update the assets found in this reciept.
     * @param Asset[] $assets
     */
    public function setAssets(array $assets)
    {
        $this->assets = $assets;


        /**
         * This is required post-processing of the found assets.
         */
        foreach ($this->assets as $singleasset) {
            $singleasset->finalize();
        }
    }


    /**
     * Perform any finalization before passing this object off to anyone else.
     */
    public function finalize()
    {
        foreach ($this->assets as $singleasset) {
            $singleasset->finalize();
        }
    }

    /**
     * Compare the current asset with the suplied one.  We do NOT compare filename and map, because they may legitimately be different.
     * Typically, used by phpunit.
     * @param Result $expectedHeader
     * @returns boolean
     */
    public function compare(Result $expectedHeader)
    {

        $returnValue = true;
        $this->resultsOfLastCompare = array();

        $foundAssets = $this->assets;
        $expectedAssets = $expectedHeader->assets;

        if (count($foundAssets) !== count($expectedAssets)) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "INVALID header asset length: " . count($foundAssets) . "!==" . count($expectedAssets) . "[EOD]";
        }

        foreach ($foundAssets as $inx => $sa) {

            if (!array_key_exists($inx, $expectedAssets)) {
                $returnValue = false;
                $this->resultsOfLastCompare[] = "INVALID asset #{$inx} does not exist in expected assets header with filename=" . $this->filename . " and mapused=" . $this->mapused;
            } elseif (!$sa->compare($expectedAssets[$inx])) {
                $returnValue = false;
                $this->resultsOfLastCompare[] = "INVALID asset[$inx] compare for header with filename=" . $this->filename . " and mapused=" . $this->mapused;
                $this->resultsOfLastCompare = array_merge($this->resultsOfLastCompare, $sa->getResultsOfLastCompare());
            }
        }

        return $returnValue;

    }


}


