<?php

namespace ClearC2\AssetParser;

use ClearC2\AssetParser\Entity\Asset;
use ClearC2\AssetParser\Entity\Attribute;
use ClearC2\AssetParser\Entity\HtmlDocument;
use ClearC2\AssetParser\Entity\Map;
use ClearC2\AssetParser\Exception\CouldNotParseHTMLException;
use ClearC2\AssetParser\Repository\MapRepository;
use ClearC2\AssetParser\Utils\AtFunctions;
use DOMDocument;
use DOMElement;
use DOMNode;
use DOMNodeList;
use Exception;
use stdClass;


/**
 * Class EmailParser
 * @author James Jurczewsky james@clearc2.com
 * @copyright Clear C2, Inc.  2015
 * @filesource
 * @version 0.0.1
 * @since 0.0.1
 */
class Parser
{

    /**
     * Used to set the debug level for file parsing.  At this level, there will be NO output.
     * @var int
     */
    public static $DEBUG_LEVEL_NONE = 0;

    /**
     * Used to set the debug level for file parsing.  At this level, there will be 'header' output for each map processed.
     *
     * For example, if a map was NOT correct, you might get this output:
     *
     * **************************************************
     * 15:17:35.473: Logging level: 1
     * 15:17:35.473: Loading html into DOMDocument...
     * 15:17:35.480: Loading html into DOMDocument...success
     * 15:17:35.481: START: Processing map for
     * Vendor: amazon.com
     * Description: amazon_1_00000001.json
     * 15:17:35.517: Parsing terminated for this map [FAILED].
     * 15:17:35.517: END: Processing map for
     * Vendor: amazon.com
     * Description: amazon_1_00000001.json
     * 15:17:35.517: Map is NOT valid.
     * 15:17:35.517: **************************************************
     *
     * If a map WAS correct, you might get this output:
     *
     * **************************************************
     * 15:17:35.518: Logging level: 1
     * 15:17:35.518: Loading html into DOMDocument...
     * 15:17:35.525: Loading html into DOMDocument...success
     * 15:17:35.526: START: Processing map for
     * Vendor: amazon.com
     * Description: C:\OracleLinuxVM\thelogue\assetparser\tests\files\receipts\amazon\amazon_1_00000002.html
     * 15:17:35.596: END: Processing map for
     * Vendor: amazon.com
     * Description: C:\OracleLinuxVM\thelogue\assetparser\tests\files\receipts\amazon\amazon_1_00000002.html
     * 15:17:35.596: Map is valid.  Total number of assets found: 2
     * 15:17:35.596: [{"internalId":"repeatid-0001","id":"","name":" Training Kit (Exam\r\n                    70-462): Administering Microsoft SQL\r\n                    Server 2012 Databases ","attributes":{"image":{"name":"image","value":"http:\/\/ecx.images-amazon.com\/images\/I\/51rTDV+JSZL._SCLZZZZZZZ__SY115_SX115_.jpg","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_price":{"name":"purchase_price","value":"$41.99","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_location":{"name":"purchase_location","value":"Amazon.com","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"order_number":{"name":"order_number","value":"111-4662335-7334655","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_date":{"name":"purchase_date","value":"Placed on Sunday,\r\n                                September 1, 2013","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]}},"resultsOfLastCompare":[]},{"internalId":"repeatid-0002","id":"","name":" Training Kit (Exam\r\n                    70-461): Querying Microsoft SQL Server\r\n                    2012 ","attributes":{"image":{"name":"image","value":"http:\/\/ecx.images-amazon.com\/images\/I\/51FNoP9xOeL._SCLZZZZZZZ__SY115_SX115_.jpg","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_price":{"name":"purchase_price","value":"$43.79","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_location":{"name":"purchase_location","value":"Amazon.com","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"order_number":{"name":"order_number","value":"111-4662335-7334655","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]},"purchase_date":{"name":"purchase_date","value":"Placed on Sunday,\r\n                                September 1, 2013","public":false,"family":false,"friends":false,"resultsOfLastCompare":[]}},"resultsOfLastCompare":[]}]
     * 15:17:35.596: **************************************************
     *
     * @var int
     */
    public static $DEBUG_LEVEL_HEADER = 1;

    /**
     * Used to set the debug level for file parsing.  At this level, there will still be minimal output for each map processed.
     * This includes all the information from DEBUG_LEVEL_HEADER, as well as a trace for each attribute being processed.
     *
     * @var int
     */
    public static $DEBUG_LEVEL_MINIMAL = 2;

    /**
     * Used to set the debug level for file parsing.  At this level, there will quite a bit of output for each map processed.
     * This includes all the information from DEBUG_LEVEL_MINIMAL, as well as more information for each attribute being processed (e.g., attribute value).
     *
     * @var int
     */
    public static $DEBUG_LEVEL_MODERATE = 3;

    /**
     * Used to set the debug level for file parsing.  At this level, there will be complete output for each map processed.  Use this level if you need to debug a map you think should work.
     * @var int
     */
    public static $DEBUG_LEVEL_FULL = 4;

    /**
     * Used to set the ID of the repeatable element within the HTML being processed.
     * @var string
     */
    public static $REPEATABLE_ID = 'data-repeatable-id';


    /**
     * If the result of an exit routine is this value, it indicates that the attribute should be skipped.
     * Currently, CANNOT be false, as this inteferes with logic in applyExitRoutine().
     * @var null
     * @see EmailParser::applyExitRoutine
     */
    public static $EXITROUTINE_VALUE_INDICATING_SKIP = null;


    /**
     * Specify the level of debugging (0 => none, 4 => most).
     * @var int
     */
    public $debugLevel = 0;


    /**
     * If true, then ALL attributes must be found within ALL repeating elements.  If false, then repeating elements WITHOUT all attributes are simply thrown out at the end.
     * So, for the strictest parsing possible, set this to true.  However, setting to false allows more leeway with repeating elements (that are often simply spacers, or provide some sort
     * of UI demarcation.)
     * @var bool
     */
    private $requireAllAttributesAtAllRepeats = false;


    /**
     * The list of nodes that matched our path to completion.  Used in our recursive path trace.
     * @var DOMElement[] $nodesThatMatchedPath
     */
    private $nodesThatMatchedPath = array();

    /**
     * The data-repeatable-id values used by our FIRST attribute search.  This sets the stage for all others. Each value in the array was assigned to a single element (that could be repeated).
     * For example, if a <tr> could be repeated, and there were five repeated <tr>s (representing five different assets), there would be five entries in this array, and each entry
     * would be an attribute (see self::$REPEATABLE_ID) of one <tr>.
     * @var string[]
     */
    private $repeatableIdsUsedByFirstAttribute = array();

    /**
     * Repeatable elements will have the attribute data-repeatable-id=<unique #>.  This allows us to group and check repeated attributes properly.
     * This value gets reset to 0, then incremented, when a new HTML is parsed.
     * @var int
     */
    private $idOfNextRepeatableElement = 0;


    /**
     * The name of the attribute asset we are looking for.
     * @var string
     */
    private $assetAttributeBeingSearched = '';

    /**
     * If true, child counts are ignored during the FIND node opeation.  Set at the map level, and overrides individual node settings.
     * @var bool
     */
    private $ignoreChildCount = false;


    /**
     * If true, sibling counts are ignored during the FIND node opeation.  Set at the map level, and overrides individual node settings.
     * @var bool
     */
    private $ignoreSiblingCount = false;

    /**
     * @var MapRepository
     */
    private $maps;


    /**
     * The atFunctions object used when parsing a map.  Automatically gets reset for each asset parsed.
     * @var AtFunctions
     */
    private $atfunctions = null;

    /**
     * @param MapRepository $maps
     */
    public function __construct(MapRepository $maps)
    {
        $this->maps = $maps;
        $this->atfunctions = new AtFunctions();
    }

    /**
     * Given a string, make sure that it is in UTF-8 format, which is required for JSON encoding.
     * @param string $text
     * @returns string
     */
    public static function enforceUTF8forJSON($text)
    {
        $returnValue = $text;

        if (is_string($text)) {
            $returnValue = iconv(mb_detect_encoding($text, mb_detect_order(), true), "UTF-8", $text);
        }

        return $returnValue;
    }

    /**
     * Currently, only used in testing.  Real maps will be stored by the system in Oracle.
     * @param string $map The map (aka, template) that needs to be stored.  Typically, generated by email_parser.html.
     * @return string|boolean The fully qualified path name if map was written successfully; false otherwise.
     */
    public function storeMapToFile($map)
    {

        $returnValue = false;

        // Get our map in good working order (JSON object).
        /*
         * $workingMap should be in the following format:
         * <code>
         * {
         * header : { vendor : string, description : string, ignoreChildCount : boolean, ignoreSiblingCount : boolean }
         * , map : { attribute : string, parseThisNode : boolean, exitRoutine : { postread : string }, repeatable : boolean, path : stdClass[] }[]
         * }
         * </code>
         *
         * @var stdClass $workingMap
         */
        $workingMap = json_decode($map);
        $this->ensureWorkingMapIsValid($workingMap);

        /**
         * What vendor is this map for?
         * @var string $vendor
         */
        $vendor = str_ireplace(' ', '_', $this->getValueFromstdClass($workingMap->header, 'vendor', ''));
        if (trim($vendor) === '') {
            $vendor = 'UNKNOWN';
        }


        $dir_to_write_to = getcwd() . self::changeWindowsSeparatorToSystem('\\tests\\files\\receipts\\system');

        $filename_for_map = $vendor . '_' . $this->getTimeStamp(3, 'YmdHis') . '.json';
        /**
         * @see http://stackoverflow.com/questions/2021624/string-sanitizer-for-filename
         */
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;:[]().
        $filename_for_map = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $filename_for_map);
        // Remove any runs of periods (thanks falstro!)
        $filename_for_map = preg_replace("([\.]{2,})", '', $filename_for_map);
        $filename_for_map = self::changeWindowsSeparatorToSystem($dir_to_write_to . '\\' . $filename_for_map);


        if (!is_dir($dir_to_write_to)) {
            // This is a problem ...
        } else if (!is_writable($dir_to_write_to)) {
            // This is a problem ...
        } else {
            $filesize = file_put_contents($filename_for_map, trim($map));
            if ($filesize !== false) {
                $returnValue = realpath($filename_for_map);
            }
        }


        return $returnValue;

    }

    /**
     * Make sure out workingMap has all the appropriate top-level attributes.  (Very old maps may not).
     * @param stdClass $workingMap
     */
    private function ensureWorkingMapIsValid(stdClass $workingMap)
    {

        /**
         * Put in all the elements required in workingMap (some old maps might not have them.)
         */
        if (!property_exists($workingMap, 'header')) {
            $workingMap->header = json_encode('{"header":{ "vendor":"--NONE PROVIDED--", "description":"--NONE PROVIDED--"}}');
        }

    }

    /**
     * Retrieve a value from a stdClass object (typically, a JSON object).  If the value doesn't exist, return the default value.
     * @param stdClass $std The JSON object.
     * @param string $value The name of the element to return.
     * @param * $default The default value, of the named element doesn't exist.
     * @returns *
     */
    private function getValueFromstdClass($std, $value, $default = null)
    {
        if ($std === null) {
            $returnValue = $default;
        } else {
            $returnValue = property_exists($std, $value) ? $std->{$value} : $default;
            if ($returnValue === null) {
                $returnValue = $default;
            }
        }

        return $returnValue;
    }

    /**
     * Convert a path with a known Windows directory separator embedded, and return with the running system's valid separator in use.
     * @param string $path
     * @return string
     */
    public static function changeWindowsSeparatorToSystem($path)
    {
        return str_ireplace('\\', DIRECTORY_SEPARATOR, $path);
    }

    /**
     * Get a timestamp, with microsecond precision.
     * @param int $precision How precise the timestamp should be.
     * @param string $format The format of the return string
     * @return string
     */
    private function getTimeStamp($precision = 3, $format = 'H:i:s.')
    {

        // Returns a string in the format		0.microseconds secondsSinceEpoch
        $microtime = microtime();

        $comps = explode(' ', $microtime);
        $seconds = $comps[1];
        $microseconds = substr($comps[0], 2, $precision);

        return date($format . $microseconds, $seconds);

    }

    /**
     * Apply all maps (in our file system) against the supplied HTML, and quit when we have a map.
     * @param string $html The HTML to be parsed.  Only HTML within the &lt;body&gt; tag (inclusive), is used.
     * @return Result
     * @throws CouldNotParseHTMLException
     */
    public function parseHTMLAgainstAllMaps($html)
    {
        foreach ($this->maps->all() as $map) {
            $header = $this->parseHTMLAgainstSingleMap($html, (string)$map);
            if ($header->getParsed()) {
                return $header;
            }
        }

        throw new CouldNotParseHTMLException();
    }

    /**
     * Using the supplied $map (JSON) file, attempt to parse the $html.
     * @param string $html The HTML to be parsed.  Only HTML within the &lt;body&gt; tag (inclusive), is used.
     * @param string|stdClass $map A valid JSON string to be used to parse the html.
     * @return Result
     */
    public function parseHTMLAgainstSingleMap($html, $map)
    {

        libxml_use_internal_errors(true); // We will almost assuredly get malformed HTML; do what we can.

        $mapIsGood = true;
        $STAR_LENGTH = 50;

        /**
         * If we don't take look for one attribute, the map failed.
         */
        $attributesParsed = 0;

        /**
         * @var Asset[] $foundAssets
         */
        $foundAssets = array();


        /**
         * The number of attributes EACH asset must have to be considered complete and valid.
         */
        $eachAssetMustHaveThisNumberOfAttributes = 0;


        /**
         * Wipe out any setfields from prior maps.  We are starting fresh.
         */
        $this->atfunctions->clearfield();


        $this->log("\n\n\n" . str_repeat('*', $STAR_LENGTH), self::$DEBUG_LEVEL_HEADER);
        $this->log("Logging level: " . $this->debugLevel, self::$DEBUG_LEVEL_HEADER);
        $this->log("Strict mode (all attributes must be found in all repeating elements): " . ($this->requireAllAttributesAtAllRepeats ? 'true' : 'false'), self::$DEBUG_LEVEL_HEADER);
        $this->log('Loading html into DOMDocument...', self::$DEBUG_LEVEL_HEADER);
        $dom = $this->loadDOMDocument($html);

        if ($dom instanceof DOMDocument) {
            $this->log('Loading html into DOMDocument...success', self::$DEBUG_LEVEL_HEADER);

            if ($this->debugLevel >= self::$DEBUG_LEVEL_FULL) {
                // $this->log("\n\n" . str_repeat('-', 50) . "\n" . $dom->saveHTML() . "\n" . str_repeat('-', 50) . "\n\n", self::$DEBUG_LEVEL_FULL);
            }

            // The HTML parsed correctly.  Is our map correct?

            // Get our map in good working order (JSON object).
            /*
             * $workingMap should be in the following format:
             * <code>
             * {
             * header : { vendor : string, description : string, ignoreChildCount : boolean, ignoreSiblingCount : boolean }
             * , map : { attribute : string, parseThisNode : boolean, exitRoutine : { postread : string }, repeatable : boolean, path : stdClass[] }[]
             * }
             * </code>
             *
             * @todo Verify $map could be successfully json_decode(d).  What do we do if it can't?
             * @var stdClass $workingMap
             */
            $workingMap = $map instanceof stdClass ? $map : json_decode($map);
            $this->ensureWorkingMapIsValid($workingMap);


            /**
             * What vendor is this map for?
             * @var string $vendor
             */
            $vendor = $this->getValueFromstdClass($workingMap->header, 'vendor', '* NO VENDOR SPECIFIED *');

            /**
             * Description of the map.
             * @var string $description
             */
            $description = $this->getValueFromstdClass($workingMap->header, 'description', '* NO DESCRIPTION SPECIFIED *');

            $this->log("START: Processing map for\nVendor: $vendor\nDescription: $description", self::$DEBUG_LEVEL_HEADER);

            // Ensure $workingMap has all repeatable attributes FIRST, and non-repeatable ones LAST.
            $workingMap = $this->reorderMap($workingMap);


            // Initialize our data id trackers.
            $this->repeatableIdsUsedByFirstAttribute = array();
            $this->idOfNextRepeatableElement = 0;


            /**
             * As we traverse the path for each repeatable attribute, we must wind up with the same number of end nodes.  That number determines how many assets, AT MOST,
             * the receipt contains.
             * The initial value of -1 indicates we haven't yet figured out the required number.
             * @var integer $requiredNumberOfMatchingNodes
             */
            $requiredNumberOfMatchingNodes = -1;


            /**
             * @var stdClass $attributeToFind
             */
            foreach ($workingMap->map as $attributeToFind) {


                /**
                 * The asset attribute we are currently working with (e.g., purchase_price or description).
                 * @var string $attributeName
                 */
                $attributeName = $this->getValueFromstdClass($attributeToFind, 'attribute', '*** ATTRIBUTE NOT SPECIFIED ***');
                $attributesParsed++;
                $this->assetAttributeBeingSearched = $attributeName;
                $this->log("Trying to find attribute '$attributeName'...", self::$DEBUG_LEVEL_MINIMAL);


                /**
                 * The path to traverse to get to this attribute.  It starts from the top (BODY), and goes to the base element that contains our attribute information (e.g., a DIV or TD or even B).
                 * @var stdClass[] $pathToTraverse
                 */
                $pathToTraverse = $this->getValueFromstdClass($attributeToFind, 'path', array());

                /**
                 * Some attributes are not repeatable (i.e., they appear once and only once in the receipt, no matter how many assets appear in the receipt.)
                 * @var boolean $attributeRepeats
                 */
                $attributeRepeats = $this->getValueFromstdClass($attributeToFind, 'repeatable', false);

                /**
                 * If true, the child counts are ignored (in the entire map) during the find operation.
                 * @var boolean $ignoreChildCount
                 */
                $this->ignoreChildCount = $this->getValueFromstdClass($workingMap->header, 'ignoreChildCount', false);


                /**
                 * If true, the sibling counts are ignored (in the entire map) during the find operation.
                 * @var boolean $ignoreSiblingCount
                 */
                $this->ignoreSiblingCount = $this->getValueFromstdClass($workingMap->header, 'ignoreSiblingCount', false);


                /**
                 * Some attributes are in a 'supporting' role, and are not actually parsed.  We will skip these node.
                 * @var boolean $parseThisNode
                 */
                $parseThisNode = $this->getValueFromstdClass($attributeToFind, 'parseThisNode', true);


                /**
                 * Get all the exit routines to be processed.
                 * @var stdClass $exitroutine
                 */
                $exitroutine = $this->getValueFromstdClass($attributeToFind, 'exitRoutine', new stdClass());

                /**
                 * Get the postread exit routine, if it exists.
                 * @var string $exitroutine_postread
                 */
                $exitroutine_postread = $this->getValueFromstdClass($exitroutine, 'postread', '');

                if ($parseThisNode) {

                    // The name is not really an attribute of the asset, but rather the property of the asset.
                    if (strcasecmp($attributeName, 'name') !== 0) {
                        // A complete asset must have this number of attributes.
                        $eachAssetMustHaveThisNumberOfAttributes++;
                    }


                    // Initialize.  This is used in our recursive algorithm.
                    $this->nodesThatMatchedPath = array();

                    // Find all nodes that matched our path.
                    $this->traversePath($dom, $pathToTraverse);

                    // How many nodes did we find?
                    $numNodesThatMatchedPath = count($this->nodesThatMatchedPath);

                    // Validate the found nodes.
                    if ($numNodesThatMatchedPath == 0) {
                        // Error!  We didn't find a single node that we needed for the searched-for attribute.  It is now impossible to find any complete assets.  We are outta here.
                        $mapIsGood = false;
                        $this->log("Trying to find attribute '$attributeName'...FAILED.  0 nodes found.", self::$DEBUG_LEVEL_MINIMAL);
                    } elseif (($numNodesThatMatchedPath !== 1) && (!$attributeRepeats)) {
                        // A non-repeatable attribute can occur once and only once.  This is a fatal error.  Done.
                        $mapIsGood = false;
                        $this->log("Trying to find attribute '$attributeName'...FAILED.  $numNodesThatMatchedPath nodes found, but only 1 was expected [NON-REPEATABLE].", self::$DEBUG_LEVEL_MINIMAL);
                    } else {
                        // The map worked (at least partially) for this attribute.  (It found at least a single node.)  Keep going.
                        if ($requiredNumberOfMatchingNodes === -1) {

                            /**
                             * This is the first attribute we have processed.  We need to do some initial housekeeping.
                             * INITIAL INSTANCE OF ASSET IS DEFINED HERE.
                             */

                            $this->log("Trying to find attribute '$attributeName'...success [FIRST ATTRIBUTE].  $numNodesThatMatchedPath nodes found.", self::$DEBUG_LEVEL_MINIMAL);

                            // We know now how many nodes each asset should contain to be considered complete.
                            $requiredNumberOfMatchingNodes = $numNodesThatMatchedPath;
                            // Keep track of all the repeatable ids used by this first attribute.  They are the only ones that count.
                            $log_listOfRepeatableIds = array();

                            /**
                             * Set up the number of assets to return.  Here's the logic:
                             * This is the first attribute we found.  An asset MUST have all attributes within the repeated block to be valid.
                             * Therefore, at MOST we can have this number of assets.
                             *
                             * @var DOMElement @nmp
                             */
                            foreach ($this->nodesThatMatchedPath as $nmp) {
                                /**
                                 * Get the repeatable ID for each attribute.  All other attributes must be a child of this repeatable ID (because a complete asset MUST contain all repeatable attribute.
                                 * So if it is not in the first asset, it is not complete anyway).
                                 * @var string $rid
                                 */
                                $rid = $this->getRepeatableID($nmp);
                                // Store the list of repeatable IDs that AT BEST can contain complete assets.
                                $log_listOfRepeatableIds[] = $rid;
                                $this->repeatableIdsUsedByFirstAttribute[$rid] = $rid;
                                // Create one asset for each node we found.  Again, since an asset must contain ALL attributes to be valid, we can AT MOST have the number of assets equal to
                                // the number of attributes first found.
                                $epa = new Asset($rid);


                                $numAttributesBeforeExitRoutine = $epa->getNumberOfAttributes();
                                $valueAfterExitRoutine = $this->applyExitRoutine($attributeName, $this->nodeValueBestGuess($nmp), $exitroutine_postread, $epa);
                                /**
                                 * If the exit routine implicitly added attributes, we need to take care of that now.
                                 */
                                $eachAssetMustHaveThisNumberOfAttributes += $epa->getNumberOfAttributes() - $numAttributesBeforeExitRoutine;

                                if ($valueAfterExitRoutine === self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
                                    // This is the map's way of telling us to SKIP this attribute.
                                } else {
                                    $attributeValue = Attribute::convertValueToAppropriateType($attributeName, $valueAfterExitRoutine);
                                    if (strcasecmp($attributeName, 'name') === 0) {
                                        $epa->setName($attributeValue);
                                    } else {
                                        $epa->addAttribute(new Attribute($attributeName, $attributeValue));
                                    }

                                    $foundAssets[] = $epa;
                                }
                            }

                            $this->log("Expecting $requiredNumberOfMatchingNodes assets in this receipt.  The following IDs are in play: " . implode(', ', $log_listOfRepeatableIds), self::$DEBUG_LEVEL_MODERATE);

                        } elseif (($requiredNumberOfMatchingNodes !== $numNodesThatMatchedPath) && $attributeRepeats) {

                            if ($this->requireAllAttributesAtAllRepeats) {
                                // Oops.  This attribute couldn't be found the correct number of times.  Map is bad.
                                $this->log("Trying to find attribute '$attributeName'...FAILED.  $numNodesThatMatchedPath nodes found, but expected to find $requiredNumberOfMatchingNodes nodes.  Found nodes displayed next.", self::$DEBUG_LEVEL_MINIMAL);
                                $mapIsGood = false;

                                foreach ($this->nodesThatMatchedPath as $nmp) {
                                    $this->log("\t" . $nmp->nodeName . "=" . $this->nodeValueAbbreviated($nmp), self::$DEBUG_LEVEL_MODERATE);
                                }
                            } else {
                                // We're missing one or more attributes, but we are going to store what we can, and keep processing.
                                $this->stuffAttributeIntoProperAsset($foundAssets, $attributeName, $exitroutine_postread);
                            }

                        } else {

                            // So far, so good.  We have an EXACT match with the first attribute.
                            if ($attributeRepeats) {
                                $this->log("Trying to find attribute '$attributeName'...success.  $numNodesThatMatchedPath nodes found.", self::$DEBUG_LEVEL_MINIMAL);

                                // Stuff all the found attributes
                                $this->stuffAttributeIntoProperAsset($foundAssets, $attributeName, $exitroutine_postread);
                            } else {
                                $this->log("Trying to find attribute '$attributeName'...success.  $numNodesThatMatchedPath nodes found [NON-REPEATABLE].", self::$DEBUG_LEVEL_MINIMAL);

                                /**
                                 * Stuff this single-occurence attribute into all our of found assets.
                                 */
                                $assetcounter = 0;
                                foreach ($foundAssets as $sa) {

                                    $assetcounter++;

                                    $numAttributesBeforeExitRoutine = $sa->getNumberOfAttributes();
                                    $valueAfterExitRoutine = $this->applyExitRoutine($attributeName, $this->nodeValueBestGuess($this->nodesThatMatchedPath[0]), $exitroutine_postread, $sa);
                                    /**
                                     * If the exit routine implicitly added attributes, we need to take care of that now.
                                     */
                                    if ($assetcounter === 1) {
                                        /**
                                         * We have added fields to an asset.  However, only the first asset contributes to this value.  (All other assets will be required to match this value.)
                                         */
                                        $eachAssetMustHaveThisNumberOfAttributes += $sa->getNumberOfAttributes() - $numAttributesBeforeExitRoutine;
                                    }


                                    if ($valueAfterExitRoutine === self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
                                        // This is the map's way of telling us to SKIP this attribute.
                                    } else {
                                        $attributeValue = Attribute::convertValueToAppropriateType($attributeName, $valueAfterExitRoutine);
                                        if (strcasecmp($attributeName, 'name') === 0) {
                                            $sa->setName($attributeValue);
                                        } else {
                                            $sa->addAttribute(new Attribute($attributeName, $attributeValue));
                                        }

                                    }

                                }

                            }
                        }

                    }


                    $this->log("[END OF SEARCH FOR ATTRIBUTE: {$attributeName}]", self::$DEBUG_LEVEL_MINIMAL);


                    if (!$mapIsGood) {
                        $this->log("Parsing terminated for this map [FAILED].", self::$DEBUG_LEVEL_HEADER);
                        break;
                    }

                } else {
                    $this->log("Trying to find attribute '$attributeName'...skipped (not a parseable node.)  Continuing.", self::$DEBUG_LEVEL_MINIMAL);
                }

            }   // foreach ($workingMap->map as $attributeToFind)

            $this->log("END: Processing map for\nVendor: $vendor\nDescription: $description", self::$DEBUG_LEVEL_HEADER);

        } else {
            // We were given an invalid HTML receipt to parse.  We are done.
            $this->log("Loading html into DOMDocument...failed", self::$DEBUG_LEVEL_HEADER);
            $mapIsGood = false;
        }


        // Better logging/debugging.  Give the user a guess as to what happened.
        if ($attributesParsed === 0) {
            $this->log("No attributes were even searched.  Probably an invalid map was searched.", self::$DEBUG_LEVEL_HEADER);
            $mapIsGood = false;
        }

        // Before we declare victory, we have to weed out invalid assets.
        if ($mapIsGood) {
            $needToReorder = false;
            foreach ($foundAssets as $inx => $sa) {
                if ($sa->getNumberOfAttributes() !== $eachAssetMustHaveThisNumberOfAttributes) {
                    $this->log("Removing asset '" . $sa->getName() . "' because it has " . $sa->getNumberOfAttributes() . " attributes, but it should have $eachAssetMustHaveThisNumberOfAttributes attributes.", self::$DEBUG_LEVEL_MINIMAL);
                    $needToReorder = true;
                    unset($foundAssets[$inx]);
                }
            }
            if ($needToReorder) {
                $foundAssets = array_values($foundAssets);
            }
        }


        if ($mapIsGood) {
            $this->log("Map is valid.  Total number of assets found: " . count($foundAssets), self::$DEBUG_LEVEL_HEADER);
        } else {
            $this->log("Map is NOT valid.", self::$DEBUG_LEVEL_HEADER);
        }

        $header = new Result();
        $header->setParsed($mapIsGood);
        $header->setAssets($foundAssets);

        if ($mapIsGood) {
            $this->log(json_encode($header->getJSONObject()), self::$DEBUG_LEVEL_HEADER);
            $this->log(json_encode($header->getJSONObject(true)), self::$DEBUG_LEVEL_FULL);
        }
        $this->log(str_repeat('*', $STAR_LENGTH) . "\n\n\n", self::$DEBUG_LEVEL_HEADER);

        return ($header);
    }

    /**
     * Built-in logger.
     * @param string $message The message to log.
     * @param integer [$level=4]
     */
    private function log($message, $level = 4)
    {

        if (($level > self::$DEBUG_LEVEL_NONE) && ($this->debugLevel >= $level)) {
            echo $this->getTimeStamp() . ': ' . $message . "\r\n";
        }

    }

    /**
     * Given an HTML string, create a DOMDocument object.
     * @param string $html The HTML to be parsed.  Only HTML within the &lt;body&gt; tag (inclusive), is used.
     * @return DOMDocument|null
     */
    private function loadDOMDocument($html)
    {

        // If we can, get everything in the <body> tag.
        $body_start = stripos($html, '<body');
        if ($body_start !== false) {
            // Find the closing tag.
            $body_start = stripos($html, '>', $body_start);
            if ($body_start !== false) {
                $body_end = stripos($html, '</body>', $body_start);
                if ($body_end !== false) {
                    $html = '<html><body>' . substr($html, $body_start, ($body_end - $body_start)) . '</body></html>';
                }
            }
        }

        // Can we load and parse our document?
        $dom = new DOMDocument();
        $ok = $dom->loadHTML($html);

        // If so, return it.  Otherwise, return null.
        return $ok ? $dom : null;

    }

    /**
     * Reorder the traversal map, putting REPEATABLE elements first, then non-repeating elements.  This is REQUIRED for valid parsing.
     * @param stdClass $map
     * @return stdClass
     */
    private function reorderMap(stdClass $map)
    {
        /*
         * $map should be in the following format:
         * <code>
         * {
         * header : { vendor : string, description : string, ignoreChildCount : boolean, ignoreSiblingCount : boolean }
         * , map : { attribute : string, parseThisNode : boolean, exitRoutine : { postread : string }, repeatable : boolean, path : string[] }[]
         * }
         * </code>
         */

        $repeatbleAttributes = array();
        $nonrepeatableAttributes = array();

        foreach ($this->getValueFromstdClass($map, 'map', array()) as $singleAttribute) {
            if ($this->getValueFromstdClass($singleAttribute, 'repeatable', false)) {
                $repeatbleAttributes[] = $singleAttribute;
            } else {
                $nonrepeatableAttributes[] = $singleAttribute;
            }
        }

        $this->log("Map resorted to parse repeatable attributes first, non-repeatable attributes second.", self::$DEBUG_LEVEL_MODERATE);

        $counter = 0;
        $len = count($repeatbleAttributes);
        foreach ($repeatbleAttributes as $singleAttribute) {
            $counter++;
            $this->log("Repeatable attribute[$counter/$len]: " . $singleAttribute->attribute, self::$DEBUG_LEVEL_FULL);
        }

        $counter = 0;
        $len = count($nonrepeatableAttributes);
        foreach ($nonrepeatableAttributes as $singleAttribute) {
            $counter++;
            $this->log("Non-repeatable attribute[$counter/$len]: " . $singleAttribute->attribute, self::$DEBUG_LEVEL_FULL);
        }

        $map->map = array_merge($repeatbleAttributes, $nonrepeatableAttributes);


        return $map;
    }

    /**
     * Finds all elements that match the path.  Uses recursion to review every single node within the path.
     * @param DOMNode $node The starting node.
     * @param stdClass[] $pathToTraverse WARNING : This gets destroyed, so if you need to, pass in a copy when initially called.
     */
    private function traversePath(DOMNode $node, array $pathToTraverse)
    {

        if (count($pathToTraverse) === 0) {
            // We completed the full path!

            $allowMatch = false;
            /** @noinspection PhpParamsInspection */
            $rid = $this->getRepeatableID($node);       // At this point, $node is a DOMElement.
            $nodeValue = $this->nodeValueAbbreviated($node);
            if ($rid === '') {
                // Non-repeatable attribute ... no problem
                $allowMatch = true;
                $this->log("\tAttribute is non-repeatable.  Added to nodesThatMatchedPath.  nodeValue=$nodeValue", self::$DEBUG_LEVEL_FULL);
            } elseif (count($this->repeatableIdsUsedByFirstAttribute) == 0) {
                // We are processing the first attribute ... everything is good.
                $allowMatch = true;
                $this->log("\tAttribute is repeatable ($rid), that this is our first attribute searched.  Added to nodesThatMatchedPath.  nodeValue=$nodeValue", self::$DEBUG_LEVEL_FULL);
            } elseif (array_key_exists($rid, $this->repeatableIdsUsedByFirstAttribute)) {
                $allowMatch = true;
                $this->log("\tAttribute is repeatable ($rid), and matches one of our first attributes.  Added to nodesThatMatchedPath.  nodeValue=$nodeValue", self::$DEBUG_LEVEL_FULL);
            } else {
                $this->log("\tAttribute is repeatable ($rid), but does NOT match one of our first attributes.  NOT ADDED to nodesThatMatchedPath.  nodeValue=$nodeValue", self::$DEBUG_LEVEL_FULL);
            }

            if ($allowMatch) {
                $this->nodesThatMatchedPath[] = $node;
            }
            return;
        }

        $step = array_shift($pathToTraverse);
        $matchingNodes = $this->find($node, $step);
        if (count($matchingNodes) === 0) {
            // This node didn't make it.  We are done.
            return;
        }

        foreach ($matchingNodes as $sm) {
            // Look at this element.
            $this->traversePath($sm, $pathToTraverse);
        }

    }

    /**
     * For the given node, find out which repeatable element it is under, and return its id.  If it is NOT under a repeatable element (which is valid, e.g., order number), return blank.
     * @param DOMElement $node
     * @return string
     */
    private function getRepeatableID(DOMElement $node)
    {
        $returnValue = '';

        $n = $node;
        while ($n !== null) {
            if (!($n instanceof DOMElement)) {
                break;
            }        // This can happen when we get the DOMDocument.  At this point, we are too far.
            if ($n->hasAttribute(self::$REPEATABLE_ID)) {
                $returnValue = $n->getAttribute(self::$REPEATABLE_ID);
                break;
            }
            $n = $n->parentNode;
        }

        return $returnValue;
    }

    /**
     * Get the (text) value of the node, abbreviated for debugging.
     * @param DOMNode $node
     * @param boolean $bestGuessValue If true, get our best guess at the node's value (based on the node type); if false, get its actual nodeValue.
     * @return string
     */
    private function nodeValueAbbreviated(DOMNode $node, $bestGuessValue = false)
    {
        $nodeValue = $bestGuessValue ? $this->nodeValueBestGuess($node) : $node->nodeValue;
        $originalLength = strlen($nodeValue);
        $condensedValue = Attribute::cleanupWhitespace($nodeValue);
        if ($this->debugLevel < self::$DEBUG_LEVEL_FULL) {
            $condensedValue = substr($condensedValue, 0, 255);
            $ellipses = $originalLength > 255 ? "...($originalLength chars.)" : "";
            $condensedValue .= $ellipses;
        }

        return $condensedValue;

    }

    /**
     * Get the value of the node.  The value depends on what type of node we are working on.
     * IMPORTANT : We do all node value conversion here (e.g., converting a nbsp to a regular space).  Basically, the return value here should be the 'eyeball' node value (what it looks like to your eye).
     * @param DOMElement|DOMNode $node
     * @return string The value of the node.
     */
    private function nodeValueBestGuess(DOMNode $node)
    {

        if (strcasecmp($node->nodeName, 'img') === 0) {
            $returnValue = $node->getAttribute('src');
        } else {
            $returnValue = $node->nodeValue;
        }

        /**
         * Handle non-breaking spaces.
         */
        $returnValue = preg_replace('/\x{00A0}/u', ' ', $returnValue);

        return $returnValue;
    }

    /**
     * Find the requested node.  The primary step in parsing HTML.
     * @param DOMNode $incomingNode
     * @param stdClass $step Information to get to the proper node.
     * @return DOMElement[] List of nodes that match the specified nodeName
     */
    private function find(DOMNode $incomingNode, stdClass $step)
    {
        $returnValue = array();

        // Do we have any child nodes to possibly find?
        if (!$incomingNode->hasChildNodes()) {
            // Nothing more to process.
            return $returnValue;
        }

        $incomingNodeName = $incomingNode->nodeName;

        /*
         * $step has the following attributes:
         *  { name : string, position : number, repeatable : boolean, children_required : number, list_of_children : string[], siblings_required : number, list_of_siblings : string[], step_number : integer, encoded_value : string, comment : string  }
         */


        /**
         * Node name must match this one.
         * @var string $nodename
         */
        $nodeName = strtolower($this->getValueFromstdClass($step, 'name', 'html'));

        /**
         * Node position (i.e., position within siblings) must be at LEAST this number.
         * @var int $nodePosition
         */
        $nodePosition = $this->getValueFromstdClass($step, 'position', 0);

        /**
         * Can the node be repeated (i.e., more than one returned?)
         * @var boolean $repeatable
         */
        $repeatable = $this->getValueFromstdClass($step, 'repeatable', false);

        /**
         * How many children an otherwise-matching node is required to have.  -1 means skip this requirement.
         * @var int $numberOfRequiredChildren
         */
        $numberOfRequiredChildren = $this->getValueFromstdClass($step, 'children_required', -1);
        if ($this->ignoreChildCount) {
            $numberOfRequiredChildren = -1;
        }

        /**
         * The sequence of required children.  Used for debugging purposes only.
         * @var string[] $listOfRequiredChildren
         */
        $listOfRequiredChildren = $this->getValueFromstdClass($step, 'list_of_children', array());

        /**
         * How many siblings an otherwise-matching node is required to have.  -1 means skip this requirement.
         * @var int $numberOfRequiredSiblings
         */
        $numberOfRequiredSiblings = $this->getValueFromstdClass($step, 'siblings_required', -1);
        if ($this->ignoreSiblingCount) {
            $numberOfRequiredSiblings = -1;
        }

        /**
         * The sequence of required siblings. Used for debugging purposes only.
         * @var string[] $listOfRequiredSiblings
         */
        $listOfRequiredSiblings = $this->getValueFromstdClass($step, 'list_of_siblings', array());


        // The node value field, $step->encoded_value, is not used in this processing.

        /**
         * The comment field, $step->comment, is not used in this processing.
         * @var string $comment
         */
        $comment = $this->getValueFromstdClass($step, 'comment', '');

        /**
         * Which step in our path are we on (0 is our last step).
         * @var int $step_number
         */
        $step_number = $this->getValueFromstdClass($step, 'step_number', 0);


        /**
         * @var DOMNodeList $children
         */
        if (($incomingNode instanceof DOMDocument) && (strcasecmp($nodeName, 'body') === 0)) {
            // First time in, we behave a bit differently.
            $childen = $incomingNode->getElementsByTagName($nodeName);
        } else {
            $childen = $incomingNode->childNodes;
        }

        /**
         * Future use ... we may want to automatically change the logging level on STEP 0 ... haven't decided yet.
         * If I do decide to do that, do it after this step.
         */
        $debugLevelOnStart = $this->debugLevel;

        $this->log("STEP {$step_number}: Incoming <$incomingNodeName> looking for <$nodeName>, nodePosition=$nodePosition, repeatable=" . ($repeatable ? 'true' : 'false') . ", numberOfChildren=" . $childen->length . ", numberOfRequiredChildren=$numberOfRequiredChildren, numberOfRequiredSiblings=$numberOfRequiredSiblings, comment=$comment", self::$DEBUG_LEVEL_FULL);

        /**
         * @var DOMNode $childNode
         */
        $childCount = 0;
        foreach ($childen as $childNode) {

            if ($childNode->nodeType === XML_ELEMENT_NODE) {
                // We only care about DOMElement nodes.

                /**
                 * @var DOMElement $childNode
                 */
                if (strcasecmp($nodeName, $childNode->nodeName) === 0) {
                    // Node name matches.  Make other checks.

                    /**
                     * Set to true if this node winds up something we will want to return.
                     */
                    $returnThisNode = false;

                    $numChildren = $this->numberOfDOMElementChildren($childNode);
                    $numSiblings = $this->numberOfDOMElementSiblings($childNode);


                    $this->log("\tFOUND [$childCount]: nodeName=$nodeName, repeatable=" . ($repeatable ? 'true' : 'false') . ", childCount=$childCount, nodePosition=$nodePosition, nodeValue=" . $this->nodeValueAbbreviated($childNode, true), self::$DEBUG_LEVEL_FULL);

                    if ($childCount === $nodePosition) {
                        // The node is in the exact position.  It's a keeper.
                        $returnThisNode = true;
                        $this->log("\tGoodness. childCount($childCount) === ($nodePosition)nodePosition", self::$DEBUG_LEVEL_FULL);
                    } elseif ($repeatable && ($childCount >= $nodePosition)) {
                        // This node is repeatable, so we are good to do.
                        $returnThisNode = true;
                        $this->log("\tGoodness.  repeatable=true && childCount($childCount) >= ($nodePosition)nodePosition", self::$DEBUG_LEVEL_FULL);
                    } else {
                        $this->log("\t\tNode did not make the cut (looking for nodePosition=$nodePosition, node actually in position $childCount).", self::$DEBUG_LEVEL_FULL);
                    }

                    if (!$returnThisNode) {
                        // No need to skip, we aren't using this node anyway.
                    } elseif (($numberOfRequiredChildren >= 0) && ($numberOfRequiredChildren !== $numChildren)) {
                        // We have a set number of required children, and this node doesn't have it.
                        $returnThisNode = false;
                        $this->log("\tNode '" . $this->assetAttributeBeingSearched . "' removed from the cut [child mismatch]... numberOfRequiredChildren($numberOfRequiredChildren) !== ($numChildren)numChildren.  nodeValue=" . $this->nodeValueAbbreviated($childNode, true), self::$DEBUG_LEVEL_MODERATE);


                        $this->log("\tList of $numberOfRequiredChildren expected child nodes displayed next.  (Ignore the values ... they are only valid with the file used for the map definition.)", self::$DEBUG_LEVEL_FULL);
                        $inx = 0;
                        foreach ($listOfRequiredChildren as $rc) {
                            $inx++;
                            $this->log("\t\t[$inx] " . Attribute::cleanupWhitespace($rc));
                        }

                        $this->log("\tList of $numChildren found child nodes displayed next.", self::$DEBUG_LEVEL_FULL);
                        /**
                         * Used for debugging purposes only.  Can be removed at will.
                         * @var DOMNode $innercn
                         */
                        $inx = 0;
                        foreach ($childNode->childNodes as $innercn) {
                            if ($innercn->nodeType === XML_ELEMENT_NODE) {
                                $inx++;
                                $this->log("\t\t[$inx] <" . $innercn->nodeName . ">:" . $this->nodeValueAbbreviated($innercn), self::$DEBUG_LEVEL_FULL);
                            }
                        }
                    } elseif (($numberOfRequiredSiblings >= 0) && ($numberOfRequiredSiblings !== $numSiblings)) {
                        // We have a set number of required siblings, and this node doesn't have it.
                        $returnThisNode = false;
                        $this->log("\tNode '" . $this->assetAttributeBeingSearched . "' removed from the cut [sibling mismatch]... numberOfRequiredSiblings($numberOfRequiredSiblings) !== ($numSiblings)numSiblings.  nodeValue=" . $this->nodeValueAbbreviated($childNode, true), self::$DEBUG_LEVEL_MODERATE);


                        $this->log("\tList of $numberOfRequiredSiblings expected sibling nodes displayed next.  (Ignore the values ... they are only valid with the file used for the map definition.)", self::$DEBUG_LEVEL_FULL);
                        $inx = 0;
                        foreach ($listOfRequiredSiblings as $rc) {
                            $inx++;
                            $this->log("\t\t[$inx] " . Attribute::cleanupWhitespace($rc));
                        }
                        $this->log("\tList of $numSiblings found sibling nodes displayed next.", self::$DEBUG_LEVEL_FULL);
                        /**
                         * Used for debugging purposes only.  Can be removed at will.
                         * @var DOMNode $innercn
                         */
                        $innerpn = $childNode->parentNode;
                        if ($innerpn !== null) {
                            $inx = 0;
                            foreach ($innerpn->childNodes as $innercn) {
                                if ($innercn->nodeType === XML_ELEMENT_NODE) {
                                    $inx++;
                                    $this->log("\t\t[$inx] <" . $innercn->nodeName . ">:" . $this->nodeValueAbbreviated($innercn), self::$DEBUG_LEVEL_FULL);
                                }
                            }
                        }
                    }


                    if ($returnThisNode) {
                        if ($repeatable) {
                            if ($childNode->hasAttribute(self::$REPEATABLE_ID)) {
                                // We have already added the repeatable id...done.
                            } else {
                                // Never reuse the same id.
                                $this->idOfNextRepeatableElement++;
                                $rid = 'repeatid-' . substr('0000' . $this->idOfNextRepeatableElement, -4);
                                /**
                                 * The id will be in the following format:   repeatid-####  and will be sequential, 0001 through nnnn.
                                 */
                                $childNode->setAttribute(self::$REPEATABLE_ID, $rid);
                                $this->log("\tElement is repeatable, and assigned an id of $rid.", self::$DEBUG_LEVEL_FULL);
                            }
                        }

                        if ($step_number === 0) {
                            $this->log("\t-->Node (attribute '" . $this->assetAttributeBeingSearched . "') made the cut.<--", self::$DEBUG_LEVEL_FULL);
                        } else {
                            $this->log("\t-->Node made the cut.<--", self::$DEBUG_LEVEL_FULL);
                        }
                        $returnValue[] = $childNode;
                    }

                    $childCount++;
                } else {
                    // Node name doesn't match what we are looking for.  Skip it.
                    $this->log("\t\tSKIPPING (unmatching node): nodeName=" . $childNode->nodeName . ", nodeValue=" . $this->nodeValueAbbreviated($childNode), self::$DEBUG_LEVEL_FULL);
                }

            }


        }

        // ---------------------------------------------
        // LAST MINUTE ADJUSTMENTS.
        if ((strcasecmp($incomingNodeName, 'table') === 0) && (strcasecmp($nodeName, 'tbody') === 0)) {
            if (count($returnValue) === 0) {
                // Some HTMLs do NOT have a <tbody>, but the javascript engine always puts it in.  In this case, we 'skip' the tbody by simply resubmitting the table for processing.
                $this->log("\tThe incoming node is a table, cannot find tbody, returning incoming table node for subsequent processing.", self::$DEBUG_LEVEL_FULL);
                $returnValue[] = $incomingNode;
            }
        }
        // ---------------------------------------------

        $this->log("\tNumber of nodes found: " . count($returnValue), self::$DEBUG_LEVEL_FULL);

        $this->debugLevel = $debugLevelOnStart;
        return $returnValue;
    }

    /**
     * Get the number of children that are DOMElements for the given node.
     * @param DOMNode $node
     * @return int
     */
    private function numberOfDOMElementChildren(DOMNode $node)
    {

        $numChildren = 0;
        foreach ($node->childNodes as $cn) {
            /**
             * @var DOMNode $cn
             */
            if ($cn->nodeType === XML_ELEMENT_NODE) {
                $numChildren++;
            }
        }

        return $numChildren;
    }

    /**
     * Get the number of siblings that are DOMElements for the given node (including itself).
     * @param DOMNode $node
     * @return int
     */
    private function numberOfDOMElementSiblings(DOMNode $node)
    {

        $parent = $node->parentNode;
        if ($parent === null) {
            return 1;
        }     // He counts only himself.

        return $this->numberOfDOMElementChildren($parent);
    }

    /**
     * If necessary, apply the exit routine to our attribute value.
     * @param string $name The name of the attribute
     * @param string $value The value of the attribute
     * @param string $exitRoutine The exit routine to eval()
     * @param Asset $asset The target asset for any new attributes created in the exit routine.
     * @return string The new value of the attribute (after the exit routine has processed).
     * @throws Exception
     */
    private function applyExitRoutine($name, $value, $exitRoutine, Asset $asset)
    {

        $THROW_EXCEPTION = true;

        $originalvalue = $value;
        $returnValue = $value;
        $exitRoutine = trim($exitRoutine);

        if ($exitRoutine !== '') {
            try {
                // Add in objects that are available to the exit routine. These aren't used in this method, but MUST BE available for the eval of the exit routine.
                /** @noinspection PhpUnusedLocalVariableInspection */
                $at = $this->atfunctions;

                /**
                 * eval() returns NULL unless return is called in the evaluated code, in which case the value passed to return is returned. If there is a parse error in the evaluated code, eval() returns FALSE and execution of the following code continues normally. It is not possible to catch a parse error in eval() using set_error_handler().
                 * @see http://us3.php.net/manual/en/function.eval.php
                 */
                $returnValue = eval($exitRoutine);

                /**
                 * This indicates a parse error.  If the user returns a false, we must treat it as a parse error, because we can't distingiush any difference.
                 */
                if ($returnValue === false) {
                    $returnValue = $originalvalue;
                    if ($THROW_EXCEPTION) {
                        throw new Exception("\n********************\nThe exit routine did not run successfully.\n\nvalue=\n'$originalvalue'.\n\nexitroutine=\n$exitRoutine\n********************\n\n", 6001);
                    }
                } else {

                    if ($returnValue === null) {

                        // User didn't return anything (    e.g.,   return $value;  ).  Did he just make a mistake, and not return anything by accident?

                        if ($value !== self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
                            // If the user didn't wipe out $value, we are going to assume he simply forgot the return $value.
                            /**
                             * This means the easiest way for a user to indicate a skipped attribute is this:
                             *
                             * $value = null;
                             *
                             * and not specify a return.
                             */
                            $returnValue = $value;

                        }
                    }


                    /**
                     * If the exit routine created any new attributes (via the setfield command), process them now.
                     */
                    if ($value !== self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
                        $this->createAttributesFromExitRoutine($asset);
                    }


                }
            } catch (Exception $e) {
                /**
                 * @todo JLJ : 07-09-2015 : Should we throw an error here, and abort?  Currently, we ignore error and keep going.
                 */
                if ($THROW_EXCEPTION) {
                    throw $e;
                }
            }
        }


        if ($returnValue === self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
            $skipvaluedisplay = self::$EXITROUTINE_VALUE_INDICATING_SKIP;
            if ($skipvaluedisplay === null) {
                $skipvaluedisplay = 'null';
            } elseif ($skipvaluedisplay === false) {
                $skipvaluedisplay = 'false';
            } elseif ($skipvaluedisplay === true) {
                $skipvaluedisplay = 'true';
            }
            $this->log("\t\tSKIPPING (applyExitRoutine()): nodeName=$name, nodeValue=$value changed to $skipvaluedisplay, rendering the attribute skipped.", self::$DEBUG_LEVEL_FULL);
        }


        return $returnValue;
    }

    /**
     * After running the exit routine, the user may have performed one or more setfields.  These fields need to be pushed to our current asset as attributes.
     * This typically happens when a single block of text contains multile attributes (e.g., purchase date and purchase place).
     * @param Asset $epa
     * @return int The number of attributes created.
     */
    private function createAttributesFromExitRoutine(Asset $epa)
    {

        $returnValue = 0;

        $newAttributes = $this->atfunctions->getfieldnames();
        foreach ($newAttributes as $attributeName) {
            $attributeValue = Attribute::convertValueToAppropriateType($attributeName, $this->atfunctions->getfield($attributeName));
            if (!$epa->hasAttribute($attributeName)) {
                $returnValue++;
            }
            $epa->addAttribute(new Attribute($attributeName, $attributeValue));
        }

        return $returnValue;
    }

    /**
     * Given a node, find the asset it belongs to, and add it to that asset.  If it can't find the proper asset, it is dropped.
     * @param Asset[] $foundAssets
     * @param string $attributeName
     * @param string $exitroutine_postread The exit routine to apply to the attribute value.
     */
    private function stuffAttributeIntoProperAsset(array & $foundAssets, $attributeName, $exitroutine_postread = '')
    {

        foreach ($this->nodesThatMatchedPath as $nmp) {
            $rid = $this->getRepeatableID($nmp);
            $indexIntoFoundAssets = $this->findInternalIdInListOfAssets($foundAssets, $rid);
            if ($indexIntoFoundAssets >= 0) {
                $sa = $foundAssets[$indexIntoFoundAssets];

                // We found the proper asset.  Store the attribute.
                $valueAfterExitRoutine = $this->applyExitRoutine($attributeName, $this->nodeValueBestGuess($nmp), $exitroutine_postread, $sa);
                if ($valueAfterExitRoutine === self::$EXITROUTINE_VALUE_INDICATING_SKIP) {
                    // This is the map's way of telling us to SKIP this attribute.
                } else {
                    $attributeValue = Attribute::convertValueToAppropriateType($attributeName, $valueAfterExitRoutine);
                    if (strcasecmp($attributeName, 'name') === 0) {
                        $sa->setName($attributeValue);
                    } else {
                        $sa->addAttribute(new Attribute($attributeName, $attributeValue));
                    }
                }
            } else {
                $this->log("The attribute '$attributeName', with a repeatable ID of '$rid', has been dropped, because no matching asset could be found.", self::$DEBUG_LEVEL_FULL);
            }
        }


    }

    /**
     * Given a list of assets, return the index number matching the requested internal id.  Return -1 for no match.
     * @param Asset[] $assets
     * @param string $internalid
     * @returns int
     */
    private function findInternalIdInListOfAssets(array $assets, $internalid)
    {

        $returnValue = -1;

        foreach ($assets as $inx => $sa) {
            if (strcasecmp($sa->getInternalId(), $internalid) === 0) {
                $returnValue = $inx;
                break;
            }
        }

        return $returnValue;
    }

    /**
     * @param HtmlDocument $html
     * @return Result
     * @throws CouldNotParseHTMLException
     */
    public function parse(HtmlDocument $html)
    {
        foreach ($this->maps->all() as $map) {
            try {
                return $this->parseUsingMap($html, $map);
            } catch (CouldNotParseHTMLException $e) {
            }
        }

        throw new CouldNotParseHTMLException("Could not be parsed by any template.");
    }

    /**
     * @param HtmlDocument $document
     * @param Map $map
     * @return Result
     * @throws CouldNotParseHTMLException
     */
    public function parseUsingMap(HtmlDocument $document, Map $map)
    {
        $header = $this->parseHTMLAgainstSingleMap((string)$document, (string)$map);
        if ($header->getParsed()) {
            return $header;
        }

        throw new CouldNotParseHTMLException("Could not be parsed by template.");
    }
}