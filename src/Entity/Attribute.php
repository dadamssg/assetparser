<?php

namespace ClearC2\AssetParser\Entity;

use ClearC2\AssetParser\Parser;
use stdClass;

/**
 * Class Attribute
 * This class should match the characteristics of an asset attribute.
 * @author James Jurczewsky james@clearc2.com
 * @copyright Clear C2, Inc.  2015
 * @filesource
 * @version 0.0.1
 * @since 0.0.1
 */
class Attribute
{

    /**
     * The list of attributes that are known and accepted by the back-end.  This may be different from the attributes that can be collected and parsed in the HMTL page.
     * The key MUST be in lowercase.
     * @var array
     */
    private static $attributesKnownToBackend = array(
        'brand' => ''
        , 'category' => ''
        , 'lily_pad' => ''
        , 'location' => ''
        , 'model' => ''
        , 'name' => ''
        , 'notes' => ''
        , 'payment_method' => ''
        , 'purchase_date' => ''
        , 'purchase_location' => ''
        , 'purchase_price' => ''
        , 'quantity' => ''
        , 'serial_number' => ''
        , 'warranty_expiration_date' => ''
        , 'warranty_extended_date' => ''
        , 'warranty_id' => ''

        // Allowed on the front end (see app/html/email_parser.html), but not allowed on the backend.
        /*
		, 'image' => ''
		, 'order_number' => ''
		, 'purchase_price_extended' => ''
		, 'repeatable_element' => ''
		, 'sku_number' => ''
		*/
    );

    /**
     * The name of the attribute.
     * @var string $name
     */
    protected $name = '';

    /**
     * The value of the attribute.  Dates are stored as integers (seconds from base date); prices are stored as floats.
     * @var string|integer|float $value
     */
    protected $value = '';

    /**
     * If true, this attribute is public (everyone can see).
     * @var bool $public
     */
    protected $public = false;

    /**
     * If true, this attribute can be seen by the family members.
     * @var bool $family
     */
    protected $family = false;

    /**
     * If true, this attribute can be seen by friends.
     * @var bool $friends
     */
    protected $friends = false;


    /**
     * The results of the last compare.  Will be 0-count if no compare was executed, or if the compare was true.
     * Typically, used in PHPUnit testing.
     * @var string[]
     */
    private $resultsOfLastCompare = array();


    /**
     * If you know the name and/or value of the attribute, you can construct it directly.
     * @param string $name
     * @param string $value
     */
    public function __construct($name, $value = '')
    {
        $this->setName($name);
        $this->setValue($value);
    }

    /**
     * Based on the attribute name, try to convert the attribute value to the appropriate one.  This method may change as new attributes are defined.
     * @param string $attributeName
     * @param string $attributeValue
     * @returns string|integer|float
     */
    public static function convertValueToAppropriateType($attributeName, $attributeValue)
    {
        $attributeName = strtolower($attributeName);
        if (array_key_exists($attributeName, array('purchase_price' => '', 'purchase_price_extended' => ''))) {
            // Try to convert to a number.
            $returnValue = self::convertValueToFloat($attributeValue, 2);
        } elseif (array_key_exists($attributeName, array('quantity' => ''))) {
            // Try to convert to a number.
            $returnValue = self::convertValueToFloat($attributeValue, 3);
        } elseif (array_key_exists($attributeName, array('purchase_date' => '', 'warranty_expiration_date' => '', 'warranty_extended_date' => ''))) {
            // Try to convert to a date.
            $returnValue = self::convertValueToTimestamp($attributeValue);
        } else {
            // Convert to a string.
            $returnValue = self::cleanupWhitespace($attributeValue);
        }

        return $returnValue;
    }

    /**
     * Try to convert the value to a Timestamp (integer) value.
     * @param boolean|integer|double|string $attributeValue The value to be converted to a float.
     * @param integer $precision The precision of the returned float.
     * @return float
     */
    private static function convertValueToFloat($attributeValue, $precision = 2)
    {

        $type = gettype($attributeValue);
        $returnValue = $attributeValue;

        if (strcasecmp($type, 'boolean') === 0) {
            $returnValue = $attributeValue ? 1.0 : 0.0;
        } elseif (strcasecmp($type, 'integer') == 0) {
            $returnValue = $attributeValue + 0.0;
        } elseif (strcasecmp($type, 'double') == 0) {
            // Nothing needs to be done here.
        } elseif (strcasecmp($type, 'string') == 0) {
            $attributeValue = self::cleanupWhitespace($attributeValue);
            $attributeValue = str_replace(array(',', '$', '€', 'USD'), '', $attributeValue);     // Remove known 'polluters'.
            $returnValue = floatval(trim($attributeValue));
        }

        $returnValue = round($returnValue, $precision, PHP_ROUND_HALF_UP);

        return $returnValue;
    }

    /**
     * Clean up duplicate whitespace from the string.
     * @param {string|array} $val
     * @return string
     */
    public static function cleanupWhitespace($val)
    {
        $wv = (is_array($val) ? $val : strval($val));
        return trim(preg_replace('/\s+/', ' ', $wv));
    }


    /**
     * Try to convert the value to a Timestamp (integer) value.
     * @param integer|float|string $attributeValue
     * @return int The time representation; 0 => invalid time.
     * @todo JLJ : 2015-07-09 : Need to write test cases.
     */
    private static function convertValueToTimestamp($attributeValue)
    {
        $type = gettype($attributeValue);
        $returnValue = $attributeValue;

        if (strcasecmp($type, 'integer') == 0) {
            // Nothing needs to be done here.
        } elseif (strcasecmp($type, 'double') == 0) {
            $returnValue = intval($attributeValue);
        } elseif (strcasecmp($type, 'string') == 0) {

            $attributeValue = self::cleanupWhitespace($attributeValue);

            // Remove known 'polluters'.
            $attributeValue = str_ireplace(
                array(
                    'Placed on'                             // amazon.com - 'Placed on Monday, December 30, 2013'
                ),
                '',
                $attributeValue
            );
            $returnValue = strtotime(trim($attributeValue));
            if ($returnValue === false) {
                $returnValue = 0;
            }

        }

        return $returnValue;
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
    }

    /**
     * Get the results of the last compare() operation.
     * @return string[]
     */
    public function getResultsOfLastCompare()
    {
        return $this->resultsOfLastCompare;
    }

    /**
     * Compare the current attribute with the supplied one.
     * Typically, used by phpunit.
     * @param Attribute $attributeToCompare
     * @returns boolean
     */
    public function compare(Attribute $attributeToCompare)
    {

        $returnValue = true;
        $this->resultsOfLastCompare = array();

        $tvalue = $this->getName();
        $cvalue = $attributeToCompare->getName();
        if (strcasecmp($tvalue, $cvalue) !== 0) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute name: $tvalue!=={$cvalue}[EOD]";
        }

        $tvalue = $this->getValue();
        $tvalue_type = $this->getTypeOfValue();
        $cvalue = $attributeToCompare->getValue();
        $cvalue_type = $attributeToCompare->getTypeOfValue();
        if (strcasecmp($tvalue, $cvalue) !== 0) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute value: $tvalue!=={$cvalue}[EOD]";
        } elseif ($tvalue !== $cvalue) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute value (strict): $tvalue<{$tvalue_type}>!==$cvalue<{$cvalue_type}>[EOD]";
        }

        $tvalue = $this->getPublic();
        $cvalue = $attributeToCompare->getPublic();
        if ($tvalue !== $cvalue) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute public: " . ($tvalue ? "1" : "0") . "!==" . ($cvalue ? "1" : "0") . "[EOD]";
        }


        $tvalue = $this->getFamily();
        $cvalue = $attributeToCompare->getFamily();
        if ($tvalue !== $cvalue) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute family: " . ($tvalue ? "1" : "0") . "!==" . ($cvalue ? "1" : "0") . "[EOD]";
        }


        $tvalue = $this->getFriends();
        $cvalue = $attributeToCompare->getFriends();
        if ($tvalue !== $cvalue) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\t\tINVALID attribute friends: " . ($tvalue ? "1" : "0") . "!==" . ($cvalue ? "1" : "0") . "[EOD]";
        }

        return $returnValue;

    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = self::cleanupWhitespace($name);
    }

    /**
     * @return float|int|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = self::convertValueToAppropriateType($this->getName(), $value);
    }

    /**
     * Returns the type (e.g., boolean, integer, double, string, array, object, resource, NULL, unknown type) of the current value.  Typically, used for strict comparisons.
     * @return string
     */
    public function getTypeOfValue()
    {
        return gettype($this->getValue());
    }

    /**
     * @return bool
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * @param boolean $public
     */
    public function setPublic($public)
    {
        $this->public = ($public === true);
    }

    /**
     * @return bool
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param boolean $family
     */
    public function setFamily($family)
    {
        $this->family = ($family === true);
    }

    /**
     * @return bool
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * @param boolean $friends
     */
    public function setFriends($friends)
    {
        $this->friends = ($friends === true);
    }


    /**
     * Get the JSON value of the object.
     * @param boolean $strict If true, then return ONLY those properties expected by the backend.
     * @return stdClass
     */
    public function getJSONObject($strict = false)
    {
        $returnValue = new stdClass();

        $needToOutput = false;
        if ($strict) {
            $name_lc = strtolower($this->getName());
            if (array_key_exists($name_lc, self::$attributesKnownToBackend)) {
                $needToOutput = true;
            }
        } else {
            $needToOutput = true;
        }

        if ($needToOutput) {
            $returnValue->name = Parser::enforceUTF8forJSON($this->getName());
            $returnValue->value = Parser::enforceUTF8forJSON($this->getValue());
            $returnValue->public = $this->getPublic();
            $returnValue->family = $this->getFamily();
            $returnValue->friends = $this->getFriends();
        } else {
            $returnValue = null;
        }

        return $returnValue;
    }


}

