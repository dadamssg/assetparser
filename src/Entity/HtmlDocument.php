<?php

namespace ClearC2\AssetParser\Entity;

/**
 * Class HtmlDocument
 * @package ClearC2\AssetParser\Entity
 */
class HtmlDocument
{
    /**
     * @var string
     */
    private $contents;

    /**
     * @param string $contents
     */
    public function __construct($contents)
    {
        $this->contents = $contents;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->contents;
    }
}