<?php

namespace ClearC2\AssetParser\Entity;

/**
 * Class Map
 * @package ClearC2\AssetParser\Entity
 */
class Map
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}