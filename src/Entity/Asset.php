<?php

namespace ClearC2\AssetParser\Entity;

use ClearC2\AssetParser\Parser;
use stdClass;

/**
 * Class Asset
 * This class should match what the backend expects on an asset save.
 * @author James Jurczewsky james@clearc2.com
 * @copyright Clear C2, Inc.  2015
 * @filesource
 * @version 0.0.1
 * @since 0.0.1
 */
class Asset
{
    /**
     * The id of the asset.  A long string (e.g., b84c0537-078c-4a1c-b39b-10ccbfd1ff4a).  Almost always, will remain blank from parsing.
     * @var string $id
     */
    protected $id = '';

    /**
     * The name of the asset.
     * @var string $name
     */
    protected $name = '';

    /**
     * Associative array of attributes for this asset.  The key is the attribute name (e.g., purchase_price or warranty_number).
     * @var Attribute[] $attributes
     */
    protected $attributes = array();

    /**
     * The internal id of this asset.  Only valid during the parsing of the HTML.
     * Does NOT represent anything on the back-end.
     * @var string
     */
    private $internalId = '';

    /**
     * The results of the last compare.  Will be zero count if no compare was executed, or if the compare was true.
     * Typically, used in PHPUnit testing.
     * @var string[]
     */
    private $resultsOfLastCompare = array();


    /**
     * @param string $internalId If known, the internal id for this asset.
     */
    public function __construct($internalId = '')
    {
        $this->internalId = $internalId;
    }


    /**
     * Destructor.
     */
    public function __destruct()
    {
        foreach ($this->attributes as $sa) {
            $sa->__destruct();
        }
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Perform any finalization before passing this object off to anyone else.
     */
    public function finalize()
    {
        $this->finalize_quantity();
        $this->finalize_purchase_price_extended();
    }

    /**
     * Handle the case where this asset doesn't have a quantity.  We will default to 1.
     */
    private function finalize_quantity()
    {
        if (! $this->hasAttribute('quantity')) {
            $this->addAttribute(new Attribute('quantity', 1));
        }
    }

    /**
     * Determines if the named attribute exists within this asset.
     * @param string $name
     * @return bool True if the named attribute exists; false otherwise.
     */
    public function hasAttribute($name) {
        return $this->getAttribute($name) !== null;
    }

    /**
     * Get the named attribute, if it exists.  Otherwise, return null.
     * @param string $name The name of the attribute to retrieve.
     * @return Attribute|null
     */
    public function getAttribute($name) {
        $returnValue = null;
        foreach ($this->attributes as $sa) {
            if (strcasecmp($sa->getName(), $name) === 0) {
                $returnValue = $sa;
                break;
            }
        }

        return $returnValue;
    }

    /**
     * Add/replace the provided attribute to this asset.
     * @param Attribute $attribute
     */
    public function addAttribute(Attribute $attribute)
    {
        $this->attributes[$attribute->getName()] = $attribute;
    }

    /**
     * Handle the case where this asset has an extended purchase price (which the back-end doesn't recognize), but not a purchase price (which is what the back-end is expecting.)
     */
    private function finalize_purchase_price_extended() {

        /**
         * If we have a purchase_price attribute, we are done.
         */
        if ($this->hasAttribute('purchase_price')) {
            return;
        }

        $purchase_price_extended = $this->getAttribute('purchase_price_extended');
        if ($purchase_price_extended !== null) {
            /*
			 * Ok, we have an extended price.  We need to calculate the unit price, as that is what the backend is expecting.
			 */
            $ppe = $purchase_price_extended->getValue();

            $quantity = $this->getAttribute('quantity');
            if ($quantity === null) {
                /**
                 * If we don't have a quantity explicitly set, we use a 1 for our calculations.
                 */
                $q = 1;
            } else {
                $q = $quantity->getValue();
            }

            $purchase_price = (($q === 0.0) || ($q === 0) ? $ppe : ($ppe / $q));
            $this->addAttribute(new Attribute('purchase_price', $purchase_price));


        }

    }

    /**
     * Get the number of attributes this asset has.
     * @return int
     */
    public function getNumberOfAttributes()
    {
        return count($this->attributes);
    }

    /**
     * Get the results of the last compare() operation.
     * @return string[]
     */
    public function getResultsOfLastCompare()
    {
        return $this->resultsOfLastCompare;
    }

    /**
     * Compare the current asset with the suplied one.
     * Typically, used by phpunit.
     * @param Asset $assetToCompare
     * @returns boolean
     */
    public function compare(Asset $assetToCompare)
    {

        $returnValue = true;
        $this->resultsOfLastCompare = array();

        $tvalue = count($this->attributes);
        $cvalue = count($assetToCompare->attributes);
        if ($tvalue !== $cvalue) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\tINVALID asset attribute length: (current asset)$tvalue!=={$cvalue}(asset to compare)[EOD]";
        }

        $tvalue = $this->id;
        $cvalue = $assetToCompare->id;
        if (strcasecmp($tvalue, $cvalue) !== 0) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\tINVALID asset id: $tvalue!=={$cvalue}[EOD]";
        }

        $tvalue = $this->name;
        $cvalue = $assetToCompare->name;
        if (strcasecmp($tvalue, $cvalue) !== 0) {
            $returnValue = false;
            $this->resultsOfLastCompare[] = "\tINVALID asset name: $tvalue!=={$cvalue}[EOD]";
        }


        foreach ($this->attributes as $sa_name => $sa) {
            if (!array_key_exists($sa_name, $assetToCompare->attributes)) {
                $returnValue = false;
                $this->resultsOfLastCompare[] = "\tMISSING asset attribute in asset to compare: {$sa_name}.  Attribute value in current asset=" . $sa->getValue() . "; type=" . $sa->getTypeOfValue() . "[EOD]";
            } elseif (!$sa->compare($assetToCompare->attributes[$sa_name])) {
                $returnValue = false;
                $this->resultsOfLastCompare[] = "\tError in attribute compare found for asset $sa_name.";
                $this->resultsOfLastCompare = array_merge($this->resultsOfLastCompare, $sa->getResultsOfLastCompare());
            }
        }

        foreach ($assetToCompare->attributes as $sa_name => $sa) {
            if (!array_key_exists($sa_name, $this->attributes)) {
                $returnValue = false;
                $this->resultsOfLastCompare[] = "\tMISSING asset attribute in current asset: {$sa_name}.  Attribute value in asset to compare=" . $sa->getValue() . "[EOD]";
            }
        }


        return $returnValue;

    }

    /**
     * Get the JSON value of the object.
     * @param boolean $strict If true, then return ONLY those properties expected by the backend.
     * @return stdClass
     */
    public function getJSONObject($strict = false)
    {
        $returnValue = new stdClass();

        $returnValue->id = Parser::enforceUTF8forJSON($this->getId());
        $returnValue->name = Parser::enforceUTF8forJSON($this->getName());
        $returnValue->attributes = array();

        if (!$strict) {
            $returnValue->internalid = Parser::enforceUTF8forJSON($this->getInternalId());
        }

        foreach ($this->attributes as $inx => $sa) {
            $attrJSON = $sa->getJSONObject($strict);
            if ($attrJSON !== null) {
                $returnValue->attributes[] = $attrJSON;
            }
        }

        return $returnValue;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = Attribute::cleanupWhitespace($id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = Attribute::cleanupWhitespace($name);
    }

    /**
     * Get the internal id for the asset.
     * @return string
     */
    public function getInternalId()
    {
        return $this->internalId;
    }


}


