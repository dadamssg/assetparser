<?php

namespace ClearC2\AssetParser\Exception;

/**
 * Class CouldNotParseHTMLException
 * @package ClearC2\AssetParser\Exception
 */
class CouldNotParseHTMLException extends \Exception
{
}