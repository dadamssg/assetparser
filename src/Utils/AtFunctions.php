<?php

namespace ClearC2\AssetParser\Utils;

use DateTime;

/**
 * Emulate Lotus Notes V 6.0 @functions.
 * User: JJurczewsky
 * Date: 7/14/2015
 * Time: 7:18 PM
 * @see http://www-12.lotus.com/ldd/doc/domino_notes/Rnext/help6_designer.nsf/f4b82fbb75e942a6852566ac0037f284/292f5b677fa6b3ce85256c54004e82d5?OpenDocument
 */
class AtFunctions
{

    /**
     * The source/target for @getfield and @setfield.  Allows the user to manipulate temporary fields.
     * @var array Associative array of fields.  Name is stored in lowercase, value can be anything.
     * @see AtFunctions::getfield
     * @see AtFunctions::setfield
     */
    private $fields = array();


    /**
     * Returns the absolute (positive) value of a number.
     * @param int|float|array $number Any number valid in Lotus Notes/Domino, whether positive or negative, whole or fractional, integer or real.
     * @return array  The absolute value of anyNumber.
     */
    public function abs($number)
    {

        $working = $this->returnAsArray($number);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_int($v) || is_float($v)) {
                array_push($returnValue, abs($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Wrap $value inside an array (if it isn't already an array.)
     * @param $value
     * @return array
     */
    private function returnAsArray($value)
    {
        return is_array($value) ? $value : array($value);
    }

    /**
     * Adjusts the specified time­date value by the number of years, months, days, hours, minutes, and/or seconds you specify. The amount of adjustment may be positive or negative.
     * @param DateTime|array $dateToAdjust Time-date. The time-date value you want to increment. This should be a single value, not a range.
     * @param int $years The number of years to increment by.
     * @param int $months The number of months to increment by.
     * @param int $days The number of days to increment by.
     * @param int $hours The number of hours to increment by.
     * @param int $minutes The number of minutes to increment by.
     * @param int $seconds The number of seconds to increment by.
     * @return DateTime|array
     */
    function adjust($dateToAdjust, $years = 0, $months = 0, $days = 0, $hours = 0, $minutes = 0, $seconds = 0)
    {
        $working = $this->returnAsArray($dateToAdjust);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                /**
                 * Do NOT modify the original that was passed in.
                 * @type DateTime
                 */
                $newv = clone $v;

                if ($seconds !== 0) {
                    $newv->modify("$seconds second");
                }
                if ($minutes !== 0) {
                    $newv->modify("$minutes minute");
                }
                if ($hours !== 0) {
                    $newv->modify("$hours hour");
                }
                if ($days !== 0) {
                    $newv->modify("$days day");
                }
                if ($months !== 0) {
                    $newv->modify("$months month");
                }
                if ($years !== 0) {
                    $newv->modify("$years year");
                }

                array_push($returnValue, $newv);
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Converts an IBM Code Page 850 code number into the corresponding single character string.
     * @param int|array $codeNumber Any number between 0 and 255. Non­integer numbers are truncated to integers.
     * @return string A single character that corresponds to codeNumber.
     */
    function char($codeNumber)
    {

        $working = $this->returnAsArray($codeNumber);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_float($v)) {
                $v = intval($v, 10);
            }

            if (is_int($v)) {
                array_push($returnValue, chr($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Tells what the user's browser is capable of.
     * @param string $user_agent The User Agent to be analyzed. By default, the value of HTTP User-Agent header is used; however, you can alter this (i.e., look up another browser's info) by passing this parameter.    You can bypass this parameter with a NULL value.
     * @param bool $return_array If set to TRUE, this function will return an array instead of an object.
     * @return \stdClass|array|boolean The information is returned in an object or an array which will contain various data elements representing, for instance, the browser's major and minor version numbers and ID string; TRUE/FALSE values for features such as frames, JavaScript, and cookies; and so forth.  Returns a false is get_browser is not enabled in php.ini.
     */
    function browserinfo($user_agent = null, $return_array = false)
    {
        return get_browser($user_agent, $return_array);
    }

    /**
     * Compares the alphabetic order of the elements in two lists pair-wise.
     * @param array $list1 The first two parameters are text lists. If one list is shorter, the last element in the shorter list is repeated until it reaches the same length as the longer list. The corresponding elements of each list are compared.
     * @param array $list2
     * @param bool|true $caseSensitive If true, a caseSensitive comparision is made.
     * @return array Number list. Each element is the result of comparing the corresponding elements in the text lists, and is one of three values:    0 if the elements in the two lists are equal; -1 if the element in the first list is less than the element in the second list. For example, this is the result if the first list contains alice and the second list contains bobby; 1 if the element in the first list is greater than the element in the second list. For example, this is the result if the first list contains bobby and the second list contains alice.
     */
    public function compare($list1, $list2, $caseSensitive = true)
    {

        $returnValue = array();

        $working1 = $this->returnAsArray($list1);
        $working2 = $this->returnAsArray($list2);
        if (count($working1) < count($working2)) {
            $working1 = $this->expandArrayWithLastElement($working1, count($working2));
        } else {
            $working2 = $this->expandArrayWithLastElement($working2, count($working1));
        }

        reset($working1);
        reset($working2);

        /**
         * $working1 and $working2 are now guaranteed to have the same length.  Do the compare.
         */
        while (true) {
            $working1_kv = each($working1);
            $working2_kv = each($working2);
            if ($working1_kv === false) {
                break;
            }
            if ($working2_kv === false) {
                break;
            }      // Failsafe.

            /**
             * We don't care about the keys, look at only the values.
             */
            $working1_v = $working1_kv[1];
            $working2_v = $working2_kv[1];

            if ($caseSensitive === false) {
                $comparison = strcasecmp($working1_v, $working2_v);
            } else {
                $comparison = strcmp($working1_v, $working2_v);
            }

            if ($comparison < 0) {
                $comparison = -1;
            } elseif ($comparison > 0) {
                $comparison = 1;
            }

            array_push($returnValue, $comparison);

        }

        // If we were sent in a single value, return a single value.
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Return an array with $newcount length.  If $newcount is smaller than current length, do nothing.  If $newcount is larger than current length, increase the array size, and fill with the last entry of the array.
     * @param array $list The array that needs to be filled.
     * @param int $newcount How many elements the array should be.
     * @param mixed $filleroverride If non-null, then use as the filler value.
     * @returns array
     */
    private function expandArrayWithLastElement($list, $newcount, $filleroverride = null)
    {

        $working = $this->returnAsArray($list);
        if (count($working) >= $newcount) {
            // We are done.
        } else {
            $lastelem = ($filleroverride !== null ? $filleroverride : $working[count($working) - 1]);

            while (count($working) < $newcount) {
                array_push($working, $lastelem);
            }
        }

        return $working;

    }

    /**
     * @param string $token The token to decode.
     * @return string|array
     */
    function urldecode($token)
    {
        $returnValue = array();

        $working = $this->returnAsArray($token);
        foreach ($working as $v) {
            if (is_string($v)) {
                $v = urldecode($v);
            }
            array_push($returnValue, $v);
        }

        // If we were sent in a single value, return a single value.
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * @param string $token The token to encode.
     * @return string
     */
    function urlencode($token)
    {
        $returnValue = array();

        $working = $this->returnAsArray($token);
        foreach ($working as $v) {
            if (is_string($v)) {
                $v = urlencode($v);
            }
            array_push($returnValue, $v);
        }

        // If we were sent in a single value, return a single value.
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * In a Web application, returns the current URL command and parameters, or the value of one of the parameters.
     * @param $paramaterName
     * @return string|array
     */
    function urlquerystring($paramaterName = null)
    {
        $returnValue = array();

        if ($paramaterName === null) {

            /**
             * User didn't specify a querystring parameter ... give them all back to him.
             */
            if (isset($_GET)) {
                $returnValue = $_GET;
            }
        } else {

            /**
             * User specified one or more parameters ... give only the requested ones back.
             */

            $working = $this->returnAsArray($paramaterName);

            foreach ($working as $wk => $wv) {
                $value = null;

                if (($value === null) && isset($_POST)) {
                    if (array_key_exists($wv, $_POST)) {
                        // Quickest check ... an exact match
                        $value = $_POST[$wv];
                    } else {
                        // Have to look for it.
                        foreach ($_POST as $gk => $gv) {
                            if (strcasecmp($wv, $gk) === 0) {
                                $value = $gv;
                                break;
                            }
                        }
                    }
                }

                if (($value === null) && isset($_GET)) {
                    if (array_key_exists($wv, $_GET)) {
                        // Quickest check ... an exact match
                        $value = $_GET[$wv];
                    } else {
                        // Have to look for it.
                        foreach ($_GET as $gk => $gv) {
                            if (strcasecmp($wv, $gk) === 0) {
                                $value = $gv;
                                break;
                            }
                        }
                    }
                }


                if ($value === null) {
                    $value = '';
                }
                array_push($returnValue, $value);
            }
        }

        // If we were sent in a single value, return a single value.
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Determines if a substring is at the end of a string. This function is case-sensitive.
     * @param string|array $string Text or text list. The string(s) you want to search.
     * @param string|array $substring Text or text list. The string you want to search for at the end of string.
     * @return bool Returns true (1) if any substring end any one of the strings.  Returns false (0) if no substrings ends in any of the strings. If any element in the substrings is a null string(""), this function always returns true.
     * @see http://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    function ends($string, $substring)
    {

        $returnValue = false;
        $string_array = $this->returnAsArray($string);
        $substring_array = $this->returnAsArray($substring);

        foreach ($string_array as $string_value) {
            foreach ($substring_array as $substring_value) {
                if ($substring_value === "" || (($temp = strlen($string_value) - strlen($substring_value)) >= 0 && strpos($string_value, $substring_value, $temp) !== false)) {
                    $returnValue = true;
                    break;
                }
            }
            if ($returnValue) {
                break;
            }
        }

        return $returnValue;
    }

    /**
     * At run-time, compiles and runs each element in a text expression as a formula. Returns the result of the last formula expression in the list or an error if an error is generated by any of the formula expressions in the list.
     * If the eval should return anything, you MUST add a return statement.
     * @param $textExpressions
     * @return \Exception|boolean|mixed|null
     * @see http://php.net/manual/en/function.eval.php
     */
    public function evalex($textExpressions)
    {
        $working = $this->returnAsArray($textExpressions);
        $returnValue = array();


        foreach ($working as $v) {

            try {

                /**
                 * If the user's statement generates an exception, it will be returned to him.
                 * @type \Exception
                 */
                $runtimeException = null;

                /**
                 * Do the user a favor ... wrap everything in a try/catch, and return the exception.
                 * @type string
                 */
                $safeEval = '
try {
	' . $v . '
} catch (\Exception $e) {
	$runtimeException = $e;
}
';

                /**
                 * Actually try to execute (evaulate) the user's code.
                 * @type *
                 */
                $result = eval($safeEval);

            } catch (\Exception $e) {
                $result = $e;
            }

            /**
             * Push the result.
             * A false is returned if the statement could not be parsed.  Of course, it is possible that the code eval'd correctly, and the result was a false.  No way to determine this.
             * An exception is returned in the case of a runtime error.
             * A null is returned in the eval statement returned nothing.
             */
            array_push($returnValue, $result);

        }


        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Calculates the number e (approximately 2.718282) raised to the specified power (this value can contain up to 14 decimal places).
     * @param int|float|array $power The power to which you want to raise e.
     * @return float|array
     */
    public function exp($power)
    {
        $working = $this->returnAsArray($power);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_int($v) || is_float($v)) {
                array_push($returnValue, pow(M_E, $v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * @param float|int $number1 The first number to compare.
     * @param float|int $number2 The second number to compare.
     * @param float $confidenceRange The amount within which the numbers must be equal.
     * @return boolean|array  Returns true if the difference of the numbers is less than or equal to the confidence range; false otherwise.
     */
    public function floateq($number1, $number2, $confidenceRange = 0.0001)
    {

        $returnValue = array();

        $working1 = $this->returnAsArray($number1);
        $working2 = $this->returnAsArray($number2);
        if (count($working1) < count($working2)) {
            $working1 = $this->expandArrayWithLastElement($working1, count($working2));
        } else {
            $working2 = $this->expandArrayWithLastElement($working2, count($working1));
        }


        reset($working1);
        reset($working2);


        /**
         * $working1 and $working2 are now guaranteed to have the same length.  Do the compare.
         */
        while (true) {
            $working1_kv = each($working1);
            $working2_kv = each($working2);
            if ($working1_kv === false) {
                break;
            }
            if ($working2_kv === false) {
                break;
            }      // Failsafe.

            /**
             * We don't care about the keys, look at only the values.
             */
            $working1_v = $working1_kv[1];
            $working2_v = $working2_kv[1];


            if ((is_int($working1_v) || is_float($working1_v)) && (is_int($working2_v) || is_float($working2_v))) {
                $comparison = (abs($working1_v - $working2_v) <= $confidenceRange);
            } else {
                $comparison = false;
            }

            array_push($returnValue, $comparison);

        }


        // If we were sent in a single value, return a single value.
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Returns the current operating system's time zone settings in canonical time zone format.
     * @param bool|false $getAbbreviatedValue If true, then the abbreviated timezone is returned (e.g., 'PST' instead of 'America/Los_Angeles').
     * @return string
     */
    public function getcurrenttimezone($getAbbreviatedValue = false)
    {

        if ($getAbbreviatedValue === true) {
            $returnValue = date('T');
        } else {
            $returnValue = date_default_timezone_get();
        }

        return $returnValue;

    }


    /**
     * If a field has been set, return its value (can be anything).  Otherwise, return null.
     * @param string $name
     * @return *|null
     */
    public function getfield($name)
    {

        $working = is_string($name) ? strtolower(trim($name)) : $name;
        if (array_key_exists($working, $this->fields)) {
            $returnValue = $this->fields[$working];
        } else {
            $returnValue = null;
        }

        return $returnValue;
    }

    /**
     * Set a field to a particular value, and return the value (can be anything).
     * @param string $name
     * @param * $value
     * @return *
     */
    public function setfield($name, $value)
    {
        $working = is_string($name) ? strtolower(trim($name)) : $name;

        $this->fields[$working] = $value;
        return $value;
    }


    /**
     * If a field exists, return true; otherwise, false.
     * @param string $name
     * @return bool
     */
    public function hasfield($name)
    {

        $working = is_string($name) ? strtolower(trim($name)) : $name;
        if (array_key_exists($working, $this->fields)) {
            $returnValue = true;
        } else {
            $returnValue = false;
        }

        return $returnValue;
    }


    /**
     * Clear/remove/unset a field, or unset ALL fields.
     * @param string|null $name If null, ALL fields are cleared.
     */
    public function clearfield($name = null)
    {

        $working = is_string($name) ? strtolower(trim($name)) : $name;
        if ($working === null) {
            /**
             * User wants to clear ALL fields.
             */
            $this->fields = array();
        } elseif (array_key_exists($working, $this->fields)) {
            /**
             * User wants to clear only a single field.
             */
            unset($this->fields[$working]);
        }


    }

    /**
     * Return all the field names in use.
     * @return array
     */
    public function getfieldnames()
    {
        return array_keys($this->fields);
    }

    /**
     * In a Web application, returns the value of an HTTP header from the browser client request being processed by the server.  In a non-web application, return null.
     * @param string|array $requestHeader The name of a request-header field, for example, "From," "Host," or "User-Agent."
     * @return string|null|array The value of the request-header; null if the request-header could not be found.
     */
    public function gethttpheader($requestHeader)
    {

        $working = $this->returnAsArray($requestHeader);
        $returnValue = array();


        /**
         * Get all the HTTP headers.
         * Use the native apache call, if available.
         * Otherwise, rip through the $_SERVER variables and make them into the requested HTTP headers.
         */
        if (function_exists('getallheaders')) {

            /**
             * Native apache function exists.  Use it.
             */
            try {
                $allheaders = getallheaders();
            } catch (\Exception $e) {
                $allheaders = false;
            }
        } elseif (isset($_SERVER) && is_array($_SERVER)) {

            /**
             * Native apache function does NOT exist ... find information inside of $_SERVER.
             */
            $allheaders = array();
            foreach ($_SERVER as $name => $value) {
                if (strcasecmp(substr($name, 0, 5), 'HTTP_') === 0) {
                    $allheaders[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
        } else {

            /**
             * We can't get anything.
             */
            $allheaders = false;
        }


        foreach ($working as $v) {

            $header = null;
            if ($allheaders !== false) {
                foreach ($allheaders as $hk => $hv) {
                    if (strcasecmp($hk, $v) === 0) {
                        $header = $hv;
                        break;
                    }
                }
            }
            array_push($returnValue, $header);

        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns a string containing the version of the currently running PHP parser or extension.
     * @param string $extension An optional extension name.
     * @return string|boolean If the optional extension parameter is specified, phpversion() returns the version of that extension, or FALSE if there is no version information associated or the extension isn't enabled.
     */
    public function version($extension = '')
    {
        /**
         * Note : this can return false, even in the extension is loaded (e.g., curl) when there is no associated version information.
         */
        $returnValue = ($extension === '' ? phpversion() : phpversion($extension));
        return $returnValue;
    }

    /**
     * Returns the name of the server containing the current database. When the database is local, returns the user name.
     * @return string|false
     */
    public function servername()
    {
        if (isset($_SERVER) && is_array($_SERVER) && array_key_exists('HTTP_HOST', $_SERVER)) {
            $returnValue = $_SERVER['HTTP_HOST'];
        } else {
            $returnValue = getenv('COMPUTERNAME');
        }

        return $returnValue;
    }

    /**
     * Returns the value that appears in the number position. If the number is greater than the number of values, @Select returns the last value in the list. If the value in the number position is a list, returns the entire list contained within the value.
     * @param int $number $returnValue = '';
     * @param array $values The values to search.
     * @return string|array
     */
    public function select($number, $values)
    {

        $counter = 0;
        $working = $this->returnAsArray($values);
        $returnValue = end($working);

        foreach ($working as $v) {
            $counter++;
            if ($counter === $number) {
                $returnValue = $v;
                break;
            }
        }

        return $returnValue;

    }

    /**
     * Check if a php.ini file is loaded, and retrieve its path.
     * @return string|false The loaded php.ini path, or FALSE if one is not loaded.
     */
    public function configfile()
    {
        return php_ini_loaded_file();
    }

    /**
     * Returns the name of the current server and filename, separated by a colon (::).
     * @return string
     */
    public function dbname()
    {
        return gethostname() . '::' . __FILE__;
    }

    /**
     * The directory of the file. If used inside an include, the directory of the included file is returned. This is equivalent to dirname(__FILE__). This directory name does not have a trailing slash unless it is the root directory.
     * @return string
     */
    public function filedir()
    {
        return __DIR__;
    }

    /**
     * Generate a hash value (message digest)
     * @param string $data The data to be hashed.
     * @param string $algorithm The hashing alogorithm to use.
     * @param bool $raw_output When set to TRUE, outputs raw binary data. FALSE outputs lowercase hexits.
     * @return string Returns a string containing the calculated message digest as lowercase hexits unless raw_output is set to true in which case the raw binary representation of the message digest is returned.
     * @see http://php.net/manual/en/function.hash-algos.php
     */
    public function hashpassword($data, $algorithm = 'md5', $raw_output = false)
    {
        return hash($algorithm, $data, $raw_output);
    }

    /**
     * Sets a PHP ini variable.  Sets the value of the given configuration option. The configuration option will keep this new value during the script's execution, and will be restored at the script's ending.
     * @param string $variable
     * @param string $value
     * @return string|bool Returns the old value on success, FALSE on failure.
     */
    public function setini($variable, $value)
    {
        return ini_set($variable, $value);
    }

    /**
     * Returns a true.
     * @return bool
     */
    public function success()
    {
        return $this->true();
    }

    /**
     * Return a true.
     * @return bool
     */
    function true()
    {
        return true;
    }

    /**
     * Sets an environment variable.  The environment variable will only exist for the duration of the current request. At the end of the request the environment is restored to its original state.
     * @param string $variable
     * @param mixed $value
     * @return bool True if the set worked; false otherwise.
     */
    public function setenvironment($variable, $value)
    {
        return putenv($variable . "=" . $value);
    }

    /**
     * Given a number, returns its positive square root.
     * @param int|float|array $number The number whose square root you want to find. The number must be positive, otherwise @Sqrt returns an error.
     * @return float|array
     */
    public function sqrt($number)
    {
        $working = $this->returnAsArray($number);
        $returnValue = array();

        foreach ($working as $v) {
            if ((is_int($v) || is_float($v)) && ($v >= 0)) {
                array_push($returnValue, sqrt($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Tests a string for a pattern string. Because the pattern string can contain a number of "wildcard" characters and logical symbols, you can test for complex character patterns.
     * Uses REGULAR EXPRESSION instead of the characters used by @matches.
     * @param string|array $string The value to be tested to see if it matches pattern.
     * @param string $pattern The regular expression pattern.  Does NOT need the beginning and ending /, but will be honored if supplied.
     * @return boolean|array Returns true if the pattern matches the string; false otherwise.
     */
    function matches($string, $pattern)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();

        $boundary = '/';
        if ((substr($pattern, 1) === '/') && (substr($pattern, -1) === '/')) {
            $boundary = '';
        }

        $fullpattern = "$boundary$pattern$boundary";
        foreach ($working as $v) {
            $pm = preg_match($fullpattern, $v);
            if ($pm === false) {
                // This is an error, but we will handle it as a non-match.
            } elseif ($pm === 1) {
                $pm = true;
            } else {
                $pm = false;
            }
            array_push($returnValue, $pm);
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * @param int|float|array $base The value that you want raised to exponent. May be positive or negative
     * @param int|float $exponent The power.
     * @return float|array
     */
    public function power($base, $exponent)
    {
        $working = $this->returnAsArray($base);
        $returnValue = array();

        foreach ($working as $v) {
            if ((is_int($v) || is_float($v)) && (is_int($exponent) || is_float($exponent))) {
                array_push($returnValue, pow($v, $exponent));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Matches a string with a pattern. It is case-sensitive.
     * @param string $string The value to be tested to see if it matches pattern.
     * @param string $pattern The sequence of characters to search for within string. May also contain any of the wildcard characters listed below.
     * @param string $escape Optional. A character to use before a wildcard character to indicate that it should be treated literally.
     * @return bool True if the pattern matches, false otherwise.
     */
    public function like($string, $pattern, $escape = '\\')
    {

        /**
         * Temporarily used as a replacement for like escape sequences.
         * Should not contain any special preg_replace characters, and should not appear in the $pattern.
         */
        $sequence_that_doesnt_exist_in_wild = '@`@``@```@``@`@';

        /**
         * Temporarily used as a replacement for escaped escape sequences.
         * Should not contain any special preg_replace characters, and should not appear in the $pattern.
         */
        $escape_escape = '`@`@@`@@@`@@`@`';

        /**
         * First, we escape characters that like doesn't think are special, but preg_match does.
         */
        $working_pattern = $pattern;
        $working_pattern = str_ireplace(
            array(
                '\\\\',         // The user has already escaped the preg_replace escape character.
                '\\',           // We need to manually escape the preg_replace escape character, because like doesn't know about it.
                '.',
                '[',
                ']',
                '^',
                '$',
                '(',
                ')',
                '?',
                '*',
                '+',
                '{',
                '}'
            ),
            array(
                $escape_escape, // Temporary change ... will be changed back to \\ before the pattern is applied.
                "\\\\",         // Manually escape the preg_replace escape character.
                "\.",
                "\[",
                "\]",
                "\^",
                "\\$",
                "\(",
                "\)",
                "\?",
                "\*",
                "\+",
                "\{",
                "\}"
            ),
            $working_pattern);


        /**
         * Next, convert the pattern to something that preg_match understands.
         * like _   =>  preg_match .
         * like %   =>  preg_match .*
         */
        $working_pattern = $this->replacesubstring($working_pattern, "{$escape}_", $sequence_that_doesnt_exist_in_wild);
        $working_pattern = $this->replacesubstring($working_pattern, '_', '.');     // Replaces the like wildcard with the preg_match wildcard.
        $working_pattern = $this->replacesubstring($working_pattern, $sequence_that_doesnt_exist_in_wild, "_"); // This is not a preg_match wildcard, so we treat it as-is.

        $working_pattern = $this->replacesubstring($working_pattern, "{$escape}%", $sequence_that_doesnt_exist_in_wild);
        $working_pattern = $this->replacesubstring($working_pattern, '%', '.*');    // Replaces the like wildcard with the preg_match wildcard.
        $working_pattern = $this->replacesubstring($working_pattern, $sequence_that_doesnt_exist_in_wild, "%"); // This is not a preg_match wildcard, so we treat it as-is.

        $working_pattern = $this->replacesubstring($working_pattern, $escape_escape, "\\\\");


        /**
         * Create our preg_match pattern.
         */
        $preg_pattern = '/' . $working_pattern . '/';

        /**
         * Do we have a winner?
         */
        $returnValue = preg_match($preg_pattern, $string) === 1;

        return $returnValue;

    }

    /**
     * Replaces specific words or phrases in a string with new words or phrases that you specify. Case sensitive.
     * @param string|array $sourcelist Text or text list. The string whose contents you want to modify.
     * @param string|array $fromlist Text or text list. A list containing the words or phrases that you want to replace.
     * @param string|array $tolist Text or text list. A list containing the replacement words or phrases.
     * @return string|array
     */
    function replacesubstring($sourcelist, $fromlist, $tolist)
    {
        $returnValue = array();

        $w_sourcelist = $this->returnAsArray($sourcelist);
        $w_fromlist = $this->returnAsArray($fromlist);
        $w_tolist = $this->returnAsArray($tolist);

        foreach ($w_sourcelist as $keysource => $source) {

            foreach ($w_fromlist as $keyfrom => $from) {

                $posInSourceThatMatchesFrom = strpos($source, $from);
                if ($posInSourceThatMatchesFrom !== false) {

                    // We have something that needs to be replaced.
                    if (array_key_exists($keyfrom, $w_tolist)) {
                        // We have a corresponding replace value ... use it
                        $replacementValue = $w_tolist[$keyfrom];
                    } else {
                        // Use the last replace value specified
                        $replacementValue = $w_tolist[count($w_tolist) - 1];
                    }

                    $source = str_replace($from, $replacementValue, $source);

                }

                $returnValue[$keysource] = $source;

            }


        }


        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Returns the natural log of a number. Natural logs use e (approximately 2.718282) as their base.
     * @param int|float|array $number May be any value greater than 0, and can contain up to 15 decimal places.
     * @return float|array
     */
    public function ln($number)
    {
        $working = $this->returnAsArray($number);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_int($v) || is_float($v)) {
                array_push($returnValue, log($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns the common logarithm (base 10) of any number greater than zero.
     * @param int|float|array $number May be any value greater than 0, and can contain up to 15 decimal places.
     * @return float|array
     */
    public function log($number)
    {
        $working = $this->returnAsArray($number);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_int($v) || is_float($v)) {
                array_push($returnValue, log($v, 10));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Return the value of pi.
     * @return float
     */
    function pi()
    {
        return pi();
    }

    /**
     * Converts the words in a string to proper­name capitalization: the first letter of each word becomes uppercase, all others become lowercase.
     * @param string|array $string The string you want to convert to propercase.
     * @return string|array The string, converted to propercase letters.
     */
    function propercase($string)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();
        foreach ($working as $v) {
            if (is_string($v)) {
                array_push($returnValue, ucwords(strtolower($v)));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Converts the lowercase letters in the specified string to uppercase.
     * @param string|array $string The string you want to convert to uppercase.
     * @return string|array The string, converted to uppercase letters.
     */
    function uppercase($string)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();
        foreach ($working as $v) {
            if (is_string($v)) {
                array_push($returnValue, strtoupper($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Calculates the number of text, number, or time­date values in a list. This function always returns a number to indicate the number of entries in the list.
     * This function is similar to AtFunctions::elements, except that it returns the number 1 instead of 0, when the value it is evaluating is not a list or is a null string.
     * @param array $list Text list, number list, or time-date list.
     * @return int The number of elements in the list. If the value is not a list or is a null string, returns the number 1.
     * @see AtFunctions::elements
     */
    function count($list)
    {
        if (is_array($list)) {
            return count($list);
        } else {
            return 1;
        }
    }

    /**
     * Calculates the number of text, number, or time­date values in a list. This function always returns a number to indicate the number of entries in the list.
     * This function is similar to AtFunctions::count, except that it returns the number 0 instead of 1, when the value it is evaluating is not a list or is a null string.
     * @param array $list Text list, number list, or time-date list.
     * @return int The number of elements in the list. If the value is not a list or is a null string, returns the number 0.
     * @see AtFunctions::count
     */
    function elements($list)
    {
        if (is_array($list)) {
            return count($list);
        } else {
            return 0;
        }
    }

    /**
     * Translates numbers for the various components of time and date; then returns the time­date value.
     * @param int $hour
     * @param int $minute
     * @param int $second
     * @param int $year If not specified, defaults to current year.
     * @param int $month If not specified, defaults to current month.
     * @param int $day If not specified, defaults to current day.
     * @return DateTime
     */
    function time($hour, $minute, $second, $year = 0, $month = 0, $day = 0)
    {

        $now = time();

        if ($year === 0) {
            $year = intval(date('Y', $now), 10);
        }

        if ($month === 0) {
            $month = intval(date('n', $now), 10);
        }

        if ($day === 0) {
            $day = intval(date('j', $now), 10);
        }

        return $this->date($year, $month, $day, $hour, $minute, $second);

    }

    /**
     * Translates numbers for the various components of time and date, then returns the DateTime value.
     * @param int $year The year that you want to appear in the resulting date. You must specify an entire four-digit year. (For example, use 1996, not 96).
     * @param int $month The month that you want to appear in the resulting date.
     * @param int $day The day that you want to appear in the resulting date.
     * @param int [$hour=0] The number of hours.  Defaults to midnight if not specified.
     * @param int [$minute=0] The number of minutes.
     * @param int [$second=0] The number of seconds.
     * @return DateTime
     */
    function date($year, $month, $day, $hour = 0, $minute = 0, $second = 0)
    {

        $returnValue = new DateTime();

        if (is_integer($year) && is_integer($month) && is_integer($day)) {
            if (is_integer($hour) && is_integer($minute) && is_integer($second)) {
                $returnValue = new DateTime("$year-$month-$day $hour:$minute:$second");
            } else {
                $returnValue = new DateTime("$year-$month-$day");
            }
        }

        return $returnValue;

    }

    /**
     * Converts any value to a text string.
     * @param string|int|float|DateTime $value Number, time-date, rich text, or text.  The value you want to convert to text.
     * @param string [$formatstring=] Used when converting numbers of DateTimes.
     * @return string|array
     */
    function text($value, $formatstring = '')
    {

        $working = $this->returnAsArray($value);
        $returnValue = array();


        foreach ($working as $v) {
            if (is_float($v) || is_int($v)) {

                if (trim($formatstring) === '') {
                    // The simplest conversion possible.
                    $v = "$v";
                } else {
                    /**
                     * True if we should represent the number as a percent; false otherwise.
                     * @type boolean
                     */
                    $percentage = stripos($formatstring, '%') === false ? false : true;

                    /**
                     * The number that will ultimately be converted to text.
                     * @type float|int
                     */
                    $numberv = $v;
                    if ($percentage) {
                        $numberv = $numberv / 100;
                    }


                    $currency = stripos($formatstring, 'C') === false ? false : true;
                    /**
                     * How many decimal places to keep.
                     * @type int
                     */
                    $precision = preg_replace('/\D/', '', $formatstring);
                    if (($precision === '') || $currency) {
                        $precision = 2;
                    } else {
                        $precision = intval($precision, 10);
                    }


                    /**
                     * The thousands separator to use.
                     * @type string
                     */
                    $thousands_sep = stripos($formatstring, ',') === false ? '' : ',';


                    /**
                     * The working number, formatted to the correct precision and with separators.
                     * @type string
                     */
                    $v = number_format($numberv, $precision, '.', $thousands_sep);


                    if ($percentage) {
                        $v .= '%';
                    }

                    if ($currency) {
                        $v = "\${$v}";
                    }

                    $parens = stripos($formatstring, '()') === false ? false : true;
                    if ($parens && ($numberv < 0)) {
                        $v = "($v)";
                    }

                    $notation = stripos($formatstring, 'S') === false ? false : true;
                    if ($notation) {
                        /**
                         * A request for scientific notation.
                         */
                        $v = sprintf("%.{$precision}e", $numberv);
                    }

                }


            } elseif (is_bool($v)) {
                $v = ($v ? -1 : 0);
            } elseif ($v instanceof DateTime) {
                if ($formatstring === '') {
                    $v = $this->canonicaldate($v);
                } else {
                    $v = date($formatstring, $v->getTimestamp());
                }
            }

            array_push($returnValue, $v);
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Given a DateTime object, return the canonical date YYYY-MM-DD HH:MM:SS.
     * @param Datetime|array $timeDateValue The value with the canonical date that you want to extract.
     * @return string|array The canonical dates, the original value if not a DateTime object.
     */
    function canonicaldate($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, $v->format('Y-m-d H:i:s'));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Extracts and returns the year from the specified time­date value.
     * @param DateTime|array $timeDateValue The date containing the year value that you want to extract.
     * @return int|array The number corresponding to the year indicated by timeDateValue.  0 if timeDateValue is not valid.
     */
    function year($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('Y'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Extracts and returns the month from the specified time­date value.
     * @param DateTime|array $timeDateValue The date containing the month value that you want to extract.
     * @return int The number corresponding to the month indicated by timeDateValue.  0 if timeDateValue is not valid.
     */
    function month($timeDateValue)
    {
        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('n'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Extracts the day of the month from the specified date.
     * @param DateTime|array $timeDateValue The date containing the day value that you want to extract.
     * @return int The number corresponding to the day of the month indicated by timeDateValue.  0 if timeDateValue is not valid.
     */
    function day($timeDateValue)
    {
        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('j'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Extracts the day of the month from the specified date.
     * @param DateTime|array $timeDateValue The date containing the day value that you want to extract.
     * @return int The number corresponding to the day of the month indicated by timeDateValue.  0 if timeDateValue is not valid.
     */
    function weekday($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('w'), 10) + 1);  // In Notes, Sunday = 1 ... Saturday = 7
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Returns the specified word from a text string. A "word" is defined as the part of a string that is delimited by the defined separator character. For example, if you specify a space (" ") as the separator, then a word is any series of characters preceded by and followed by a space (or by the quotation marks that indicate the beginning or end of the string).
     * @param string|array $string
     * @param string $separator
     * @param int $number
     * @return string|array
     */
    function word($string, $separator, $number)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();

        foreach ($working as $v) {
            $exploded_v = explode($separator, $v);
            if (count($exploded_v) >= $number) {
                $valueToReturn = $exploded_v[$number - 1];
            } else {
                $valueToReturn = '';
            }

            array_push($returnValue, $valueToReturn);
        }


        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Concatenates all members of a text list and returns a text string.
     * @param array $textlistValue List containing the items you want to concatenate into a single string. If you send a single piece of text instead of a list, returns the text unaltered.
     * @param string [$separator= ] Used to separate the values in the concatenated string. If you don't specify a separator, a space is used.
     * @return string String containing each member of textListValue, separated by separator.
     */
    function implode($textlistValue, $separator = ' ')
    {

        if (is_array($textlistValue)) {
            return implode($separator, $textlistValue);
        } else {
            return $textlistValue;
        }

    }

    /**
     * Return a false.
     * @return bool
     */
    function no()
    {
        return $this->false();
    }

    /**
     * Return a false.
     * @return bool
     */
    function false()
    {
        return false;
    }

    /**
     * Return a true.
     * @return bool
     */
    function yes()
    {
        return $this->true();
    }

    /**
     * Return a true.
     * @return bool
     */
    function all()
    {
        return $this->true();
    }

    /**
     * Return a new line.
     * @return string The newline character(s) for the current environment.
     */
    function newline()
    {
        return PHP_EOL;
    }

    /**
     * Generate and return a random number.
     * @return int
     */
    function random()
    {
        return rand();
    }

    /**
     * @param string|string[] $sendTo The primary recipient(s) of the mail memo.
     * @param string|string[] $copyTo The copy recipient(s) of the mail memo.
     * @param string|string[] $blindCopyTo The blind copy recipient(s) of the mail memo.
     * @param string $subject The text you want displayed in the Subject field. This is equivalent to the Subject field on a mail memo; the message is displayed in the Subject column in the views in the recipients' mail databases.
     * @param string $body Any text you want at the beginning of the body field of the memo.
     * @param string $from Who is sending the memo.
     * @return bool|string A message if the mail() configuration is wrong; true is the mail() worked; a false otherwise.
     * @see http://php.net/manual/en/function.mail.php
     * @see http://glob.com.au/sendmail/
     * @todo Need to add from, copyTo and blindCopyTo processing.
     */
    function mailsend($sendTo,
                      $copyTo = '',
                      $blindCopyTo = '',
                      $subject = '',
                      $body = '',
        /** @noinspection PhpUnusedParameterInspection */
                      $from = '')
    {


        /**
         * This becomes a string if mail isn't set up (because we want to tell the user the problem), a false if the actual mail() call failed, or a true if everything is good.
         */
        $returnValue = true;


        /**
         * EOL for each header.
         */
        $rn = "\r\n";


        // ----- SET UP HEADERS -----
        /**
         * String to be inserted at the end of the email header.
         * This is typically used to add extra headers (From, Cc, and Bcc). Multiple extra headers should be separated with a CRLF (\r\n). If outside data are used to compose this header, the data should be sanitized so that no unwanted headers could be injected.
         */
        if (trim($from) === '') {
            $from = $this->getini('sendmail_from');
            if (!is_string($from)) {
                $from = '';
            }
        }
        $from = trim($from);

        $headers = "From: $from$rn";
        $headers .= "Reply-To: $from$rn";
        $headers .= "X-Mailer: PHP/" . phpversion() . $rn;

        $working_copyTo = trim(is_array($copyTo) ? implode(',', $copyTo) : $copyTo);
        if ($working_copyTo !== '') {
            $headers .= "CC: $working_copyTo$rn";
        }

        $working_blindCopyTo = trim(is_array($blindCopyTo) ? implode(',', $blindCopyTo) : $blindCopyTo);
        if ($working_blindCopyTo !== '') {
            $headers .= "BCC: $working_blindCopyTo$rn";
        }


        // ----- ERROR CHECK HEADERS -----
        if (($returnValue === true) && ($from === '')) {
            $returnValue = 'The from was not specified in the call, and could not be found in ini_get(\'sendmail_from\').  The email will not be sent.';
        }


        /**
         * Do some initial error checking.
         */
        if ($this->iswindows()) {

            /**
             * Is sendmail installed, and in the path?
             * REQUIRED : Put sendmail in c:\sendmail, and add c:\sendmail to the path.
             */
            if (($returnValue === true) && (!$this->contains($this->explode($this->lowercase($this->getenvironment('path')), ';'), 'sendmail'))) {
                $returnValue = "This is a windows machine, and sendmail is not on the path.";
            }

            /**
             * Caution (Windows only)
             * When PHP is talking to a SMTP server directly, if a full stop is found on the start of a line, it is removed. To counter-act this, replace these occurrences with a double dot.
             * @see http://php.net/manual/en/function.mail.php
             */
            $subject = str_replace("\n.", "\n..", $subject);
            $body = str_replace("\n.", "\n..", $body);

        }


        /**
         * If we haven't run into an error, go ahead and attempt the mail.
         */
        if ($returnValue === true) {
            try {
                $working_sendTo = trim(is_array($sendTo) ? implode(',', $sendTo) : $sendTo);
                $returnValue = mail($working_sendTo, $subject, $body, $headers);
            } catch (\Exception $e) {
                $returnValue = $e->getCode() . ":" . $e->getMessage();
            }
        }

        /**
         * If this is a true, everything worked.  Otherwise, we ran into a problem.
         */
        return $returnValue;
    }

    /**
     * Returns the value of an PHP ini variable.
     * @param string $variable
     * @return string|false The value of the PHP ini variable; false if it does not exist.
     * @see http://php.net/manual/en/function.getenv.php
     */
    public function getini($variable)
    {
        return ini_get($variable);
    }

    /**
     * @return bool True if the operating system is Windows; false otherwise.
     */
    public function iswindows()
    {
        $os = $this->lowercase($this->platform());
        return (($os === 'win32') || ($os === 'winnt') || ($os === 'windows'));
    }

    /**
     * Converts the uppercase letters in the specified string to lowercase.
     * @param string|array $string The string you want to convert to lowercase.
     * @return string|array The string, converted to lowercase letters.
     */
    function lowercase($string)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();
        foreach ($working as $v) {
            if (is_string($v)) {
                array_push($returnValue, strtolower($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns the name of the currently running platform version of Lotus Notes/Domino.
     * @param string $specific A string of characters recognized by php_uname, namely : s (Operating system name), n (Host name), r (Release name), v (Version information), m (Machine type), a (combo of s n r v m).
     * @return string
     * @see http://php.net/manual/en/function.php-uname.php
     */
    public function platform($specific = '')
    {

        $returnValue = PHP_OS;

        if (is_string($specific) && ($specific !== '')) {


            if (stripos($specific, 'a') !== false) {
                $specific = 'a';
            }

            $info = array();
            $working = str_split($specific);
            foreach ($working as $v) {
                array_push($info, php_uname($v));
            }
            $returnValue = implode(' ', $info);

        }

        return $returnValue;

    }

    /**
     * Determines whether a substring is stored within a string. This function is case-sensitive.
     * @param string|array $string Text or text list. The string(s) you want to search.
     * @param string|array $substring Text or text list. The string(s) you want to search for in string.
     * @return bool Returns true (1) if any substring is contained in one of the strings.  Returns false (0) if no substrings are contained in any of the strings. If any element in the substrings is a null string(""), this function always returns true.
     */
    function contains($string, $substring)
    {

        $returnValue = false;
        $string_array = $this->returnAsArray($string);
        $substring_array = $this->returnAsArray($substring);

        foreach ($string_array as $string_value) {
            foreach ($substring_array as $substring_value) {
                if (($substring_value === '') || (strpos($string_value, $substring_value) !== false)) {
                    $returnValue = true;
                    break;
                }
            }
            if ($returnValue) {
                break;
            }
        }

        return $returnValue;
    }

    /**
     * Returns a text list composed of the elements of a text string. Elements are defined as sequences of characters separated by separator characters and newlines.
     * @param string $string The string that you want to make into a text list.
     * @param string $separators One or more characters that define the end of an element in string. The default separators are " ,;" (space, comma, semicolon), which means that Lotus Domino adds a new element to the text list each time a space, comma, or semicolon occurs in the original string. When you use more than one character to specify separators, each character defines one separator. For example, the specification "and" breaks the string at each occurrence of "a," "n," and "d"; it does not break the string at each occurrence of the word "and." The newline is a separator, regardless of the specification of this parameter, unless newlineAsSeparator is specified as False.
     * @param bool [$includeEmpties=false] Specify True (1) to place an empty string ("") in the returned list when a separator appears at the beginning or end of the string, or two separators appear consecutively in the string. Specify False (0) to not include empty list elements for leading, trailing, and consecutive separators.
     * @param bool [$newlineAsSeparator=true] Specify True (1) to treat the newline as a separator. Specify False (0) to not treat the newline as a separator.
     * @return array A list containing each element found in string.
     */
    function explode($string, $separators = ' ,;', $includeEmpties = false, $newlineAsSeparator = true)
    {
        $returnValue = array();

        if (is_string($string)) {

            if (is_string($separators)) {
                $separators_array = array();
                for ($i = 0; $i < strlen($separators); $i++) {
                    array_push($separators_array, $this->substr($separators, $i, 1));
                }
            } else {
                $separators_array = array(' ', ',', ';');
            }

            if ($newlineAsSeparator !== false) {
                array_push($separators_array, "\n", "\r");
            }

            $charpos = 0;
            while ($string !== '') {
                // Get the current character we are reviewing, to see if it is a separator.
                $string_char = $this->substr($string, $charpos, 1);

                if (in_array($string_char, $separators_array)) {
                    // We have found a separator.  Capture the word.
                    if ($charpos === 0) {
                        $element = '';
                    } else {
                        $element = $this->substr($string, 0, $charpos);
                    }

                    // Create our new string to explode, by removing the element PLUS the separator.
                    $string = $this->substr($string, $charpos + 1);
                    // Reset our search.
                    $charpos = 0;

                    // Ok, now what do we do with the element?
                    if (($element === '') && ($includeEmpties === true)) {
                        array_push($returnValue, $element);
                    } else {
                        array_push($returnValue, $element);
                    }

                } elseif ($charpos === (strlen($string) - 1)) {
                    // We are at the end.  We are done.
                    array_push($returnValue, $string);
                    $string = '';

                } else {
                    // Keep looking for a separator character.
                    $charpos++;
                }

            }   // while


        } else {
            array_push($returnValue, $string);
        }

        return $returnValue;
    }

    /**
     * Returns the portion of string specified by the start and length parameters.  NEVER returns false (returns '' on false).
     * @param string $string
     * @param int $start
     * @param int [$length]
     * @returns string
     */
    private function substr($string, $start, $length = null)
    {
        if (is_int($length)) {
            $returnValue = substr($string, $start, $length);
        } else {
            $returnValue = substr($string, $start);
        }
        return ($returnValue === false ? '' : $returnValue);
    }

    /**
     * Returns the value of an environment variable.
     * @param string $variable
     * @return string|false The value of the environment variable; false if it does not exist.
     * @see http://php.net/manual/en/function.getenv.php
     */
    public function getenvironment($variable)
    {
        return getenv($variable);
    }

    /**
     * Queries the Windows registry for a specified value.
     * @param string $keyName
     * @param string $defaultIfNotFound If the key is not found, return this value.
     * @param boolean $returnExceptionIfNotFound If true, AND the key is not found, then return the exception. Useful for debugging.
     * @return string|int|string[]|int[]
     */
    public function regqueryvalue($keyName, $defaultIfNotFound = '', $returnExceptionIfNotFound = false)
    {

        $returnValue = $defaultIfNotFound;

        if ($this->isappinstalled('com_dotnet')) {

            try {

                /** @noinspection PhpUndefinedClassInspection */
                $shell = new \COM("WScript.Shell");
                /** @noinspection PhpUndefinedMethodInspection */
                $returnValue = $shell->RegRead($keyName);

            } catch (\Exception $e) {

                if ($returnExceptionIfNotFound) {
                    $returnValue = $e;
                }

            }

        }

        return $returnValue;

    }

    /**
     * Find out whether an extension is loaded.
     * @param string $application
     * @return bool
     */
    public function isappinstalled($application)
    {
        return extension_loaded($application);
    }

    /**
     * Returns the literal Web if we are invoked from a brower; CLI otherwise.
     * @return string
     */
    function clienttype()
    {
        if (isset($_SERVER) && isset($_SERVER['HTTP_HOST'])) {
            $returnValue = 'WEB';
        } else {
            // $returnValue = PHP_SAPI;
            $returnValue = 'CLI';
        }

        return $returnValue;
    }

    /**
     * Returns the number of the hour in the specified time­date.
     * @param Datetime|array $timeDateValue The value with the hour that you want to extract.
     * @return int A number representing the hour contained in timeDateValue.
     */
    function hour($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('g'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Extracts the number of minutes from the specified time­date.
     * @param Datetime|array $timeDateValue The value with the minute that you want to extract.
     * @return int The number of minutes in the minute part of the time.
     */
    function minute($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('i'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Extracts and returns the seconds value from the specified time­date.
     * @param Datetime|array $timeDateValue The value with the second that you want to extract.
     * @return int The number of seconds in the second part of the time.
     */
    function second($timeDateValue)
    {

        $working = $this->returnAsArray($timeDateValue);
        $returnValue = array();

        foreach ($working as $v) {
            if ($v instanceof DateTime) {
                array_push($returnValue, intval($v->format('s'), 10));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a DateTime (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Calculate the soundex key of a string.
     * @param string|string[] $string
     * @return string|array
     */
    function soundex($string)
    {
        $working = $this->returnAsArray($string);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_string($v)) {
                array_push($returnValue, soundex($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Truncates the values in a number or number list at the whole number, leaving off any decimals.
     * @param int|float|array $numberValue The value(s) you want to truncate.
     * @return int|array The truncated value(s).
     */
    function integer($numberValue)
    {

        $working = $this->returnAsArray($numberValue);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_float($v)) {
                $v = intval($v, 10);
            }
            array_push($returnValue, $v);
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Indicates if a piece of text (or a text list) is contained within another text list. The function is case-sensitive.
     * @param string|array $textListValue1 (aka textValue)
     * @param $textListValue2
     * @return bool Returns 1 (True) if the textValue is contained in textListValue.  Returns 0 (False) if not.  If both parameters are lists, returns 1 if all elements of textListValue1 are contained in textListValue2
     */
    function isnotmember($textListValue1, $textListValue2)
    {
        return !$this->ismember($textListValue1, $textListValue2);
    }

    /**
     * Indicates if a piece of text (or a text list) is contained within another text list. The function is case-sensitive.
     * @param string|array $textListValue1 (aka textValue)
     * @param $textListValue2
     * @return bool Returns 1 (True) if the textValue is contained in textListValue.  Returns 0 (False) if not.  If both parameters are lists, returns 1 if all elements of textListValue1 are contained in textListValue2
     */
    function ismember($textListValue1, $textListValue2)
    {
        $returnValue = false;

        if (is_scalar($textListValue1) && is_array($textListValue2)) {
            $returnValue = in_array($textListValue1, $textListValue2);
        } elseif (is_array($textListValue1) && is_array($textListValue2)) {
            $returnValue = true;
            foreach ($textListValue1 as $v) {
                if (!in_array($v, $textListValue2)) {
                    $returnValue = false;
                    break;
                }
            }
        }

        return $returnValue;
    }

    /**
     * Tests for a null value.
     * @param * $value Any data type. Any value.
     * @return bool
     */
    function isnull($value)
    {
        return $value === null;
    }

    /**
     * Indicates if a given value is a number (or ALL values in a number list).
     * @param * $value
     * @return bool Returns 1 (True) if the value is a number or a number list.  Returns 0 (False) if the value is not a number or a number list.
     */
    function isnumber($value)
    {

        $working = $this->returnAsArray($value);
        $returnValue = true;

        foreach ($working as $v) {
            $returnValue = (is_int($v) || is_float($v));
            if (!$returnValue) {
                break;
            }
        }

        return $returnValue;

    }

    /**
     * Indicates if a given value is a string (or ALL values in a string list).
     * @param * $value
     * @return bool Returns 1 (True) if the value is a string or a string list.  Returns 0 (False) if the value is not a string or a string list.
     */
    function istext($value)
    {

        $working = $this->returnAsArray($value);
        $returnValue = true;

        foreach ($working as $v) {
            $returnValue = is_string($v);
            if (!$returnValue) {
                break;
            }
        }

        return $returnValue;

    }

    /**
     * Indicates if a given value is a DateTime (or ALL values in a time-date list).
     * @param * $value
     * @return bool Returns 1 (True) if the value is a DateTime or a DateTime list.  Returns 0 (False) if the value is not a DateTime or a DateTime list.
     */
    function istime($value)
    {

        $working = $this->returnAsArray($value);
        $returnValue = true;

        foreach ($working as $v) {
            $returnValue = $v instanceof DateTime;
            if (!$returnValue) {
                break;
            }
        }

        return $returnValue;

    }

    /**
     * Given two text lists, returns only those items from the second list that are found in the first list.
     * @param array|string $textList1 A list of items.
     * @param array|string $textList2 A list of items that you want to compare to textList1.
     * @param string [$separator=?. ,!;:[](){}"<>] One or more characters to be used as delimiters between words. @Keywords considers each character (not the combination of multiple characters) to be a delimiter. For example, defining separator as ". ," (period, space, comma) tells the function to separate the text at each period, space, and comma into separate words.
     * @return array Text list. When a separator is in effect, either by default or specification, @Keywords parses textList1 into words delimited by the separator and returns any word that exactly matches a keyword in textList2. When no separator is in effect (when you specify a null separator), @Keywords returns any sequence of characters in textList1 that matches a keyword specified in textList2.
     */
    function keywords($textList1, $textList2, $separator = '?. ,!;:[](){}"<>')
    {
        $returnValue = array();

        $tl1_array = $this->returnAsArray($textList1);
        $tl2_array = $this->returnAsArray($textList2);


        foreach ($tl1_array as $v1) {

            if ($separator === '') {
                // No separator is in effect.  Return all elements from $textList2 that appear anywhere in $textList1.

                foreach ($tl2_array as $v2) {
                    if ($this->contains($v1, $v2)) {
                        $returnValue[$v2] = $v2;
                    }
                }

            } else {

                // A separator is in effect.  Explode the single value from $textList1 by the separator, and compare to all values in $textList2.
                $v1 = $this->explode($v1, $separator);
                foreach ($v1 as $v1_explodedvalue) {
                    foreach ($tl2_array as $v2) {
                        if ($v1_explodedvalue === $v2) {
                            $returnValue[$v2] = $v2;
                        }
                    }
                }

            }

        }

        // Return as simple indexed array.
        $returnValue = array_values($returnValue);

        return $returnValue;
    }

    /**
     * Searches a string from left to right and returns the leftmost characters of the string.
     * @param array|string $stringToSearch The string where you want to find the leftmost characters.
     * @param int|float|string $subString_or_numberOfChars Number. The number of characters to return. If the number is 2, the first two characters of the string are returned; if the number is 5, the first five characters are returned, and so on. If the number is negative, the entire string is returned.  Text. A substring of stringToSearch. @Left returns the characters to the left of subString. It finds subString by searching stringToSearch from left to right.
     * @return string|array The leftmost characters in stringToSearch. The number of characters returned is determined by either numberOfChars or subString. Returns "" if subString is not found in stringToSearch.
     */
    function left($stringToSearch, $subString_or_numberOfChars)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($subString_or_numberOfChars)) {
            $subString_or_numberOfChars = intval($subString_or_numberOfChars, 10);
        }

        foreach ($search as $v) {

            if (is_string($subString_or_numberOfChars)) {
                $pos = strpos($v, $subString_or_numberOfChars);
                if ($pos === false) {
                    $valueToReturn = '';
                } else {
                    $valueToReturn = $this->substr($v, 0, $pos);
                }
            } elseif (is_int($subString_or_numberOfChars)) {
                if ($subString_or_numberOfChars < 0) {
                    $valueToReturn = $v;
                } else {
                    $valueToReturn = $this->substr($v, 0, $subString_or_numberOfChars);
                }
            } else {
                $valueToReturn = $v;
            }

            array_push($returnValue, $valueToReturn);

        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Performs a find-and-replace operation on a text list.
     * @param string|array $sourcelist The list whose values you want to scan.
     * @param string|array $fromlist A list containing the values that you want to replace.
     * @param string|array $tolist A list containing the replacement values.
     * @return array The sourcelist, with any values from fromlist replaced by the corresponding value in tolist. If none of the values in fromlist matched the values in sourcelist, then sourcelist is returned unaltered.
     */
    function replace($sourcelist, $fromlist, $tolist)
    {
        $returnValue = array();

        $w_sourcelist = $this->returnAsArray($sourcelist);
        $w_fromlist = $this->returnAsArray($fromlist);
        $w_tolist = $this->returnAsArray($tolist);

        foreach ($w_sourcelist as $sourcevalue) {

            $replacementvalue = $sourcevalue;

            foreach ($w_fromlist as $fromkey => $fromvalue) {

                if ($sourcevalue === $fromvalue) {
                    /**
                     * We found a match.  Replace it.
                     */

                    if (array_key_exists($fromkey, $w_tolist)) {
                        $tovalue = $tolist[$fromkey];
                    } else {
                        $tovalue = '';
                    }

                    /**
                     * Store the replacement value.
                     */
                    $replacementvalue = $tovalue;

                    /**
                     * Done processing.
                     */
                    break;
                }

            }

            array_push($returnValue, $replacementvalue);

        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Returns the rightmost characters in the string.
     * @param array|string $stringToSearch The string whose rightmost characters you want to find.
     * @param int|float|string $subString_or_numberOfChars Number. Counting from left to right, the number of characters to skip. All the characters to the right of that number are returned.  Text. A substring of stringToSearch. @RightBack returns all the characters to the right of subString. It finds subString by searching stringToSearch from right to left.
     * @return string|array The rightmost characters in stringToSearch. The number of characters returned is determined by either numberOfChars or subString.
     */
    function rightback($stringToSearch, $subString_or_numberOfChars)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($subString_or_numberOfChars)) {
            $subString_or_numberOfChars = intval($subString_or_numberOfChars, 10);
        }

        foreach ($search as $v) {

            if (is_string($subString_or_numberOfChars)) {
                $pos = strrpos($v, $subString_or_numberOfChars);
                if ($pos === false) {
                    $valueToReturn = '';
                } else {
                    $valueToReturn = $this->substr($v, $pos + strlen($subString_or_numberOfChars));
                }
            } elseif (is_int($subString_or_numberOfChars)) {
                if ($subString_or_numberOfChars < 0) {
                    // Not valid.  Return nothing.
                    $valueToReturn = '';
                } else {
                    // @RightBack("Lennard Wallace";3) returns 'nard Wallace'
                    $valueToReturn = $this->substr($v, $subString_or_numberOfChars);
                }
            } else {
                $valueToReturn = $v;
            }

            array_push($returnValue, $valueToReturn);

        }


        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Repeats a string a specified number of times.
     * @param string|array $string The string you want to repeat.
     * @param int $number The number of times you want to repeat string.
     * @param int [$numberchars=null] Optional. The maximum number of characters you want returned. @Repeat truncates the result to this number.
     * @return string
     */
    function repeat($string, $number, $numberchars = null)
    {

        $returnValue = array();
        $working = $this->returnAsArray($string);

        foreach ($working as $s) {

            if (is_string($s)) {
                $valueToReturn = str_repeat($string, $number);
                if (is_int($numberchars)) {
                    $valueToReturn = $this->substr($valueToReturn, 0, $numberchars);
                }
            } else {
                $valueToReturn = $string;
            }

            array_push($returnValue, $valueToReturn);
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Rounds the designated number to the nearest whole number; if an additional number is specified, it is used as the rounding factor.
     * @param int|float|array $number Numbers to be rounded.
     * @param int [$factor] Number. Optional. The rounding factor to use. For example, if factor is 10, @Round rounds to the nearest number that is a factor of 10. If you don't specify a factor, the number is rounded to the nearest whole number.  CURRENTLY, only honors multiples of 10 (e.g., 10, 100, 1000, etc).
     * @return float|array The value of number, rounded to the specified factor or to the nearest whole number. If number is a list, each number in the list is rounded to the specified factor or to the nearest whole number.
     */
    function round($number, $factor = null)
    {

        $returnValue = array();
        $working = $this->returnAsArray($number);

        if (is_int($factor)) {
            $exp = intval($factor / 10);
        } else {
            $exp = 0;
        }

        foreach ($working as $v) {
            if (is_float($v) || (is_int($v))) {
                if (is_int($factor)) {
                    array_push($returnValue, round($v, $exp * -1));
                } else {
                    array_push($returnValue, round($v));
                }
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Adds a set of numbers or number lists.
     * @param number|array $numbers 0 or more numbers/arrays of numbers to add.  Can pass in more than 1 parameter!
     * @return int|float The sum of all the numbers, including members of number lists.
     */
    function sum($numbers)
    {

        /**
         * We will start off as an integer; if we run across a float, this will automatically be converted.
         */
        $returnValue = 0;

        $all_arguments = func_get_args();
        foreach ($all_arguments as $numbers_arg) {
            $working = $this->returnAsArray($numbers_arg);
            foreach ($working as $singlenumber) {
                if (is_float($singlenumber) || is_int($singlenumber)) {
                    $returnValue += $singlenumber;
                }
            }
        }

        return $returnValue;

    }

    /**
     * Sorts a list
     * @param array $array The array to sort.
     * @param array $order You can use the following keywords to specify the order of the sort: ASC, DESC, BYKEY, BYVAL, CASEINSENSITIVE, CASESENSITIVE, ASNUM.  You can specify zero or more keywords.  If conflicting keywords are passed, the last one in the list affects the sort order.
     * @param string|array The custom sorting function to call.  Bypasses all keywords except BYKEY and BYVAL.  If used, the sorting function MUST have the following signature : int callback ( mixed $a, mixed $b )
     * @return array|bool The sorted array; false if the sort failed.
     */
    function sort($array, $order = array(), $customFunction = null)
    {

        /**
         * Housekeeping.  Set up all our default values.
         */
        $desc = false;
        $bykey = false;
        $caseinsensitive = true;
        $asnumber = false;

        $working_order = $this->returnAsArray($order);

        /**
         * Process all the keywords, overriding our defaults.
         */
        foreach ($working_order as $v) {

            if ((strcasecmp($v, 'asc') === 0) || (strcasecmp($v, 'ascending') === 0)) {
                $desc = false;
            } elseif ((strcasecmp($v, 'desc') === 0) || (strcasecmp($v, 'descending') === 0)) {
                $desc = true;

            } elseif ((strcasecmp($v, 'byval') === 0) || (strcasecmp($v, 'byvalue') === 0)) {
                $bykey = false;
            } elseif (strcasecmp($v, 'bykey') === 0) {
                $bykey = true;

            } elseif ((strcasecmp($v, 'asnum') === 0) || (strcasecmp($v, 'asnumber') === 0)) {
                $asnumber = true;

            } elseif (strcasecmp($v, 'casesensitive') === 0) {
                $caseinsensitive = false;
            } elseif (strcasecmp($v, 'caseinsensitive') === 0) {
                $caseinsensitive = true;
            }
        }


        /**
         * If EVERYTHING in the array is a number, we are going to sort the array as a number.
         */
        $everythingIsANumber = true;
        foreach ($array as $v) {
            if (is_int($v) || is_float($v)) {
                // Good.  Keep going.
            } else {
                $everythingIsANumber = false;
                break;
            }
        }


        /**
         * Sort our array.
         * @see http://php.net/manual/en/array.sorting.php
         * @see http://php.net/manual/en/function.sort.php
         */

        $sort_flags = SORT_REGULAR;
        if ($asnumber || $everythingIsANumber) {
            $sort_flags = SORT_NUMERIC;
        } elseif ($caseinsensitive) {
            $sort_flags = SORT_STRING | SORT_FLAG_CASE;
        }

        if ($bykey) {
            if ($customFunction !== null) {
                // User takes care of everything.
                $returnValue = uksort($array, $customFunction);
            } elseif (!$desc) {
                $returnValue = ksort($array, $sort_flags);
            } else {
                $returnValue = krsort($array, $sort_flags);
            }
        } else {
            if ($customFunction !== null) {
                // User takes care of everything.
                $returnValue = usort($array, $customFunction);
            } elseif (!$desc) {
                $returnValue = sort($array, $sort_flags);
            } else {
                $returnValue = rsort($array, $sort_flags);
            }
        }

        return ($returnValue === false ? false : $array);
    }

    /**
     * Converts a text string to a number, where possible.
     * @param string|array $number The string you want to convert to a number. If the string contains both numbers and letters, it must begin with a number to be converted properly. For example, the string "12ABC" converts to 12, but "ABC12" produces an error.
     * @return int|float|array The string, converted to a number.
     */
    function texttonumber($number)
    {

        $returnValue = array();
        $working = $this->returnAsArray($number);

        foreach ($working as $sv) {

            if (is_float($sv)) {
                $numericvalue = $sv;
            } elseif (is_int($sv)) {
                $numericvalue = $sv;
            } elseif (stripos($sv, '.') !== false) {
                $numericvalue = floatval($sv);
            } else {
                $numericvalue = intval($sv, 10);
            }

            array_push($returnValue, $numericvalue);
        }


        // If we were sent in a single string, (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Converts a text string to a DateTime, where possible.
     * @param string|array $date The string you want to convert to a date. If the string could not be converted to a DateTime, then false.
     * @return DateTime|array The string, converted to a DateTime object (false if it could not be converted).
     */
    function texttotime($date)
    {

        $returnValue = array();
        $working = $this->returnAsArray($date);

        foreach ($working as $sv) {
            $datevalue = strtotime($sv);
            if ($datevalue !== false) {
                $dt = new DateTime();
                $datevalue = $dt->setTimestamp($datevalue);
            }
            array_push($returnValue, $datevalue);
        }


        // If we were sent in a single string, (as opposed to an array), return only a date (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Searches a string from right to left and returns a substring.
     * @param array|string $stringToSearch The string where you want to find the leftmost characters.
     * @param int|float|string $startString_or_numToSkip Number. Counting from right to left, the number of characters to skip. All the characters to the left of that number of characters are returned. If the number is negative, the entire string is returned.  Text. A substring of stringToSearch. All the characters to the left of startString are returned.
     * @return string|array The leftmost characters in stringToSearch. The number of characters returned is determined by either numToSkip or startString.
     */
    function leftback($stringToSearch, $startString_or_numToSkip)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($startString_or_numToSkip)) {
            $startString_or_numToSkip = intval($startString_or_numToSkip, 10);
        }

        foreach ($search as $v) {

            if (is_string($startString_or_numToSkip)) {
                $pos = strrpos($v, $startString_or_numToSkip);
                if ($pos === false) {
                    $valueToReturn = '';
                } else {
                    $valueToReturn = $this->substr($v, 0, $pos);
                }
            } elseif (is_int($startString_or_numToSkip)) {
                if ($startString_or_numToSkip < 0) {
                    $valueToReturn = $v;
                } else {
                    $valueToReturn = $this->substr($v, 0, $startString_or_numToSkip * -1);
                }
            } else {
                $valueToReturn = $v;
            }

            array_push($returnValue, $valueToReturn);

        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns the number of characters in a text string.
     * @param string|array $string A single string, or list of strings, with the length you want to find.
     * @return array|int
     */
    function length($string)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_string($v)) {
                array_push($returnValue, strlen($v));
            } else {
                array_push($returnValue, 0);
            }
        }

        // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Given a value, finds its position (1-based) in a text list.
     * @param string $value The value you want to find in stringlist.
     * @param array $stringlist Text list.
     * @return int Returns 0 if the value is not contained in stringlist. Returns 1 to n if the value is contained in the stringlist, where 1 to n is the position of the value in the stringlist.
     */
    function member($value, $stringlist)
    {
        $returnValue = 0;

        $array = $this->returnAsArray($stringlist);
        $pos = 0;
        foreach ($array as $v) {
            $pos++;
            if ($value === $v) {
                $returnValue = $pos;
                break;
            }
        }

        return $returnValue;
    }

    /**
     * Returns any substring from the middle of a string. The middle is found by scanning the string from left to right, and parameters determine where the middle begins and ends.
     * @param string $stringToSearch Any string.
     * @param string|int $offset_or_startString Number (1-based). A character position in string that indicates where you want the middle to begin, always counting from left to right. The middle begins one character after the offset. Text. A substring of string that indicates where you want the middle to begin, always counting from left to right. The middle begins one character after the end of startString.
     * @param string|int $numberchars_or_endstring Number. The number of characters that you want in the middle. If numberchars is negative, the middle starts at offset or startString and continues from right to left. If numberchars is positive, the middle starts one character past the offset or startString and continues from left to right.  Text. A substring of string that indicates the end of the middle. @Middle returns all the characters between offset and endstring, or between startString and endstring.
     * @return array|string The substring from the middle of string, which begins at the offset or startString you specify and ends at the endstring you specify, or after the numberchars have been reached.
     */
    function middle($stringToSearch, $offset_or_startString, $numberchars_or_endstring)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($offset_or_startString)) {
            $offset_or_startString = intval($offset_or_startString, 10);
        }
        if (is_float($numberchars_or_endstring)) {
            $numberchars_or_endstring = intval($numberchars_or_endstring, 10);
        }

        foreach ($search as $v) {

            if (is_int($offset_or_startString) && is_int($numberchars_or_endstring)) {

                if ($numberchars_or_endstring >= 0) {
                    // @Middle("North Carolina";4;3) returns 'h C'
                    // Slicing left -> right
                    $valueToReturn = $this->substr($v, $offset_or_startString, $numberchars_or_endstring);
                } else {
                    // @Middle("North Carolina";4;­3) returns 'ort'
                    // Slicing right -> left
                    $valueToReturn = $this->substr($v, 0, $offset_or_startString);
                    $valueToReturn = $this->right($valueToReturn, abs($numberchars_or_endstring));
                }

            } elseif (is_int($offset_or_startString) && !is_int($numberchars_or_endstring)) {

                // We know the start of the middle.  Determine the end.
                $endOfMiddle_Start = strpos($v, $numberchars_or_endstring, $offset_or_startString);
                if ($endOfMiddle_Start === false) {
                    // Couldn't find the end of the middle ... return the string from the middle start to the end of the original string
                    $valueToReturn = $this->substr($v, $offset_or_startString);
                } else {
                    // @Middle("This is the text"; 4; "text") returns ' is the '
                    $valueToReturn = $this->substr($v, $offset_or_startString, ($endOfMiddle_Start - $offset_or_startString));
                }


            } elseif (!is_int($offset_or_startString) && is_int($numberchars_or_endstring)) {

                $startOfMiddle_Start = strpos($v, $offset_or_startString);

                if ($startOfMiddle_Start === false) {
                    // We couldn't find the start of the middle ... return nothing.
                    $valueToReturn = '';
                } else {
                    if ($numberchars_or_endstring >= 0) {
                        // Simple left to right substr
                        // Skip the length of the word that defines the start of the middle.
                        $startOfMiddle_Start += strlen($offset_or_startString);
                        // @Middle("North Carolina";" ";3) returns 'Car'
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start, $numberchars_or_endstring);
                    } else {
                        // Little more complicated right to left substr
                        // @Middle("North Carolina";"th";­2) returns 'or'
                        $valueToReturn = $this->substr($v, 0, $startOfMiddle_Start);
                        $valueToReturn = $this->right($valueToReturn, abs($numberchars_or_endstring));
                    }
                }

            } else {
                // ( ! is_int($offset_or_startString) && ! is_int($numberchars_or_endstring)) {
                $startOfMiddle_Start = strpos($v, $offset_or_startString);
                // Skip the length of the word that defines the start of the middle.
                if ($startOfMiddle_Start !== false) {
                    $startOfMiddle_Start += strlen($offset_or_startString);
                }

                if ($startOfMiddle_Start === false) {
                    // We couldn't find the start of the middle ... return nothing.
                    $valueToReturn = '';
                } else {
                    $endOfMiddle_Start = strpos($v, $numberchars_or_endstring, $startOfMiddle_Start);
                    if ($endOfMiddle_Start === false) {
                        // Coudn't find the close of the middle ... return everything
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start);
                    } else {
                        // @Middle("This is the text"; "is"; "text") returns ' is the '
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start, ($endOfMiddle_Start - $startOfMiddle_Start));
                    }
                }
            }


            array_push($returnValue, $valueToReturn);

        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns the rightmost characters in the string. You can specify the number of rightmost characters you want returned, or you can indicate that you want all the characters to the right of a specific substring.
     * @param array|string $stringToSearch The string where you want to find the rightmost characters.
     * @param int|float|string $subString_or_numberOfChars Number. The number of characters to return. If the number is 2, the last two characters of stringToSearch are returned; if the number is 5, the last five characters are returned, and so on.  Text. A substring of stringToSearch. @Right returns all of the characters to the right of subString. It finds subString by searching stringToSearch from left to right.
     * @return string|array The leftmost characters in stringToSearch. The number of characters returned is determined by either numberOfChars or subString. Returns "" if subString is not found in stringToSearch.
     */
    function right($stringToSearch, $subString_or_numberOfChars)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($subString_or_numberOfChars)) {
            $subString_or_numberOfChars = intval($subString_or_numberOfChars, 10);
        }

        foreach ($search as $v) {

            if (is_string($subString_or_numberOfChars)) {
                $pos = strpos($v, $subString_or_numberOfChars);
                if ($pos === false) {
                    $valueToReturn = '';
                } else {
                    $valueToReturn = $this->substr($v, $pos + strlen($subString_or_numberOfChars));
                }
            } elseif (is_int($subString_or_numberOfChars)) {
                if ($subString_or_numberOfChars < 0) {
                    $valueToReturn = $v;
                } else {
                    $valueToReturn = $this->substr($v, $subString_or_numberOfChars * -1);
                }
            } else {
                $valueToReturn = $v;
            }

            array_push($returnValue, $valueToReturn);

        }


        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Returns any substring from the middle of a string. The middle is found by scanning the string from right to left, and parameters determine where the middle begins and ends.
     * @param string $stringToSearch Any string.
     * @param string|int $offset_or_startString Number (1-based). A character position in string that indicates where you want the middle to begin, always counting from right to left. The middle begins one character after the offset. Text. A substring of string that indicates where you want the middle to begin, always counting from right to left. The middle begins one character after the end of startString.
     * @param string|int $numberchars_or_endstring Number. The number of characters that you want in the middle. If numberchars is negative, the middle starts at offset or startString and continues from right to left. If numberchars is positive, the middle starts one character past the offset or startString and continues from left to right.  Text. A substring of string that indicates the end of the middle. @MiddleBack returns all the characters between offset and endstring, or between startString and endstring.
     * @return array|string The substring from the middle of string, which begins at the offset or startString you specify and ends at the endstring you specify, or after the numberchars have been reached.
     */
    function middleback($stringToSearch, $offset_or_startString, $numberchars_or_endstring)
    {

        $returnValue = array();

        $search = $this->returnAsArray($stringToSearch);
        if (is_float($offset_or_startString)) {
            $offset_or_startString = intval($offset_or_startString, 10);
        }
        if (is_float($numberchars_or_endstring)) {
            $numberchars_or_endstring = intval($numberchars_or_endstring, 10);
        }

        foreach ($search as $v) {

            if (is_int($offset_or_startString) && is_int($numberchars_or_endstring)) {

                $startOfMiddle_Start = strlen($v) - $offset_or_startString + 1;

                if ($numberchars_or_endstring >= 0) {
                    // @Middleback("This is a test"; 4; 3) returns 'est'
                    $valueToReturn = $this->substr($v, $startOfMiddle_Start, $numberchars_or_endstring);
                } else {
                    // @Middleback("This is a test"; 4; -3) returns 'a t'
                    $valueToReturn = $this->substr($v, 0, $startOfMiddle_Start);
                    $valueToReturn = $this->right($valueToReturn, abs($numberchars_or_endstring));
                }

            } elseif (is_int($offset_or_startString) && !is_int($numberchars_or_endstring)) {

                $endOfMiddle_Start = strlen($v) - $offset_or_startString;

                // We know the start of the middle.  Determine the end.
                $startOfMiddle_Start = strrpos($this->substr($v, 0, $endOfMiddle_Start), $numberchars_or_endstring);

                if ($startOfMiddle_Start === false) {
                    // Couldn't find the start of the middle ... return up to the end
                    $valueToReturn = $this->substr($v, 0, $endOfMiddle_Start + 1);
                } else {
                    $startOfMiddle_Start += strlen($numberchars_or_endstring);
                    // @Middleback("This is a test"; 4; 'is') returns ' a t'
                    $valueToReturn = $this->substr($v, $startOfMiddle_Start, ($endOfMiddle_Start - $startOfMiddle_Start + 1));
                }


            } elseif (!is_int($offset_or_startString) && is_int($numberchars_or_endstring)) {

                $startOfMiddle_Start = strrpos($v, $offset_or_startString);

                if ($startOfMiddle_Start === false) {
                    // We couldn't find the start of the middle ... return nothing.
                    $valueToReturn = '';
                } else {
                    if ($numberchars_or_endstring >= 0) {
                        // Simple left to right substr
                        // Skip the length of the word that defines the start of the middle.
                        $startOfMiddle_Start += strlen($offset_or_startString);
                        // @Middleback("This is a test"; 'is'; 4) returns ' a t'
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start, $numberchars_or_endstring);
                    } else {
                        // Little more complicated right to left substr
                        // @Middleback("This is a test"; 'is'; -4) returns 'his '
                        $valueToReturn = $this->substr($v, 0, $startOfMiddle_Start);
                        $valueToReturn = $this->right($valueToReturn, abs($numberchars_or_endstring));
                    }
                }

            } else {
                // ( ! is_int($offset_or_startString) && ! is_int($numberchars_or_endstring)) {


                $endOfMiddle_Start = strrpos($v, $offset_or_startString);
                if ($endOfMiddle_Start === false) {
                    $startOfMiddle_Start = false;
                } else {
                    $startOfMiddle_Start = strrpos($this->substr($v, 0, $endOfMiddle_Start), $numberchars_or_endstring);
                }


                if (($startOfMiddle_Start === false) && ($endOfMiddle_Start === false)) {
                    // Can't do anything.
                    $valueToReturn = '';
                } elseif ($startOfMiddle_Start === false) {
                    // We couldn't find the start of the middle ... return everything up to the end
                    $valueToReturn = $this->substr($v, 0, $endOfMiddle_Start);
                } else {
                    if ($endOfMiddle_Start === false) {
                        // Coudn't find the close of the middle ... return everything from the start
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start);
                    } else {
                        $startOfMiddle_Start += strlen($numberchars_or_endstring);

                        // @Middleback("This is a test"; 'is'; 'test') returns 'This '
                        // @Middleback("This is a test"; 'test'; 'is') returns ' a '
                        $valueToReturn = $this->substr($v, $startOfMiddle_Start, ($endOfMiddle_Start - $startOfMiddle_Start));
                    }
                }
            }


            array_push($returnValue, $valueToReturn);

        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Searches a list from left to right and returns the number values you specify. If you specify a negative number, @Subset searches the list from right to left, but the result is ordered as from the beginning of the list.
     * @param array $list Text list, number list, or time-date list. The list whose subset you want.
     * @param int $number The number of values from list that you want. Specifying zero (0) returns the error, "The second argument to @Subset must not be zero."
     * @return string|array
     */
    function subset($list, $number)
    {
        $working = $this->returnAsArray($list);

        if ($number >= 0) {
            $returnValue = array_slice($working, 0, $number);
        } else {
            $returnValue = array_slice($working, $number);
        }

        if (count($returnValue) === 0) {
            $returnValue = '';
        } elseif (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Return today's date.
     * @return DateTime
     */
    function today()
    {
        return $this->now();
    }

    /**
     * Return a new DateTime object, initialized with the current time.
     * @return DateTime
     */
    function now()
    {
        return new DateTime();
    }

    /**
     * Evaluates the return value of a Password data type field with a number.
     * @param string $password The password whose strength you wish to test.
     * @return int The password strength.  The higher the better; 1 is the lowest, 10 is the highest.
     * @see http://www.phpro.org/examples/Password-Strength-Tester.html
     */
    function passwordquality($password)
    {

        if (strlen($password) === 0) {
            return 1;
        }

        $strength = 0;

        /*** get the length of the password ***/
        $length = strlen($password);

        /*** check if password is not all lower case ***/
        if (strtolower($password) != $password) {
            $strength += 1;
        }

        /*** check if password is not all upper case ***/
        if (strtoupper($password) == $password) {
            $strength += 1;
        }

        /*** check string length is 8 -15 chars ***/
        if ($length >= 8 && $length <= 15) {
            $strength += 1;
        }

        /*** check if lenth is 16 - 35 chars ***/
        if ($length >= 16 && $length <= 35) {
            $strength += 2;
        }

        /*** check if length greater than 35 chars ***/
        if ($length > 35) {
            $strength += 3;
        }

        /*** get the numbers in the password ***/
        preg_match_all('/[0-9]/', $password, $numbers);
        $strength += count($numbers[0]);

        /*** check for special chars ***/
        preg_match_all('/[|!@#$%&*\/=?,;.:\-_+~^\\\]/', $password, $specialchars);
        $strength += sizeof($specialchars[0]);

        /*** get the number of unique chars ***/
        $chars = str_split($password);
        $num_unique_chars = sizeof(array_unique($chars));
        $strength += $num_unique_chars * 2;

        /*** strength is a number 1-10; ***/
        $strength = $strength > 99 ? 99 : $strength;
        $strength = floor($strength / 10 + 1);

        return intval($strength, 10);

    }

    /**
     * In a Web application, sets the value of HTTP headers in the response being generated by the server for the browser client.
     * Remember that header() must be called before any actual output is sent, either by normal HTML tags, blank lines in a file, or from PHP. It is a very common error to read code with include, or require, functions, or another file access function, and have spaces or empty lines that are output before header() is called. The same problem exists when using a single PHP/HTML file.
     * @param string $responseHeader String. The name of a response-header field, for example, "Content-Encoding," "Content-Length," or "Set-Cookie." See http:/www.w3.org/Protocols for specifications of response headers.
     * @param string $value Text, number, or date. A value for the field. Dates are converted to RFC 1123 format. An empty string ("") removes the header and its value from the HTTP response.
     * @param boolean $replace The optional replace parameter indicates whether the header should replace a previous similar header, or add a second header of the same type. By default it will replace, but if you pass in FALSE as the second argument you can force multiple headers of the same type.
     * @param int $http_response_code Forces the HTTP response code to the specified value. Note that this parameter only has an effect if the string is not empty.
     * @see http://php.net/manual/en/function.header.php
     */
    function sethttpheader($responseHeader, $value, $replace = true, $http_response_code = null)
    {
        $header = "$responseHeader: $value";
        header($header, $replace, $http_response_code);
    }

    /**
     * Return tomorrow's date.
     * @return DateTime
     */
    function tomorrow()
    {
        return $this->now()->modify('+1 day');
    }

    /**
     * Return yesterdays's date.
     * @return DateTime
     */
    function yesterday()
    {
        return $this->now()->modify('-1 day');
    }

    /**
     * Converts a value with a data type of text or number to a number value.
     * @param string $value
     * @return int|float|array
     */
    function tonumber($value)
    {

        $working = $this->returnAsArray($value);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_float($v) || (is_int($v))) {
                array_push($returnValue, $v);
            } elseif (is_numeric($v)) {
                array_push($returnValue, floatval($v));
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a number (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Removes leading, trailing, and redundant spaces from a text string, or from each element of a text list.
     * @param string|array $string
     * @return string|array
     */
    function trim($string)
    {

        $working = $this->returnAsArray($string);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_string($v)) {
                $v = trim($v);
                if ($v != '') {
                    array_push($returnValue, trim($v));
                }
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 0) {
            $returnValue = '';
        } elseif (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;

    }

    /**
     * Without a parameter, returns a random, unique text value. With a parameter, removes duplicate values from a text list by returning only the first occurrence of each member of the list.
     * @param array [$textlist]
     * @return array|string
     */
    public function unique($textlist = null)
    {

        if (is_array($textlist)) {

            $working = $this->returnAsArray($textlist);
            $returnValue = array();
            foreach ($working as $v) {
                if (!in_array($v, $returnValue)) {
                    array_push($returnValue, $v);
                }
            }

            // If we were sent in a number (as opposed to an array), return only a number (instead of an array).
            if (count($returnValue) === 1) {
                $returnValue = $returnValue[0];
            }

        } else {

            $returnValue = '';
            $length = 32;

            while (strlen($returnValue) < $length) {
                $randnum = mt_rand(0, 61);
                if ($randnum < 10) {
                    $returnValue .= chr($randnum + 48);
                } elseif ($randnum < 36) {
                    $returnValue .= chr($randnum + 55);
                } else {
                    $returnValue .= chr($randnum + 61);
                }
            }

            $returnValue = strtoupper(md5(mt_rand() . microtime() . $returnValue));

        }

        return $returnValue;

    }

    /**
     * Returns the largest number in a single list, or the larger of two numbers or number lists.
     * @param int|float|array $number1 Number or number list.
     * @param int|float|array $number2 Number or number list.
     * @return array Either number1 or number2, whichever is larger. If the parameters are number lists, returns a list that is the result of pair­wise computation on the list values.
     */
    public function max($number1, $number2 = null)
    {

        $working1 = $this->returnAsArray($number1);
        $returnValue = array();

        if ($number2 === null) {
            /**
             * When we only have one list to work with, we return the largest number in the list.
             */
            $max = null;
            foreach ($working1 as $v) {
                if (is_int($v) || is_float($v)) {
                    if (($max === null) || ($v > $max)) {
                        $max = $v;
                    }
                }
            }


            if ($max === null) {
                $max = 0;
            }
            array_push($returnValue, $max);

        } else {
            /**
             * In this case, we return the maximum from each list.
             */

            $working2 = $this->returnAsArray($number2);
            if (count($working2) > count($working1)) {
                $working1 = $this->expandArrayWithLastElement($working1, count($working2));
            } else {
                $working2 = $this->expandArrayWithLastElement($working2, count($working1));
            }


            reset($working2);
            foreach ($working1 as $v1) {
                $v2 = current($working2);

                if (floatval($v2) > floatval($v1)) {
                    array_push($returnValue, $v2);
                } else {
                    array_push($returnValue, $v1);
                }

                next($working2);
            }

        }


        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Returns the smallest number in a single list, or the smaller of two numbers or number lists.
     * @param int|float|array $number1 Number or number list.
     * @param int|float|array $number2 Number or number list.
     * @return array Either number1 or number2, whichever is smaller. If the parameters are number lists, returns a list that is the result of pair­wise computation on the list values.
     */
    public function min($number1, $number2 = null)
    {

        $working1 = $this->returnAsArray($number1);
        $returnValue = array();

        if ($number2 === null) {
            /**
             * When we only have one list to work with, we return the smallest number in the list.
             */
            $min = null;
            foreach ($working1 as $v) {
                if (is_int($v) || is_float($v)) {
                    if (($min === null) || ($v < $min)) {
                        $min = $v;
                    }
                }
            }


            if ($min === null) {
                $min = 0;
            }
            array_push($returnValue, $min);

        } else {
            /**
             * In this case, we return the minimum from each list.
             */

            $working2 = $this->returnAsArray($number2);
            if (count($working2) > count($working1)) {
                $working1 = $this->expandArrayWithLastElement($working1, count($working2));
            } else {
                $working2 = $this->expandArrayWithLastElement($working2, count($working1));
            }


            reset($working2);
            foreach ($working1 as $v1) {
                $v2 = current($working2);

                if (floatval($v2) < floatval($v1)) {
                    array_push($returnValue, $v2);
                } else {
                    array_push($returnValue, $v1);
                }

                next($working2);
            }

        }


        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Returns the time zone setting of the current computer or of a time-date value.
     * @param string|int|DateTime $date Optional. The time-date whose zone you want to know.
     * @return string|string[]
     */
    public function zone($date = null)
    {

        $working = $this->returnAsArray($date);
        $returnValue = array();


        foreach ($working as $v) {

            $working_date = ($v === null ? new \DateTime() : $this->totime($v));
            if ($working_date instanceof DateTime) {
                array_push($returnValue, $working_date->format('T'));
            } else {
                array_push($returnValue, date_default_timezone_get());
            }


        }

        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Performs the equivalent of a strtotime().
     * @param string $value
     * @return DateTime|array
     */
    function totime($value)
    {

        $working = $this->returnAsArray($value);
        $returnValue = array();

        foreach ($working as $v) {
            if (is_string($v) && (strtotime($v) !== false)) {
                array_push($returnValue, new DateTime($v));
            } elseif (is_int($v)) {
                $dt = new DateTime();
                $dt->setTimestamp($v);
                array_push($returnValue, $dt);
            } else {
                array_push($returnValue, $v);
            }
        }

        // If we were sent in a string (as opposed to an array), return only a string (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;
    }

    /**
     * Validates an Internet email address based on the RFC 822 or RFC 821 Address Format Syntax.
     * @param string|string[] $address
     * @return bool|bool[]
     */
    public function validateinternetaddress($address)
    {

        $working = $this->returnAsArray($address);
        $returnValue = array();


        foreach ($working as $v) {

            if (is_string($v)) {
                array_push($returnValue, (filter_var($v, FILTER_VALIDATE_EMAIL) !== false));
            } else {
                array_push($returnValue, false);
            }

        }

        // If we were sent in a scalar (as opposed to an array), return only a scalar (instead of an array).
        if (count($returnValue) === 1) {
            $returnValue = $returnValue[0];
        }

        return $returnValue;


    }

    /**
     * Retrieves a World Wide Web page specified by its URL.
     * @param string $url
     * @param string $username
     * @param string $password
     * @return array
     */
    public function urlopen($url, $username = null, $password = null)
    {
        $returnValue = array();

        $curlinfo = array();
        $errno = null;
        $error = '';
        $errstep = '';

        /**
         * This should be of type resource.  If it is a boolean false, that means we couldn't even initialize curl.
         */
        $resource = false;

        if ($this->isappinstalled('curl')) {
            /**
             * Can we open the URL?
             * @type resource|bool
             */
            $resource = curl_init();

            if ($resource === false) {
                /**
                 * We cannot open the URL.  Store the error.
                 */
                $errno = 666;
                $error = 'curl_init() returned a false.';
                $errstep = 'curl_init()';
            } else {
                /**
                 * Set our options.
                 */
                $options_ok = true;

                /**
                 * The URL to fetch. This can also be set when initializing a session with curl_init().
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLOPT_URL, $url);

                /**
                 * TRUE to track the handle's request string.
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLINFO_HEADER_OUT, true);

                /**
                 * TRUE to return the response headers.
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLOPT_HEADER, true);

                /**
                 * TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);

                /**
                 * TRUE to attempt to retrieve the modification date of the remote document.
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLOPT_FILETIME, true);

                /**
                 * TRUE to follow any "Location: " header that the server sends as part of the HTTP header (note this is recursive, PHP will follow as many "Location: " headers that it is sent, unless CURLOPT_MAXREDIRS is set).
                 */
                $options_ok = $options_ok && curl_setopt($resource, CURLOPT_FOLLOWLOCATION, true);

                /**
                 * --- POSSIBLE SECURITY ISSUE ---
                 * FALSE to stop cURL from verifying the peer's certificate. Alternate certificates to verify against can be specified with the CURLOPT_CAINFO option or a certificate directory can be specified with the CURLOPT_CAPATH option.
                 * @see http://unitstep.net/blog/2009/05/05/using-curl-in-php-to-access-https-ssltls-protected-sites/
                 */
                if ($this->begins($this->lowercase($url), 'https://')) {
                    $options_ok = $options_ok && curl_setopt($resource, CURLOPT_SSL_VERIFYPEER, false);
                }

                /**
                 * A username and password formatted as "[username]:[password]" to use for the connection.
                 */
                if (is_string($username) && is_string($password)) {
                    $options_ok = $options_ok && curl_setopt($resource, CURLOPT_USERPWD, "$username:$password");
                }

                if ($options_ok === false) {
                    /**
                     * Options failed.  Store the error.
                     */
                    $errno = curl_errno($resource);
                    $error = curl_error($resource);
                    $errstep = 'curl_setopt()';
                } else {
                    /**
                     * Ready to roll.  Grab the url.
                     */
                    $exec_ok = curl_exec($resource);

                    if ($exec_ok === false) {
                        /**
                         * Exec failed.  Store the error.
                         */
                        $errno = curl_errno($resource);
                        $error = curl_error($resource);
                        $errstep = 'curl_exec()';

                    } else {
                        /**
                         * Exec succeeded.  Grab the contents.
                         */
                        $curlinfo = curl_getinfo($resource);
                        if ($curlinfo === false) {
                            /**
                             * Getinfo failed.  Store the error.
                             */
                            $errno = curl_errno($resource);
                            $error = curl_error($resource);
                            $errstep = 'curl_getinfo()';
                        } else {

                            /**
                             * Because we get the response headers (CURLOPT_HEADER is set to true), we must separater these headers from the actual content.
                             */
                            $headers_length = $curlinfo['header_size'];
                            $headers = substr($exec_ok, 0, $headers_length);
                            $contents = substr($exec_ok, $headers_length);
                            $contents_length = strlen($contents);

                            $curlinfo['response_headers_string'] = $headers;
                            $curlinfo['response_headers'] = self::get_headers_from_curl_response($headers);
                            $curlinfo['contents'] = $contents;

                            /**
                             * Do this, in case the server does not send the value ...
                             * We wrap an intval() around the value, because what comes back is actually a float!
                             */
                            if ((!isset($curlinfo['download_content_length'])) || (intval($curlinfo['download_content_length'], 10) === -1)) {
                                $curlinfo['download_content_length'] = $contents_length;
                            }

                        }
                    }

                }


            }

        } else {
            $errno = 665;
            $error = 'curl is not installed.';
            $errstep = 'curl is not installed.';
        }


        /**
         * Clean up our resource.
         */
        if ($resource !== false) {
            curl_close($resource);
        }


        /**
         * Return everything to the user.
         */
        $returnValue['curlinfo'] = $curlinfo;
        $returnValue['errno'] = $errno;
        $returnValue['error'] = $error;
        $returnValue['errstep'] = $errstep;

        return $returnValue;
    }

    /**
     * Determines whether a particular substring is stored at the beginning of another string. This function is case-sensitive.
     * @param string|array $string Text or text list. The string(s) you want to search.
     * @param string|array $substring Text or text list. The string you want to search for at the beginning of string.
     * @return bool Returns true (1) if any substring begins any one of the strings.  Returns false (0) if no substrings begins in any of the strings. If any element in the substrings is a null string(""), this function always returns true.
     */
    function begins($string, $substring)
    {

        $returnValue = false;
        $string_array = $this->returnAsArray($string);
        $substring_array = $this->returnAsArray($substring);

        foreach ($string_array as $string_value) {
            foreach ($substring_array as $substring_value) {
                if (($substring_value === '') || (strpos($string_value, $substring_value) === 0)) {
                    $returnValue = true;
                    break;
                }
            }
            if ($returnValue) {
                break;
            }
        }

        return $returnValue;
    }

    /**
     * Make an array of all headers.
     *
     * For example, the headers
     *
     * HTTP/1.1 302 Found
     * Cache-Control: no-cache
     * Pragma: no-cache
     * Content-Type: text/html; charset=utf-8
     * Expires: -1
     * Location: http://www.website.com/
     * Server: Microsoft-IIS/7.5
     * X-AspNet-Version: 4.0.30319
     * Date: Sun, 08 Sep 2013 10:51:39 GMT
     * Connection: close
     * Content-Length: 16313
     *
     * HTTP/1.1 200 OK
     * Cache-Control: private
     * Content-Type: text/html; charset=utf-8
     * Server: Microsoft-IIS/7.5
     * X-AspNet-Version: 4.0.30319
     * Date: Sun, 08 Sep 2013 10:51:39 GMT
     * Connection: close
     * Content-Length: 15519
     *
     *
     * are returned as
     *
     * (
     * [0] => Array
     *  (
     *      [http_code] => HTTP/1.1 302 Found
     *      [Cache-Control] => no-cache
     *      [Pragma] => no-cache
     *      [Content-Type] => text/html; charset=utf-8
     *      [Expires] => -1
     *      [Location] => http://www.website.com/
     *      [Server] => Microsoft-IIS/7.5
     *      [X-AspNet-Version] => 4.0.30319
     *      [Date] => Sun, 08 Sep 2013 10:51:39 GMT
     *      [Connection] => close
     *      [Content-Length] => 16313
     *  )
     *
     * [1] => Array
     *  (
     *      [http_code] => HTTP/1.1 200 OK
     *      [Cache-Control] => private
     *      [Content-Type] => text/html; charset=utf-8
     *      [Server] => Microsoft-IIS/7.5
     *      [X-AspNet-Version] => 4.0.30319
     *      [Date] => Sun, 08 Sep 2013 10:51:39 GMT
     *      [Connection] => close
     *      [Content-Length] => 15519
     *  )
     * )
     *
     *
     * @param string $headerContent
     * @return array
     * @see http://stackoverflow.com/questions/10589889/returning-header-as-array-using-curl
     */
    private static function get_headers_from_curl_response($headerContent)
    {

        $headers = array();

        // Split the string on every "double" new line.
        $arrRequests = explode("\r\n\r\n", $headerContent);

        // Loop of response headers. The "count() -1" is to
        //avoid an empty row for the extra line break before the body of the response.
        for ($index = 0; $index < count($arrRequests) - 1; $index++) {

            foreach (explode("\r\n", $arrRequests[$index]) as $i => $line) {
                if ($i === 0)
                    $headers[$index]['http_code'] = $line;
                else {
                    list ($key, $value) = explode(': ', $line);
                    $headers[$index][$key] = $value;
                }
            }
        }

        return $headers;
    }


}