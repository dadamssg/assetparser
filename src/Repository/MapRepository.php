<?php

namespace ClearC2\AssetParser\Repository;

use ClearC2\AssetParser\Entity\Map;

/**
 * Interface MapRepository
 * @package ClearC2\AssetParser\Repository
 */
interface MapRepository
{
    /**
     * @return Map[]
     */
    public function all();
}