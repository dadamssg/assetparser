<?php

namespace ClearC2\AssetParser\Repository;

use ClearC2\AssetParser\Entity\HtmlDocument;

/**
 * Class FileHtmlDocumentRepository
 * @package ClearC2\AssetParser\Repository
 */
class FileHtmlDocumentRepository implements HtmlDocumentRepository
{
    /**
     * @var array
     */
    private $filenames;

    /**
     * @param array $filenames
     */
    public function __construct(array $filenames)
    {
        $this->filenames = $filenames;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return array_map(array($this, 'createDocument'), $this->filenames);
    }

    /**
     * @param string $filename
     * @return HtmlDocument
     */
    private function createDocument($filename)
    {
        $contents = file_get_contents($filename);

        return new HtmlDocument($contents);
    }
}