<?php

namespace ClearC2\AssetParser\Repository;

use ClearC2\AssetParser\Entity\Map;

/**
 * Class FileMapRepository
 * @package ClearC2\AssetParser\Repository
 */
class FileMapRepository implements MapRepository
{
    /**
     * @var array
     */
    private $filenames;

    /**
     * @param array $filenames
     */
    public function __construct(array $filenames)
    {
        $this->filenames = $filenames;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return array_map(array($this, 'createMap'), $this->filenames);
    }

    /**
     * @param $filename
     * @return Map
     */
    private function createMap($filename)
    {
        $contents = file_get_contents($filename);

        return new Map($contents);
    }
}