<?php

namespace ClearC2\AssetParser\Repository;

use ClearC2\AssetParser\Entity\HtmlDocument;

/**
 * Interface HtmlDocumentRepository
 * @package ClearC2\AssetParser\Repository
 */
interface HtmlDocumentRepository
{
    /**
     * @return HtmlDocument[]
     */
    public function all();
}